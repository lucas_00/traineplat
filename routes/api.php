<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(["namespace" => "Api"],function() {
    Route::post('/login', 'AuthController@login')->name('login');
    Route::post('/register', 'AuthController@signup');

    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::post('/logout', 'AuthController@logout');

        // Courses
        Route::get('/courses', 'CourseController@index');
        Route::post('/courses', 'CourseController@create');
        Route::get('/courses/{course}', 'CourseController@show')
        ->where('course', '[0-9]+');
        Route::get('/courses/{course}/edit', 'CourseController@edit')
        ->where('course', '[0-9]+');
        Route::put('/courses/{course}/update', 'CourseController@update')
        ->where('course', '[0-9]+');
        Route::put('/courses/{course}/destroy', 'CourseController@destroy')
        ->where('course', '[0-9]+');

        // CourseNotification
        Route::get('/courses/{course}/notifications', 'CourseNotificationController@index')
        ->where('course', '[0-9]+');
        Route::post('/courses/{course}/notifications', 'CourseNotificationController@create')
        ->where('course', '[0-9]+');
        Route::put('/courses/{course}/notifications/{notification}/destroy', 'CourseNotificationController@destroy')
        ->where('course', '[0-9]+')
        ->where('notification', '[0-9]+');

        // Messages
        Route::post('/courses/{course}/messages', 'MessageController@create')
        ->where('course', '[0-9]+');
        Route::get('/courses/{course}/messages/{message}', 'MessageController@show')
        ->where('course', '[0-9]+')
        ->where('message', '[0-9]+');
        Route::get('/courses/{course}/messages/{message}/edit', 'MessageController@edit')
        ->where('course', '[0-9]+')
        ->where('message', '[0-9]+');
        Route::put('/courses/{course}/messages/{message}/update', 'MessageController@update')
        ->where('course', '[0-9]+')
        ->where('message', '[0-9]+');
        Route::put('/courses/{course}/messages/{message}/destroy', 'MessageController@destroy')
        ->where('course', '[0-9]+')
        ->where('message', '[0-9]+');

        // Units
        Route::post('/courses/{course}/units', 'UnitController@create')
        ->where('course', '[0-9]+');
        Route::get('/courses/{course}/units/{unit}', 'UnitController@show')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+');
        Route::get('/courses/{course}/units/{unit}/edit', 'UnitController@edit')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+');
        Route::put('/courses/{course}/units/{unit}/update', 'UnitController@update')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+');
        Route::put('/courses/{course}/units/{unit}/destroy', 'UnitController@destroy')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+');

        // Lessons
        Route::post('/courses/{course}/units/{unit}/lessons', 'LessonController@create')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+');
        Route::get('/courses/{course}/units/{unit}/lessons/{lesson}', 'LessonController@show')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+')
        ->where('lesson', '[0-9]+');
        Route::get('/courses/{course}/units/{unit}/lessons/{lesson}/edit', 'LessonController@edit')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+')
        ->where('lesson', '[0-9]+');
        Route::put('/courses/{course}/units/{unit}/lessons/{lesson}/update', 'LessonController@update')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+')
        ->where('lesson', '[0-9]+');
        Route::put('/courses/{course}/units/{unit}/lessons/{lesson}/destroy', 'LessonController@destroy')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+')
        ->where('lesson', '[0-9]+');

        // Comments
        Route::post('/courses/{course}/comments', 'CommentController@courseStore')
        ->where('course', '[0-9]+');
        Route::post('/courses/{course}/units/{unit}/comments', 'CommentController@unitStore')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+');
        Route::post('/courses/{course}/units/{unit}/lessons/{lesson}/comments', 'CommentController@lessonStore')
        ->where('course', '[0-9]+')
        ->where('unit', '[0-9]+')
        ->where('lesson', '[0-9]+');

        // Icons
        Route::get('/icons', 'MaterialIconsController@show');
    });
});

