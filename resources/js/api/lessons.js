import axios from 'axios';

export default {
    all(course_id, unit_id, headers) {
        return axios.get(`/courses/${course_id}/units/${unit_id}/lessons`, headers);
    },
    find(course_id, unit_id, lesson_id, headers) {
        return axios.get(`/courses/${course_id}/units/${unit_id}/lessons/${lesson_id}`, headers);
    },
    create(course_id, unit_id, data, headers) {
        return axios.post(`/courses/${course_id}/units/${unit_id}/lessons`, data, headers);
    },
    edit(course_id, unit_id, lesson_id, headers) {
        return axios.get(`/courses/${course_id}/units/${unit_id}/lessons/${lesson_id}/edit`, headers);
    },
    update(course_id, unit_id, lesson_id, data, headers) {
        return axios.post(`/courses/${course_id}/units/${unit_id}/lessons/${lesson_id}/update`, data, headers);
    },
    destroy(course_id, unit_id, lesson_id, headers) {
        return axios.put(`/courses/${course_id}/units/${unit_id}/lessons/${lesson_id}/destroy`, headers);
    },
};
