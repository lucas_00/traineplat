import axios from 'axios';

export default {
    store(id, data, headers) {
        return axios.post(`/courses/${id}/comments`, data, headers);
    },
};
