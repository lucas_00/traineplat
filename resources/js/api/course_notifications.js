import axios from 'axios';

export default {
    all(course_id, headers) {
        return axios.get(`/courses/${course_id}/notifications`, headers);
    },
    create(course_id, data, headers) {
        return axios.post(`/courses/${course_id}/notifications`, data, headers);
    },
    destroy(course_id, course_notification_id, headers) {
        return axios.put(`/courses/${course_id}/notifications/${course_notification_id}/destroy`, headers);
    },
};
