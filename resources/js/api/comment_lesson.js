import axios from 'axios';

export default {
    store(course_id, unit_id, lesson_id, data, headers) {
        return axios.post(`/courses/${course_id}/units/${unit_id}/lessons/${lesson_id}/comments`, data, headers);
    },
};
