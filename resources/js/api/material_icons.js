import axios from 'axios';

export default {
    all(headers) {
        return axios.get('/icons', headers);
    },
    search(icon, headers) {
        return axios.get(`/icons/${icon}`, headers);
    },
};
