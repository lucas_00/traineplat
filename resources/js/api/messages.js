import axios from 'axios';

export default {
    all(headers) {
        return axios.get('/courses', headers);
    },
    find(course_id, message_id, headers) {
        return axios.get(`/courses/${course_id}/messages/${message_id}`, headers);
    },
    create(course_id, data, headers) {
        return axios.post(`/courses/${course_id}/messages/`, data, headers);
    },
    edit(course_id, message_id, headers) {
        return axios.get(`/courses/${course_id}/messages/${message_id}/edit`, headers);
    },
    update(course_id, message_id, data, headers) {
        return axios.post(`/courses/${course_id}/messages/${message_id}/update`, data, headers);
    },
    destroy(course_id, message_id, headers) {
        return axios.put(`/courses/${course_id}/messages/${message_id}/destroy`, headers);
    },
};
