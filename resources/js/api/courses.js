import axios from 'axios';

export default {
    all(headers) {
        return axios.get('/courses', headers);
    },
    find(id, headers) {
        return axios.get(`/courses/${id}`, headers);
    },
    create(data, headers) {
        return axios.post(`/courses`, data, headers);
    },
    edit(id, headers) {
        return axios.get(`/courses/${id}/edit`, headers);
    },
    update(id, data, headers) {
        return axios.post(`/courses/${id}/update`, data, headers);
    },
    destroy(id, headers) {
        return axios.put(`/courses/${id}/destroy`, headers);
    },
};
