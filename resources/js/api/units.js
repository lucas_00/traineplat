import axios from 'axios';

export default {
    all(headers) {
        return axios.get('/courses', headers);
    },
    find(course_id, unit_id, headers) {
        return axios.get(`/courses/${course_id}/units/${unit_id}`, headers);
    },
    create(course_id, data, headers) {
        return axios.post(`/courses/${course_id}/units/`, data, headers);
    },
    edit(course_id, unit_id, headers) {
        return axios.get(`/courses/${course_id}/units/${unit_id}/edit`, headers);
    },
    update(course_id, unit_id, data, headers) {
        return axios.post(`/courses/${course_id}/units/${unit_id}/update`, data, headers);
    },
    destroy(course_id, unit_id, headers) {
        return axios.put(`/courses/${course_id}/units/${unit_id}/destroy`, headers);
    },
};
