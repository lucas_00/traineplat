import Vue from 'vue';
import VueRouter from 'vue-router';

import EmptyRouterView from '@/js/components/global/EmptyRouterView';

import NotFound from '@/js/views/pages/exceptions/NotFound';

import LogoutComponent from '@/js/views/auth/LogoutComponent';
import LoginComponent from '@/js/views/auth/LoginComponent';
import RegisterComponent from '@/js/views/auth/RegisterComponent';

import UsersIndex from '@/js/views/pages/user/UsersIndex';
import SearchComponent from '@/js/views/pages/search/SearchComponent';

import HomeComponent from '@/js/views/pages/HomeComponent';

import DashboardComponent from '@/js/views/pages/dashboard/DashboardComponent';

import CoursesComponent from '@/js/views/layouts/CoursesComponent';
import CoursesIndexComponent from '@/js/views/pages/courses/CoursesIndexComponent';
import CoursesCreateComponent from '@/js/views/pages/courses/CoursesCreateComponent';
import CoursesShowComponent from '@/js/views/pages/courses/CoursesShowComponent';
import CoursesStatisticsComponent from '@/js/views/pages/courses/CoursesStatisticsComponent';
import CoursesEditComponent from '@/js/views/pages/courses/CoursesEditComponent';

import CourseNotificationsIndexComponent from '@/js/views/pages/courses/notifications/CourseNotificationsIndexComponent';

import UnitsShowComponent from '@/js/views/pages/courses/units/UnitsShowComponent';
import UnitsEditComponent from '@/js/views/pages/courses/units/UnitsEditComponent';
import UnitsCreateComponent from '@/js/views/pages/courses/units/UnitsCreateComponent';

import MessagesShowComponent from '@/js/views/pages/courses/messages/MessagesShowComponent';
import MessagesEditComponent from '@/js/views/pages/courses/messages/MessagesEditComponent';
import MessagesCreateComponent from '@/js/views/pages/courses/messages/MessagesCreateComponent';

import LessonsShowComponent from '@/js/views/pages/courses/units/lessons/LessonsShowComponent';
import LessonsEditComponent from '@/js/views/pages/courses/units/lessons/LessonsEditComponent';
import LessonsCreateComponent from '@/js/views/pages/courses/units/lessons/LessonsCreateComponent';

import ActivitiesCreateComponent from '@/js/views/pages/courses/activities/ActivitiesCreateComponent';

import StadisticsComponent from '@/js/views/pages/stadistics/StadisticsComponent';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		component: EmptyRouterView,
		children:[
			{
				path: '',
				name: 'home',
				component: HomeComponent,
				meta: {
					auth: true,
				}
			},{
				path: 'logout',
				name: 'logout',
				component: LogoutComponent,
				meta: {
					auth: true,
				}
			},{
				path: 'login',
				name: 'login',
				component: LoginComponent,
			},{
				path: 'register',
				name: 'register',
				component: RegisterComponent
			},{
				path: 'user',
				name: 'user',
				component: UsersIndex,
				meta: {
					auth: true,
				}
			},{
				path: 'courses',
				component: CoursesComponent,
				meta: {
					auth: true,
				},
				children: [
					{
						path: '',
						name: 'courses.index',
						component: CoursesIndexComponent,
					},{
						path: ':course(\\d+)',
						component: EmptyRouterView,
						children: [
							{
								path: '',
								name: 'courses.show',
								component: CoursesShowComponent,

							},{
								path: 'edit',
								name: 'courses.edit',
								component: CoursesEditComponent,
							},{
								path: 'notifications',
								component: EmptyRouterView,
								children: [
									{
                                        path: '',
                                        name: 'courses.notifications.index',
										component: CourseNotificationsIndexComponent,
									}
								]
							},{
								path: 'units',
								component: EmptyRouterView,
								children: [
									{
										path: ':unit(\\d+)',
										component: EmptyRouterView,
										children: [
											{
												path: '',
												name: 'courses.show.units.show',
												component: UnitsShowComponent,
											},{
                                                path: 'edit',
                                                name: 'courses.show.units.edit',
                                                component: UnitsEditComponent,
                                            },{
												path: 'lessons',
												component: EmptyRouterView,
												children: [
													{
														path: ':lesson(\\d+)',
														component: EmptyRouterView,
														children: [
															{
																path: '',
																name: 'courses.show.units.show.lessons.show',
																component: LessonsShowComponent,
															},{
																path: 'edit',
																name: 'courses.show.units.show.lessons.show.edit',
																component: LessonsEditComponent,
															}
														]
													},{
														path: 'create',
														name: 'courses.show.units.show.lessons.create',
														component: LessonsCreateComponent,
													}
												]
											}
										]
									},{
										path: 'create',
										name: 'courses.show.units.create',
										component: UnitsCreateComponent,
									},
								]
							},{
								path: 'messages',
								component: EmptyRouterView,
								children: [
									{
										path: ':message(\\d+)',
										component: EmptyRouterView,
										children: [
											{
												path: '',
												name: 'courses.show.messages.show',
												component: MessagesShowComponent,
											},{
                                                path: 'edit',
                                                name: 'courses.show.messages.edit',
                                                component: MessagesEditComponent,
                                            }
										]
									},{
										path: 'create',
										name: 'courses.show.messages.create',
										component: MessagesCreateComponent,
									},
								]
							},{
								path: 'activities/create',
								name: 'courses.show.activities.create',
								component: ActivitiesCreateComponent,
							}
						]
					},{
						path: 'create',
						name: 'courses.create',
						component: CoursesCreateComponent,
					},{
						path: 'statistics',
						name: 'courses.statistics',
						component: CoursesStatisticsComponent,
					}
				]
			},{
				path: 'dashboard',
				component: CoursesComponent,
				meta: {
					auth: true,
				},
				children: [
					{
						path: '',
						name: 'dashboard.index',
						component: DashboardComponent,
					},
				]
			},{
				path: 'stadistics',
				name: 'stadistics',
				component: StadisticsComponent,
				meta: {
					auth: true,
				}
			},{
				path: 'search',
				name: 'search',
				component: SearchComponent,
				meta: {
					auth: true,
				}
			}
		]
	},{
		path: '*',
		component: NotFound
	}
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
  	routes
});

router.beforeEach((to, from, next) => {
    const loggedIn = localStorage.getItem('user')
    if (to.matched.some(record => record.meta.auth) && !loggedIn) {
        next('/login')
        return
    }
    next()
})

export default router;
