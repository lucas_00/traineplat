/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import axios from 'axios';

import VueAxios from 'vue-axios';
import Vuetify from 'vuetify';

import router from '@/js/router/index.js';
import store from '@/js/store/index.js';
import App from '@/js/views/App';

import VueQuillEditor from 'vue-quill-editor';

import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false;

Vue.use(VueAxios, axios);
Vue.use(Vuetify);
Vue.use(VueQuillEditor, /* { default global options } */)

const app = new Vue({
	router,
	store,
	vuetify: new Vuetify(),
	created () {
		const userInfo = localStorage.getItem('user')
		if (userInfo) {
			const userData = JSON.parse(userInfo)
			this.$store.commit('setUserData', userData)
		}
		axios.interceptors.response.use(
			response => response,
			error => {
				if (error.response.status === 401) {
					this.$store.dispatch('logout')
				}
				return Promise.reject(error)
			}
		)
	},
	render: h => h(App)
}).$mount('#app')

export default app;
