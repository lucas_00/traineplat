<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseElement extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order', 'course_id', 'type',
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function message()
    {
        return $this->hasOne(Message::class);
    }

    public function unit()
    {
        return $this->hasOne(Unit::class);
    }
}
