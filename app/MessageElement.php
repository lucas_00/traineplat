<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageElement extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order', 'type', 'message_id'
    ];

    public function message()
    {
        return $this->belongsTo(Message::class);
    }

    public function images()
    {
        return $this->hasMany(MessageElementImage::class);
    }

    public function video()
    {
        return $this->hasOne(MessageElementVideo::class);
    }

    public function link()
    {
        return $this->hasOne(MessageElementLink::class);
    }

    public function text()
    {
        return $this->hasOne(MessageElementText::class);
    }

}
