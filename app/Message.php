<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'status_id', 'course_element_id', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function courseElement()
    {
        return $this->belongsTo(CourseElement::class);
    }

    public function elements()
    {
        return $this->hasMany(MessageElement::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

}
