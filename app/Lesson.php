<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'info', 'status_id', 'unit_element_id', 'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function unitElement()
    {
        return $this->belongsTo(ElementUnit::class);
    }

    public function files()
    {
        return $this->hasMany(LessonFile::class);
    }

    public function elements()
    {
        return $this->hasMany(LessonElement::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function comments()
    {
        return $this->hasMany(CommentLesson::class);
    }
}
