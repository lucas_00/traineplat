<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitElement extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order', 'unit_id', 'type', 'lesson_id', 'activity_id', 'exam_id'
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function lesson()
    {
        return $this->hasOne(Lesson::class);
    }

    public function activity()
    {
        return $this->hasOne(Activity::class);
    }

    public function exam()
    {
        return $this->hasOne(Exam::class);
    }
}

