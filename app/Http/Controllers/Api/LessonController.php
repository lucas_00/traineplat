<?php

namespace App\Http\Controllers\Api;

use App\Lesson;
use App\LessonFile;
use App\LessonElement;
use App\LessonElementImage;
use App\LessonElementLink;
use App\LessonElementVideo;
use App\LessonElementText;
use App\UnitElement;
use App\CommentLesson;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUpload;

class LessonController extends Controller
{
	use FileUpload;

	public function index($course_id, $lesson_id)
	{
	}

	public function show($course_id, $unit_id, $lesson_id)
	{
		$lesson = Lesson::with('files')
		->with('status')
		->with('elements', 'elements.images', 'elements.link', 'elements.video', 'elements.text')
		->find($lesson_id);

        $comments = CommentLesson::with('user')
        ->where('lesson_id', $lesson_id)
        ->orderBy('created_at', 'asc')
        ->get();

        return response()->json(array('lesson' => $lesson, 'comments' => $comments));
	}

	public function edit($course_id, $unit_id, $lesson_id)
	{
		$lesson = Lesson::with('files')
		->with('status')
		->with('elements', 'elements.images', 'elements.link', 'elements.video', 'elements.text')
		->find($lesson_id);

		return response()->json(array('lesson' => $lesson));
	}

	public function create($course_id, $unit_id, Request $request)
	{
		$user = auth()->user();

		try{
			$validator = $request->validate([
				'title'     => 'required|string',
			]);

            $count = UnitElement::where('unit_id', $unit_id)
            ->count();

            $unit_element = new UnitElement([
                'order'     => $count + 1,
                'type'    => 'lesson',
                'unit_id'    => $unit_id,
            ]);
            $unit_element->save();

			// Lesson store
			$lesson = new Lesson([
				'title'     => $request->title,
				'info'    => $request->info,
				'status_id'    => 1,
				'unit_element_id'    => $unit_element->id,
				'user_id'    => $user->id,
			]);
			$lesson->save();

			// Lesson files store
			for ($i = 0; $i < count($request->files); $i++) {
				if ($request->hasFile('files-' . $i)) {
					$requestFile = $request->file('files-' . $i);
					$file = $this->saveFiles($requestFile, 'lessons/');

					$lessonFile = new LessonFile([
						'name'     => $file,
						'original_name'     => $requestFile->getClientOriginalName(),
						'path'     => ('/uploads/lessons/' . $file),
						'user_id'    => $user->id,
						'lesson_id'    => $lesson->id,
					]);

					$lessonFile->save();
				}
			}

			$lesson_elements = json_decode($request->lesson_elements, true);

			if (count($lesson_elements)) {
				for ($i = 0; $i < count($lesson_elements); $i++) {

                    $element = json_decode(json_encode($lesson_elements[$i]));

                    $lesson_element = new LessonElement([
                        'order' => $element->order,
                        'type'    => $element->type,
                        'lesson_id'    => $lesson->id,
                    ]);

                    $lesson_element->save();

					if ($element->type == 'text') {
						$lesson_element_text = new LessonElementText([
                            'content'    => $element->content,
							'lesson_element_id'    => $lesson_element->id,
						]);

                        $lesson_element_text->save();

					}else if ($element->type == 'video') {
						$lesson_element_video = new LessonElementVideo([
                            'content'    => $element->content,
							'lesson_element_id'    => $lesson_element->id,
						]);

                        $lesson_element_video->save();

					}else if ($element->type == 'link') {
						$lesson_element_link = new LessonElementLink([
                            'content'    => $element->content,
							'lesson_element_id'    => $lesson_element->id,
						]);

                        $lesson_element_link->save();

					}else if ($element->type == 'image') {

						for ($y=0; $y < count($request->files); $y++) {
							if ($request->hasFile('lesson_elements-' . $i . '-image-' . $y)) {
								$image = $request->file('lesson_elements-' . $i . '-image-' . $y);
								$file = $this->saveFiles($image, 'lessons/contents/');

								$lesson_element_image = new LessonElementImage([
									'name' => $file,
									'original_name' => $image->getClientOriginalName(),
									'path' => ('/uploads/lessons/contents/' . $file),
									'lesson_element_id' => $lesson_element->id,
                                ]);

                                $lesson_element_image->save();
							}
						}
                    }
				}
			}

			return response()->json([
				'message' => 'Successfully created lesson!'], 201);

		}catch(\Illuminate\Validation\ValidationException $e){
			return response()->json($e, $e->status);
		}
    }

    // update lesson model
    public function update($course_id, $unit_id, $lesson_id, Request $request)
	{
        $user = auth()->user();

		try{
			$validator = $request->validate([
				'title'     => 'required|string',
            ]);

			// Lesson store
			$lesson = Lesson::find($lesson_id);
            $lesson->title = $request->title;
            $lesson->info = $request->info;
            $lesson->save();


            // Tasks
            $tasks = json_decode($request->tasks);

            foreach ($tasks as $task){
                if ($task->objetive == 'delete_element') {
                    $lesson_element = LessonElement::find($task->id);
                    $lesson_element->delete();
                }else if($task->objetive == 'delete_file'){
                    $lesson_file = LessonFile::find($task->id);
                    $lesson_file->delete();
                }else if($task->objetive == 'delete_image'){
                    $lesson_element_image = LessonElementImage::find($task->id);
                    $lesson_element_image->delete();
                }
            }

			// Lesson files store
			for ($i = 0; $i < count($request->files); $i++) {
				if ($request->hasFile('files-' . $i)) {
                    $request_file = $request->file('files-' . $i);

					$file = $this->saveFiles($request_file, 'lessons/');

					$lesson_file = new LessonFile([
						'name'     => $file,
						'original_name'     => $request_file->getClientOriginalName(),
						'path'     => ('/uploads/lessons/' . $file),
						'user_id'    => $user->id,
						'lesson_id'    => $lesson->id,
					]);

					$lesson_file->save();
				}
			}

            $lesson_elements = json_decode($request->lesson_elements);

            for ($i = 0; $i < count($lesson_elements); $i++) {

                $element = $lesson_elements[$i];

                if (!isset($element->id)) {
                    $lesson_element = new LessonElement([
                        'order' => $element->order,
                        'type'    => $element->type,
                        'lesson_id'    => $lesson->id,
                    ]);

                    $lesson_element->save();

                    if ($element->type == 'text') {
                        // Text
                        $lesson_element_text = new LessonElementText([
                            'content'    => $element->text->content,
                            'lesson_element_id'    => $lesson_element->id,
                        ]);

                        $lesson_element_text->save();

                    }else if ($element->type == 'video') {
                        // Video
                        $lesson_element_video = new LessonElementVideo([
                            'content'    => $element->video->content,
                            'lesson_element_id'    => $lesson_element->id,
                        ]);

                        $lesson_element_video->save();

                    }else if ($element->type == 'link') {
                        // Link
                        $lesson_element_link = new LessonElementLink([
                            'content'    => $element->link->content,
                            'lesson_element_id'    => $lesson_element->id,
                        ]);

                        $lesson_element_link->save();

                    }else if ($element->type == 'image') {
                        // Image
                        for ($y=0; $y < count($request->files); $y++) {
                            if ($request->hasFile('lesson_elements-' . $i . '-image-' . $y)) {
                                $image = $request->file('lesson_elements-' . $i . '-image-' . $y);
                                $file = $this->saveFiles($image, 'lessons/contents/');

                                $lesson_element_image = new LessonElementImage([
                                    'name' => $file,
                                    'original_name' => $image->getClientOriginalName(),
                                    'path' => ('/uploads/lessons/contents/' . $file),
                                    'lesson_element_id' => $lesson_element->id,
                                ]);

                                $lesson_element_image->save();
                            }
                        }
                    }
                }else{
                    if ($element->type == 'text') {
                        // Text
                        $lesson_element_text = LessonElementText::find($element->text->id);
                        $lesson_element_text->content = $element->text->content;

                        $lesson_element_text->save();

                    }else if ($element->type == 'video') {
                        // Video
                        $lesson_element_video = LessonElementVideo::find($element->video->id);
                        $lesson_element_video->content = $element->video->content;

                        $lesson_element_video->save();

                    }else if ($element->type == 'link') {
                        // Link
                        $lesson_element_link = LessonElementLink::find($element->link->id);
                        $lesson_element_link->content = $element->link->content;

                        $lesson_element_link->save();

                    }else if ($element->type == 'image') {
                        // Image
                        for ($y=0; $y < count($request->files); $y++) {
                            if ($request->hasFile('lesson_elements-' . $i . '-image-' . $y)) {
                                $image = $request->file('lesson_elements-' . $i . '-image-' . $y);
                                $file = $this->saveFiles($image, 'lessons/contents/');

                                $lesson_element_image = new LessonElementImage([
                                    'name' => $file,
                                    'original_name' => $image->getClientOriginalName(),
                                    'path' => ('/uploads/lessons/contents/' . $file),
                                    'lesson_element_id' => $element->id,
                                ]);

                                $lesson_element_image->save();
                            }
                        }
                    }
                }
            }

			return response()->json([
				'message' => 'lesson updated!'], 201);

		}catch(\Illuminate\Validation\ValidationException $e){
			return response()->json($e, $e->status);
		}
    }

    public function destroy($course_id, $unit_id, $lesson_id){
        $lesson = Lesson::find($lesson_id);

        $unit_element = UnitElement::find($lesson->unit_element_id);

        if (!isset($lesson) && !isset($unit_element) && !empty($lesson) && !empty($unit_element)) {
            return response()->json('Lesson not found.');
        }else{
            $lesson->delete();
            $unit_element->delete();
            return response()->json([
                'message' => 'Lesson removed.'], 201);
        }
    }
}
