<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CourseNotification;

class CourseNotificationController extends Controller
{
    public function index($course_id)
    {
        $course_notifications = CourseNotification::where('course_id', $course_id)
        ->withTrashed()
        ->get();

        return response()->json(array('course_notifications' => $course_notifications));

    }

    public function create($course_id, Request $request)
    {
        $user = auth()->user();

        $notifications = CourseNotification::where('course_id', $course_id)
        ->delete();

        try{
            $validator = $request->validate([
                'content'     => 'required|string',
            ]);

            $course_notification = new CourseNotification([
                'content'     => $request->content,
                'type'     => $request->type,
                'course_id'    => $course_id,
                'user_id'    => $user->id,
            ]);

            $course_notification->save();

            return response()->json([
                'message' => 'Notification created successfully!'], 201);
        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json($e, $e->status);
        }
    }

    public function destroy($course_id, $course_notification_id){
        $course_notification = CourseNotification::where('course_id', $course_id)
        ->find($course_notification_id);

        if (!isset($course_notification) && !empty($course_notification)) {
            return response()->json('Notification not found.');
        }else{
            $course_notification->delete();

            return response()->json([
                'message' => 'Notification removed.'], 201);
        }
    }
}
