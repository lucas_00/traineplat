<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CourseElement;
use App\Message;
use App\MessageElement;
use App\MessageElementImage;
use App\MessageElementLink;
use App\MessageElementVideo;
use App\MessageElementText;
use App\Http\Controllers\Traits\FileUpload;

class MessageController extends Controller
{
    use FileUpload;

    public function show($course_id, $message_id)
    {
        $message = Message::with('status')
        ->with('elements', 'elements.images', 'elements.video', 'elements.text', 'elements.link')
        ->find($message_id);

        return response()->json(array('message' => $message));
    }

    public function edit($course_id, $message_id)
    {
        $message = Message::with('status')
        ->with('elements', 'elements.images', 'elements.video', 'elements.text', 'elements.link')
        ->find($message_id);

        return response()->json(array('message' => $message));
    }

    public function update($course_id, $message_id, Request $request)
    {
        $user = auth()->user();

		try{
			$validator = $request->validate([
				'title'     => 'required|string',
            ]);

			// Message store
			$message = Message::find($message_id);
            $message->title = $request->title;
            $message->description = $request->description;
            $message->save();


            // Tasks
            $tasks = json_decode($request->tasks);

            foreach ($tasks as $task){
                if ($task->objetive == 'delete_element') {
                    $message_element = MessageElement::find($task->id);
                    $message_element->delete();
                }else if($task->objetive == 'delete_image'){
                    $message_element_image = MessageElementImage::find($task->id);
                    $message_element_image->delete();
                }
            }

            $message_elements = json_decode($request->message_elements);

            for ($i = 0; $i < count($message_elements); $i++) {

                $element = $message_elements[$i];

                if (!isset($element->id)) {
                    $message_element = new MessageElement([
                        'order' => $element->order,
                        'type'    => $element->type,
                        'message_id'    => $message->id,
                    ]);

                    $message_element->save();

                    if ($element->type == 'text') {
                        // Text
                        $message_element_text = new MessageElementText([
                            'content'    => $element->text->content,
                            'message_element_id'    => $message_element->id,
                        ]);

                        $message_element_text->save();

                    }else if ($element->type == 'video') {
                        // Video
                        $message_element_video = new MessageElementVideo([
                            'content'    => $element->video->content,
                            'message_element_id'    => $message_element->id,
                        ]);

                        $message_element_video->save();

                    }else if ($element->type == 'link') {
                        // Link
                        $message_element_link = new MessageElementLink([
                            'content'    => $element->link->content,
                            'message_element_id'    => $message_element->id,
                        ]);

                        $message_element_link->save();

                    }else if ($element->type == 'image') {
                        // Image
                        for ($y=0; $y < count($request->files); $y++) {
                            if ($request->hasFile('message_elements-' . $i . '-image-' . $y)) {
                                $image = $request->file('message_elements-' . $i . '-image-' . $y);
                                $file = $this->saveFiles($image, 'messages/contents/');

                                $message_element_image = new MessageElementImage([
                                    'name' => $file,
                                    'original_name' => $image->getClientOriginalName(),
                                    'path' => ('/uploads/messages/contents/' . $file),
                                    'message_element_id' => $message_element->id,
                                ]);

                                $message_element_image->save();
                            }
                        }
                    }
                }else{
                    if ($element->type == 'text') {
                        // Text
                        $message_element_text = MessageElementText::find($element->text->id);
                        $message_element_text->content = $element->text->content;

                        $message_element_text->save();

                    }else if ($element->type == 'video') {
                        // Video
                        $message_element_video = MessageElementVideo::find($element->video->id);
                        $message_element_video->content = $element->video->content;

                        $message_element_video->save();

                    }else if ($element->type == 'link') {
                        // Link
                        $message_element_link = MessageElementLink::find($element->link->id);
                        $message_element_link->content = $element->link->content;

                        $message_element_link->save();

                    }else if ($element->type == 'image') {
                        // Image
                        for ($y=0; $y < count($request->files); $y++) {
                            if ($request->hasFile('message_elements-' . $i . '-image-' . $y)) {
                                $image = $request->file('message_elements-' . $i . '-image-' . $y);
                                $file = $this->saveFiles($image, 'messages/contents/');

                                $message_element_image = new MessageElementImage([
                                    'name' => $file,
                                    'original_name' => $image->getClientOriginalName(),
                                    'path' => ('/uploads/messages/contents/' . $file),
                                    'message_element_id' => $element->id,
                                ]);

                                $message_element_image->save();
                            }
                        }
                    }
                }
            }

			return response()->json([
				'message' => 'Message updated!'], 201);

		}catch(\Illuminate\Validation\ValidationException $e){
			return response()->json($e, $e->status);
		}
    }

    public function create($course_id, Request $request)
    {
        $user = auth()->user();

        try{
            $validator = $request->validate([
                'title'     => 'required|string',
            ]);

            $count = CourseElement::where('course_id', $course_id)
            ->count();

            $course_element = new CourseElement([
                'order'     => $count + 1,
                'type'    => 'message',
                'course_id'    => $course_id,
            ]);
            $course_element->save();

            // Message store
            $message = new Message([
                'title'     => $request->title,
                'description'    => $request->description,
                'user_id'    => $user->id,
                'status_id'    => 1,
                'course_element_id'    => $course_element->id,
            ]);
            $message->save();

            $elements = json_decode($request->elements);

            if (count($elements)) {
				for ($i = 0; $i < count($elements); $i++) {

                    $message_element = new MessageElement([
                        'order' => $elements[$i]->order,
                        'type'    => $elements[$i]->type,
                        'message_id'    => $message->id,
                    ]);

                    $message_element->save();

					if ($elements[$i]->type == 'text') {
						$message_element_text = new MessageElementText([
                            'content'    => $elements[$i]->content,
							'message_element_id'    => $message_element->id,
						]);

                        $message_element_text->save();

					}else if ($elements[$i]->type == 'video') {
						$message_element_video = new MessageElementVideo([
                            'content'    => $elements[$i]->content,
							'message_element_id'    => $message_element->id,
						]);

                        $message_element_video->save();

					}else if ($elements[$i]->type == 'link') {
						$message_element_link = new MessageElementLink([
                            'content'    => $elements[$i]->content,
							'message_element_id'    => $message_element->id,
						]);

                        $message_element_link->save();

					}else if ($elements[$i]->type == 'image') {

						for ($y=0; $y < count($request->files); $y++) {
							if ($request->hasFile('message_elements-' . $i . '-image-' . $y)) {
								$image = $request->file('message_elements-' . $i . '-image-' . $y);
								$file = $this->saveFiles($image, 'messages/contents/');

								$message_element_image = new MessageElementImage([
									'name' => $file,
									'original_name' => $image->getClientOriginalName(),
									'path' => ('/uploads/messages/contents/' . $file),
									'message_element_id' => $message_element->id,
                                ]);

                                $message_element_image->save();
							}
						}
                    }
				}
			}

            return response()->json([
                'message' => 'Message successfully created!'], 201);
        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json($e, $e->status);
        }
    }

    public function destroy($course_id, $message_id){
        $message = Message::find($message_id);

        $course_element = CourseElement::find($message->course_element_id);

        if (!isset($message) && !isset($course_element) && !empty($message) && !empty($course_element)) {
            return response()->json('Message not found.');
        }else{
            $message->delete();
            $course_element->delete();
            return response()->json([
                'message' => 'Message removed.'], 201);
        }
    }
}
