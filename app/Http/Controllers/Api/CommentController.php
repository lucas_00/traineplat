<?php

namespace App\Http\Controllers\Api;

use App\CommentCourse;
use App\CommentUnit;
use App\CommentLesson;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{

    public function courseStore($course_id, Request $request)
    {
        try{
            $validator = request()->validate([
                'message' => 'required|string',
            ]);

            $user = auth()->user();

            $comment = new CommentCourse([
                'content'   => $request->message,
                'course_id'     => $course_id,
                'user_id'    => $user->id,
            ]);

            $comment->save();

            $newComment = CommentCourse::with('user')
                ->orderBy('created_at', 'desc')
                ->find($comment->id);

            return response()->json([
                'comment' => $newComment],  201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json('Something went wrong on the server.', $e->status);
        }
    }

    public function lessonStore($course_id, $unit_id, $lesson_id, Request $request)
    {
        try{
            $validator = request()->validate([
                'message' => 'required|string',
            ]);

            $user = auth()->user();

            $comment = new CommentLesson([
                'content'   => $request->message,
                'lesson_id'     => $lesson_id,
                'user_id'    => $user->id,
            ]);

            $comment->save();

            $newComment = CommentLesson::with('user')
                ->orderBy('created_at', 'desc')
                ->find($comment->id);

            return response()->json([
                'comment' => $newComment],  201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json('Something went wrong on the server.', $e->status);
        }
    }

    public function unitStore($course_id, $unit_id, Request $request)
    {
        try{
            $validator = request()->validate([
                'message' => 'required|string',
            ]);

            $user = auth()->user();

            $comment = new CommentUnit([
                'content'   => $request->message,
                'unit_id'     => $unit_id,
                'user_id'    => $user->id,
            ]);

            $comment->save();

            $newComment = CommentUnit::with('user')
                ->orderBy('created_at', 'desc')
                ->find($comment->id);

            return response()->json([
                'comment' => $newComment],  201);

        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json('Something went wrong on the server.', $e->status);
        }
    }
}
