<?php

namespace App\Http\Controllers\Api;

use App\Unit;
use App\CourseElement;
use App\CommentUnit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    public function show($course_id, $unit_id)
    {
        $unit = Unit::with('status')
        ->with('elements', 'elements.lesson',)
        ->find($unit_id);
        // ->with('elements', 'elements.lesson', 'elements.activity', 'elements.exam')

        $comments = CommentUnit::with('user')
        ->where('unit_id', $unit_id)
        ->orderBy('created_at', 'asc')
        ->get();

        return response()->json(array('unit' => $unit, 'comments' => $comments));
    }

    public function edit($course_id, $unit_id)
    {
        $unit = Unit::find($unit_id);

        return response()->json(array('unit' => $unit));
    }

    public function update($course_id, $unit_id, Request $request)
    {
        $user = auth()->user();

        try{
            $validator = $request->validate([
                'title'     => 'required|string',
                'description'    => 'required|string|',
            ]);

            $unit = Unit::find($unit_id);

            $unit->title = $request->title;
            $unit->description = $request->description;
            $unit->icon_name = $request->icon_name;
            $unit->save();

            return response()->json([
                'message' => 'Unit successfully updated!!'], 201);
        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json($e, $e->status);
        }
    }

    public function create($course_id, Request $request)
    {
        $user = auth()->user();

        try{
            $validator = $request->validate([
                'title'     => 'required|string',
                'description'    => 'required|string|',
            ]);

            $count = CourseElement::where('course_id', $course_id)
            ->count();

            $course_element = new CourseElement([
                'order'     => $count+1,
                'type'    => 'unit',
                'course_id'    => $course_id,
            ]);
            $course_element->save();

            $unit = new Unit([
                'title'     => $request->title,
                'description'    => $request->description,
                'icon_name'    => $request->iconName,
                'user_id'    => $user->id,
                'status_id'    => 1,
                'course_element_id'    => $course_element->id,
            ]);
            $unit->save();

            return response()->json([
                'message' => 'Unit successfully created!'], 201);
        }catch(\Illuminate\Validation\ValidationException $e){
            return response()->json($e, $e->status);
        }
    }

    public function destroy($course_id, $unit_id){
        $unit = Unit::find($unit_id);

        $course_element = CourseElement::find($unit->course_element_id);

        if (!isset($unit) && !isset($course_element) && !empty($unit) && !empty($course_element)) {
            return response()->json('Unit not found.');
        }else{
            $unit->delete();
            $course_element->delete();
            return response()->json([
                'message' => 'Unit removed.'], 201);
        }
    }
}
