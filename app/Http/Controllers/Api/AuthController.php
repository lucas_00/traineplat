<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return response([
                'message' => ['These credentials do not match our records.']
            ], 404);
        }

        $token = $user->createToken('my-app-token')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function signup(Request $request)
    {
        try{
            $validator = $request->validate([
                'name'     => 'required|string',
                'email'    => 'required|string|email|confirmed|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);

            $user = new User([
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $user->save();

            $this->guard()->login($user);

            return response()->json([
                'message' => 'Successfully created user!'], 201);

        }catch(\Illuminate\Validation\ValidationException $e){

            if ($e->status === 422) {
                return response()->json('The email has already been taken.', $e->status);
            }

            return response()->json('Something went wrong on the server.', $e->status);
        }
    }

    public function logout()
    {
        Auth::logout();
        return response()->json(['message' => 'Logged Out'], 200);
    }
}
