<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'subtitle',
    ];

    public function unitElement()
    {
        return $this->belongsTo(UnitElement::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
