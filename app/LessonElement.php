<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonElement extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order', 'type', 'lesson_id'
    ];

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    public function images()
    {
        return $this->hasMany(LessonElementImage::class);
    }

    public function video()
    {
        return $this->hasOne(LessonElementVideo::class);
    }

    public function link()
    {
        return $this->hasOne(LessonElementLink::class);
    }

    public function text()
    {
        return $this->hasOne(LessonElementText::class);
    }

}
