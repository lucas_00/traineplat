<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonElementImage extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'original_name', 'path', 'lesson_element_id'
    ];

    public function lessonElement()
    {
        return $this->belongsTo(LessonElement::class);
    }

}
