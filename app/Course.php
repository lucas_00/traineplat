<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'user_id', 'status_id', 'course_image_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function courseImage()
    {
        return $this->belongsTo(CourseImage::class);
    }

    public function notification()
    {
        return $this->hasOne(CourseNotification::class);
    }

    public function elements()
    {
        return $this->hasMany(CourseElement::class);
    }

    public function comments()
    {
        return $this->hasMany(CommentCourse::class);
    }
}
