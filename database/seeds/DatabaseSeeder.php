<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            StatusesTableSeeder::class,
            UsersTableSeeder::class,
            MaterialIconsTableSeeder::class,
        ]);

        factory(App\Course::class)->create([
            'title' => 'Rerum molestiae ad autem sit.',
            'description' => 'Commodi voluptas est sint magnam consequuntur eum quam adipisci. Mollitia dolor et consequatur sit. Tempora expedita sint sunt et et mollitia voluptas. Sequi similique quidem ipsa itaque.',
            'status_id' => 1,
            'user_id' => 1
        ]);

        factory(App\Course::class)->create([
            'title' => 'Rerum molestiae ad autem sit.',
            'description' => 'Commodi voluptas est sint magnam consequuntur eum quam adipisci. Mollitia dolor et consequatur sit. Tempora expedita sint sunt et et mollitia voluptas. Sequi similique quidem ipsa itaque.',
            'status_id' => 1,
            'user_id' => 1
        ]);
    }
}
