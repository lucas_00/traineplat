<?php

use Illuminate\Database\Seeder;

class MaterialIconsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MaterialIcon::class)->create([
            'name' => 'ab-testing',
            'content' =>"\F01C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'abjad-arabic',
            'content' =>"\F1328"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'abjad-hebrew',
            'content' =>"\F1329"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'abugida-devanagari',
            'content' =>"\F132A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'abugida-thai',
            'content' =>"\F132B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'access-point',
            'content' =>"\F0003"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'access-point-network',
            'content' =>"\F0002"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'access-point-network-off',
            'content' =>"\F0BE1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account',
            'content' =>"\F0004"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-alert',
            'content' =>"\F0005"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-alert-outline',
            'content' =>"\F0B50"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-arrow-left',
            'content' =>"\F0B51"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-arrow-left-outline',
            'content' =>"\F0B52"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-arrow-right',
            'content' =>"\F0B53"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-arrow-right-outline',
            'content' =>"\F0B54"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-box',
            'content' =>"\F0006"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-box-multiple',
            'content' =>"\F0934"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-box-multiple-outline',
            'content' =>"\F100A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-box-outline',
            'content' =>"\F0007"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-cancel',
            'content' =>"\F12DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-cancel-outline',
            'content' =>"\F12E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-cash',
            'content' =>"\F1097"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-cash-outline',
            'content' =>"\F1098"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-check',
            'content' =>"\F0008"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-check-outline',
            'content' =>"\F0BE2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-child',
            'content' =>"\F0A89"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-child-circle',
            'content' =>"\F0A8A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-child-outline',
            'content' =>"\F10C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-circle',
            'content' =>"\F0009"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-circle-outline',
            'content' =>"\F0B55"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-clock',
            'content' =>"\F0B56"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-clock-outline',
            'content' =>"\F0B57"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-cog',
            'content' =>"\F1370"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-cog-outline',
            'content' =>"\F1371"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-convert',
            'content' =>"\F000A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-convert-outline',
            'content' =>"\F1301"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-cowboy-hat',
            'content' =>"\F0E9B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-details',
            'content' =>"\F0631"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-details-outline',
            'content' =>"\F1372"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-edit',
            'content' =>"\F06BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-edit-outline',
            'content' =>"\F0FFB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-group',
            'content' =>"\F0849"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-group-outline',
            'content' =>"\F0B58"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-hard-hat',
            'content' =>"\F05B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-heart',
            'content' =>"\F0899"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-heart-outline',
            'content' =>"\F0BE3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-key',
            'content' =>"\F000B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-key-outline',
            'content' =>"\F0BE4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-lock',
            'content' =>"\F115E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-lock-outline',
            'content' =>"\F115F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-minus',
            'content' =>"\F000D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-minus-outline',
            'content' =>"\F0AEC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple',
            'content' =>"\F000E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple-check',
            'content' =>"\F08C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple-check-outline',
            'content' =>"\F11FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple-minus',
            'content' =>"\F05D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple-minus-outline',
            'content' =>"\F0BE5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple-outline',
            'content' =>"\F000F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple-plus',
            'content' =>"\F0010"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple-plus-outline',
            'content' =>"\F0800"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple-remove',
            'content' =>"\F120A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-multiple-remove-outline',
            'content' =>"\F120B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-music',
            'content' =>"\F0803"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-music-outline',
            'content' =>"\F0CE9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-network',
            'content' =>"\F0011"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-network-outline',
            'content' =>"\F0BE6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-off',
            'content' =>"\F0012"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-off-outline',
            'content' =>"\F0BE7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-outline',
            'content' =>"\F0013"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-plus',
            'content' =>"\F0014"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-plus-outline',
            'content' =>"\F0801"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-question',
            'content' =>"\F0B59"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-question-outline',
            'content' =>"\F0B5A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-remove',
            'content' =>"\F0015"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-remove-outline',
            'content' =>"\F0AED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-search',
            'content' =>"\F0016"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-search-outline',
            'content' =>"\F0935"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-settings',
            'content' =>"\F0630"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-settings-outline',
            'content' =>"\F10C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-star',
            'content' =>"\F0017"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-star-outline',
            'content' =>"\F0BE8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-supervisor',
            'content' =>"\F0A8B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-supervisor-circle',
            'content' =>"\F0A8C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-supervisor-outline',
            'content' =>"\F112D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-switch',
            'content' =>"\F0019"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-switch-outline',
            'content' =>"\F04CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-tie',
            'content' =>"\F0CE3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-tie-outline',
            'content' =>"\F10CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-tie-voice',
            'content' =>"\F1308"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-tie-voice-off',
            'content' =>"\F130A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-tie-voice-off-outline',
            'content' =>"\F130B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-tie-voice-outline',
            'content' =>"\F1309"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'account-voice',
            'content' =>"\F05CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'adjust',
            'content' =>"\F001A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'adobe',
            'content' =>"\F0936"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'adobe-acrobat',
            'content' =>"\F0F9D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'air-conditioner',
            'content' =>"\F001B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'air-filter',
            'content' =>"\F0D43"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'air-horn',
            'content' =>"\F0DAC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'air-humidifier',
            'content' =>"\F1099"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'air-humidifier-off',
            'content' =>"\F1466"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'air-purifier',
            'content' =>"\F0D44"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'airbag',
            'content' =>"\F0BE9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'airballoon',
            'content' =>"\F001C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'airballoon-outline',
            'content' =>"\F100B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'airplane',
            'content' =>"\F001D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'airplane-landing',
            'content' =>"\F05D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'airplane-off',
            'content' =>"\F001E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'airplane-takeoff',
            'content' =>"\F05D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'airport',
            'content' =>"\F084B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm',
            'content' =>"\F0020"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-bell',
            'content' =>"\F078E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-check',
            'content' =>"\F0021"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-light',
            'content' =>"\F078F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-light-outline',
            'content' =>"\F0BEA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-multiple',
            'content' =>"\F0022"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-note',
            'content' =>"\F0E71"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-note-off',
            'content' =>"\F0E72"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-off',
            'content' =>"\F0023"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-plus',
            'content' =>"\F0024"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alarm-snooze',
            'content' =>"\F068E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'album',
            'content' =>"\F0025"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert',
            'content' =>"\F0026"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-box',
            'content' =>"\F0027"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-box-outline',
            'content' =>"\F0CE4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-circle',
            'content' =>"\F0028"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-circle-check',
            'content' =>"\F11ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-circle-check-outline',
            'content' =>"\F11EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-circle-outline',
            'content' =>"\F05D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-decagram',
            'content' =>"\F06BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-decagram-outline',
            'content' =>"\F0CE5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-minus',
            'content' =>"\F14BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-minus-outline',
            'content' =>"\F14BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-octagon',
            'content' =>"\F0029"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-octagon-outline',
            'content' =>"\F0CE6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-octagram',
            'content' =>"\F0767"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-octagram-outline',
            'content' =>"\F0CE7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-outline',
            'content' =>"\F002A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-plus',
            'content' =>"\F14BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-plus-outline',
            'content' =>"\F14BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-remove',
            'content' =>"\F14BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-remove-outline',
            'content' =>"\F14BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-rhombus',
            'content' =>"\F11CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alert-rhombus-outline',
            'content' =>"\F11CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alien',
            'content' =>"\F089A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alien-outline',
            'content' =>"\F10CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'align-horizontal-center',
            'content' =>"\F11C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'align-horizontal-left',
            'content' =>"\F11C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'align-horizontal-right',
            'content' =>"\F11C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'align-vertical-bottom',
            'content' =>"\F11C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'align-vertical-center',
            'content' =>"\F11C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'align-vertical-top',
            'content' =>"\F11C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'all-inclusive',
            'content' =>"\F06BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'allergy',
            'content' =>"\F1258"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha',
            'content' =>"\F002B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-a',
            'content' =>"\F0AEE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-a-box',
            'content' =>"\F0B08"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-a-box-outline',
            'content' =>"\F0BEB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-a-circle',
            'content' =>"\F0BEC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-a-circle-outline',
            'content' =>"\F0BED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-b',
            'content' =>"\F0AEF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-b-box',
            'content' =>"\F0B09"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-b-box-outline',
            'content' =>"\F0BEE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-b-circle',
            'content' =>"\F0BEF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-b-circle-outline',
            'content' =>"\F0BF0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-c',
            'content' =>"\F0AF0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-c-box',
            'content' =>"\F0B0A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-c-box-outline',
            'content' =>"\F0BF1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-c-circle',
            'content' =>"\F0BF2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-c-circle-outline',
            'content' =>"\F0BF3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-d',
            'content' =>"\F0AF1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-d-box',
            'content' =>"\F0B0B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-d-box-outline',
            'content' =>"\F0BF4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-d-circle',
            'content' =>"\F0BF5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-d-circle-outline',
            'content' =>"\F0BF6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-e',
            'content' =>"\F0AF2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-e-box',
            'content' =>"\F0B0C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-e-box-outline',
            'content' =>"\F0BF7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-e-circle',
            'content' =>"\F0BF8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-e-circle-outline',
            'content' =>"\F0BF9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-f',
            'content' =>"\F0AF3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-f-box',
            'content' =>"\F0B0D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-f-box-outline',
            'content' =>"\F0BFA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-f-circle',
            'content' =>"\F0BFB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-f-circle-outline',
            'content' =>"\F0BFC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-g',
            'content' =>"\F0AF4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-g-box',
            'content' =>"\F0B0E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-g-box-outline',
            'content' =>"\F0BFD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-g-circle',
            'content' =>"\F0BFE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-g-circle-outline',
            'content' =>"\F0BFF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-h',
            'content' =>"\F0AF5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-h-box',
            'content' =>"\F0B0F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-h-box-outline',
            'content' =>"\F0C00"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-h-circle',
            'content' =>"\F0C01"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-h-circle-outline',
            'content' =>"\F0C02"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-i',
            'content' =>"\F0AF6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-i-box',
            'content' =>"\F0B10"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-i-box-outline',
            'content' =>"\F0C03"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-i-circle',
            'content' =>"\F0C04"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-i-circle-outline',
            'content' =>"\F0C05"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-j',
            'content' =>"\F0AF7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-j-box',
            'content' =>"\F0B11"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-j-box-outline',
            'content' =>"\F0C06"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-j-circle',
            'content' =>"\F0C07"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-j-circle-outline',
            'content' =>"\F0C08"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-k',
            'content' =>"\F0AF8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-k-box',
            'content' =>"\F0B12"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-k-box-outline',
            'content' =>"\F0C09"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-k-circle',
            'content' =>"\F0C0A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-k-circle-outline',
            'content' =>"\F0C0B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-l',
            'content' =>"\F0AF9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-l-box',
            'content' =>"\F0B13"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-l-box-outline',
            'content' =>"\F0C0C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-l-circle',
            'content' =>"\F0C0D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-l-circle-outline',
            'content' =>"\F0C0E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-m',
            'content' =>"\F0AFA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-m-box',
            'content' =>"\F0B14"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-m-box-outline',
            'content' =>"\F0C0F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-m-circle',
            'content' =>"\F0C10"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-m-circle-outline',
            'content' =>"\F0C11"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-n',
            'content' =>"\F0AFB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-n-box',
            'content' =>"\F0B15"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-n-box-outline',
            'content' =>"\F0C12"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-n-circle',
            'content' =>"\F0C13"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-n-circle-outline',
            'content' =>"\F0C14"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-o',
            'content' =>"\F0AFC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-o-box',
            'content' =>"\F0B16"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-o-box-outline',
            'content' =>"\F0C15"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-o-circle',
            'content' =>"\F0C16"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-o-circle-outline',
            'content' =>"\F0C17"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-p',
            'content' =>"\F0AFD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-p-box',
            'content' =>"\F0B17"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-p-box-outline',
            'content' =>"\F0C18"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-p-circle',
            'content' =>"\F0C19"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-p-circle-outline',
            'content' =>"\F0C1A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-q',
            'content' =>"\F0AFE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-q-box',
            'content' =>"\F0B18"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-q-box-outline',
            'content' =>"\F0C1B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-q-circle',
            'content' =>"\F0C1C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-q-circle-outline',
            'content' =>"\F0C1D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-r',
            'content' =>"\F0AFF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-r-box',
            'content' =>"\F0B19"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-r-box-outline',
            'content' =>"\F0C1E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-r-circle',
            'content' =>"\F0C1F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-r-circle-outline',
            'content' =>"\F0C20"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-s',
            'content' =>"\F0B00"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-s-box',
            'content' =>"\F0B1A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-s-box-outline',
            'content' =>"\F0C21"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-s-circle',
            'content' =>"\F0C22"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-s-circle-outline',
            'content' =>"\F0C23"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-t',
            'content' =>"\F0B01"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-t-box',
            'content' =>"\F0B1B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-t-box-outline',
            'content' =>"\F0C24"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-t-circle',
            'content' =>"\F0C25"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-t-circle-outline',
            'content' =>"\F0C26"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-u',
            'content' =>"\F0B02"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-u-box',
            'content' =>"\F0B1C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-u-box-outline',
            'content' =>"\F0C27"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-u-circle',
            'content' =>"\F0C28"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-u-circle-outline',
            'content' =>"\F0C29"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-v',
            'content' =>"\F0B03"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-v-box',
            'content' =>"\F0B1D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-v-box-outline',
            'content' =>"\F0C2A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-v-circle',
            'content' =>"\F0C2B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-v-circle-outline',
            'content' =>"\F0C2C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-w',
            'content' =>"\F0B04"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-w-box',
            'content' =>"\F0B1E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-w-box-outline',
            'content' =>"\F0C2D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-w-circle',
            'content' =>"\F0C2E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-w-circle-outline',
            'content' =>"\F0C2F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-x',
            'content' =>"\F0B05"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-x-box',
            'content' =>"\F0B1F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-x-box-outline',
            'content' =>"\F0C30"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-x-circle',
            'content' =>"\F0C31"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-x-circle-outline',
            'content' =>"\F0C32"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-y',
            'content' =>"\F0B06"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-y-box',
            'content' =>"\F0B20"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-y-box-outline',
            'content' =>"\F0C33"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-y-circle',
            'content' =>"\F0C34"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-y-circle-outline',
            'content' =>"\F0C35"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-z',
            'content' =>"\F0B07"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-z-box',
            'content' =>"\F0B21"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-z-box-outline',
            'content' =>"\F0C36"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-z-circle',
            'content' =>"\F0C37"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alpha-z-circle-outline',
            'content' =>"\F0C38"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabet-aurebesh',
            'content' =>"\F132C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabet-cyrillic',
            'content' =>"\F132D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabet-greek',
            'content' =>"\F132E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabet-latin',
            'content' =>"\F132F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabet-piqad',
            'content' =>"\F1330"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabet-tengwar',
            'content' =>"\F1337"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabetical',
            'content' =>"\F002C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabetical-off',
            'content' =>"\F100C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabetical-variant',
            'content' =>"\F100D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'alphabetical-variant-off',
            'content' =>"\F100E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'altimeter',
            'content' =>"\F05D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'amazon',
            'content' =>"\F002D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'amazon-alexa',
            'content' =>"\F08C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ambulance',
            'content' =>"\F002F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ammunition',
            'content' =>"\F0CE8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ampersand',
            'content' =>"\F0A8D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'amplifier',
            'content' =>"\F0030"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'amplifier-off',
            'content' =>"\F11B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'anchor',
            'content' =>"\F0031"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'android',
            'content' =>"\F0032"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'android-auto',
            'content' =>"\F0A8E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'android-debug-bridge',
            'content' =>"\F0033"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'android-messages',
            'content' =>"\F0D45"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'android-studio',
            'content' =>"\F0034"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'angle-acute',
            'content' =>"\F0937"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'angle-obtuse',
            'content' =>"\F0938"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'angle-right',
            'content' =>"\F0939"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'angular',
            'content' =>"\F06B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'angularjs',
            'content' =>"\F06BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'animation',
            'content' =>"\F05D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'animation-outline',
            'content' =>"\F0A8F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'animation-play',
            'content' =>"\F093A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'animation-play-outline',
            'content' =>"\F0A90"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ansible',
            'content' =>"\F109A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'antenna',
            'content' =>"\F1119"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'anvil',
            'content' =>"\F089B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apache-kafka',
            'content' =>"\F100F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'api',
            'content' =>"\F109B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'api-off',
            'content' =>"\F1257"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple',
            'content' =>"\F0035"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-airplay',
            'content' =>"\F001F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-finder',
            'content' =>"\F0036"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-icloud',
            'content' =>"\F0038"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-ios',
            'content' =>"\F0037"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-keyboard-caps',
            'content' =>"\F0632"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-keyboard-command',
            'content' =>"\F0633"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-keyboard-control',
            'content' =>"\F0634"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-keyboard-option',
            'content' =>"\F0635"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-keyboard-shift',
            'content' =>"\F0636"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apple-safari',
            'content' =>"\F0039"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'application',
            'content' =>"\F0614"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'application-export',
            'content' =>"\F0DAD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'application-import',
            'content' =>"\F0DAE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'approximately-equal',
            'content' =>"\F0F9E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'approximately-equal-box',
            'content' =>"\F0F9F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apps',
            'content' =>"\F003B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'apps-box',
            'content' =>"\F0D46"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arch',
            'content' =>"\F08C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'archive',
            'content' =>"\F003C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'archive-arrow-down',
            'content' =>"\F1259"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'archive-arrow-down-outline',
            'content' =>"\F125A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'archive-arrow-up',
            'content' =>"\F125B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'archive-arrow-up-outline',
            'content' =>"\F125C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'archive-outline',
            'content' =>"\F120E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arm-flex',
            'content' =>"\F0FD7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arm-flex-outline',
            'content' =>"\F0FD6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrange-bring-forward',
            'content' =>"\F003D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrange-bring-to-front',
            'content' =>"\F003E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrange-send-backward',
            'content' =>"\F003F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrange-send-to-back',
            'content' =>"\F0040"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-all',
            'content' =>"\F0041"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-bottom-left',
            'content' =>"\F0042"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-bottom-left-bold-outline',
            'content' =>"\F09B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-bottom-left-thick',
            'content' =>"\F09B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-bottom-right',
            'content' =>"\F0043"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-bottom-right-bold-outline',
            'content' =>"\F09B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-bottom-right-thick',
            'content' =>"\F09BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-collapse',
            'content' =>"\F0615"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-collapse-all',
            'content' =>"\F0044"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-collapse-down',
            'content' =>"\F0792"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-collapse-horizontal',
            'content' =>"\F084C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-collapse-left',
            'content' =>"\F0793"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-collapse-right',
            'content' =>"\F0794"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-collapse-up',
            'content' =>"\F0795"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-collapse-vertical',
            'content' =>"\F084D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-decision',
            'content' =>"\F09BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-decision-auto',
            'content' =>"\F09BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-decision-auto-outline',
            'content' =>"\F09BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-decision-outline',
            'content' =>"\F09BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down',
            'content' =>"\F0045"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-bold',
            'content' =>"\F072E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-bold-box',
            'content' =>"\F072F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-bold-box-outline',
            'content' =>"\F0730"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-bold-circle',
            'content' =>"\F0047"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-bold-circle-outline',
            'content' =>"\F0048"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-bold-hexagon-outline',
            'content' =>"\F0049"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-bold-outline',
            'content' =>"\F09BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-box',
            'content' =>"\F06C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-circle',
            'content' =>"\F0CDB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-circle-outline',
            'content' =>"\F0CDC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-drop-circle',
            'content' =>"\F004A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-drop-circle-outline',
            'content' =>"\F004B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-down-thick',
            'content' =>"\F0046"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-expand',
            'content' =>"\F0616"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-expand-all',
            'content' =>"\F004C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-expand-down',
            'content' =>"\F0796"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-expand-horizontal',
            'content' =>"\F084E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-expand-left',
            'content' =>"\F0797"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-expand-right',
            'content' =>"\F0798"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-expand-up',
            'content' =>"\F0799"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-expand-vertical',
            'content' =>"\F084F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-horizontal-lock',
            'content' =>"\F115B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left',
            'content' =>"\F004D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-bold',
            'content' =>"\F0731"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-bold-box',
            'content' =>"\F0732"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-bold-box-outline',
            'content' =>"\F0733"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-bold-circle',
            'content' =>"\F004F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-bold-circle-outline',
            'content' =>"\F0050"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-bold-hexagon-outline',
            'content' =>"\F0051"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-bold-outline',
            'content' =>"\F09C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-box',
            'content' =>"\F06C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-circle',
            'content' =>"\F0CDD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-circle-outline',
            'content' =>"\F0CDE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-drop-circle',
            'content' =>"\F0052"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-drop-circle-outline',
            'content' =>"\F0053"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-right',
            'content' =>"\F0E73"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-right-bold',
            'content' =>"\F0E74"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-right-bold-outline',
            'content' =>"\F09C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-left-thick',
            'content' =>"\F004E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right',
            'content' =>"\F0054"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-bold',
            'content' =>"\F0734"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-bold-box',
            'content' =>"\F0735"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-bold-box-outline',
            'content' =>"\F0736"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-bold-circle',
            'content' =>"\F0056"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-bold-circle-outline',
            'content' =>"\F0057"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-bold-hexagon-outline',
            'content' =>"\F0058"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-bold-outline',
            'content' =>"\F09C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-box',
            'content' =>"\F06C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-circle',
            'content' =>"\F0CDF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-circle-outline',
            'content' =>"\F0CE0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-drop-circle',
            'content' =>"\F0059"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-drop-circle-outline',
            'content' =>"\F005A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-right-thick',
            'content' =>"\F0055"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-split-horizontal',
            'content' =>"\F093B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-split-vertical',
            'content' =>"\F093C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-left',
            'content' =>"\F005B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-left-bold-outline',
            'content' =>"\F09C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-left-bottom-right',
            'content' =>"\F0E75"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-left-bottom-right-bold',
            'content' =>"\F0E76"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-left-thick',
            'content' =>"\F09C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-right',
            'content' =>"\F005C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-right-bold-outline',
            'content' =>"\F09C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-right-bottom-left',
            'content' =>"\F0E77"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-right-bottom-left-bold',
            'content' =>"\F0E78"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-top-right-thick',
            'content' =>"\F09C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up',
            'content' =>"\F005D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-bold',
            'content' =>"\F0737"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-bold-box',
            'content' =>"\F0738"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-bold-box-outline',
            'content' =>"\F0739"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-bold-circle',
            'content' =>"\F005F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-bold-circle-outline',
            'content' =>"\F0060"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-bold-hexagon-outline',
            'content' =>"\F0061"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-bold-outline',
            'content' =>"\F09C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-box',
            'content' =>"\F06C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-circle',
            'content' =>"\F0CE1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-circle-outline',
            'content' =>"\F0CE2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-down',
            'content' =>"\F0E79"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-down-bold',
            'content' =>"\F0E7A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-down-bold-outline',
            'content' =>"\F09C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-drop-circle',
            'content' =>"\F0062"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-drop-circle-outline',
            'content' =>"\F0063"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-up-thick',
            'content' =>"\F005E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'arrow-vertical-lock',
            'content' =>"\F115C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'artstation',
            'content' =>"\F0B5B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'aspect-ratio',
            'content' =>"\F0A24"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'assistant',
            'content' =>"\F0064"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'asterisk',
            'content' =>"\F06C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'at',
            'content' =>"\F0065"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'atlassian',
            'content' =>"\F0804"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'atm',
            'content' =>"\F0D47"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'atom',
            'content' =>"\F0768"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'atom-variant',
            'content' =>"\F0E7B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'attachment',
            'content' =>"\F0066"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'audio-video',
            'content' =>"\F093D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'audio-video-off',
            'content' =>"\F11B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'augmented-reality',
            'content' =>"\F0850"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'auto-download',
            'content' =>"\F137E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'auto-fix',
            'content' =>"\F0068"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'auto-upload',
            'content' =>"\F0069"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'autorenew',
            'content' =>"\F006A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'av-timer',
            'content' =>"\F006B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'aws',
            'content' =>"\F0E0F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axe',
            'content' =>"\F08C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis',
            'content' =>"\F0D48"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-arrow',
            'content' =>"\F0D49"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-arrow-info',
            'content' =>"\F140E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-arrow-lock',
            'content' =>"\F0D4A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-lock',
            'content' =>"\F0D4B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-x-arrow',
            'content' =>"\F0D4C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-x-arrow-lock',
            'content' =>"\F0D4D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-x-rotate-clockwise',
            'content' =>"\F0D4E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-x-rotate-counterclockwise',
            'content' =>"\F0D4F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-x-y-arrow-lock',
            'content' =>"\F0D50"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-y-arrow',
            'content' =>"\F0D51"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-y-arrow-lock',
            'content' =>"\F0D52"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-y-rotate-clockwise',
            'content' =>"\F0D53"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-y-rotate-counterclockwise',
            'content' =>"\F0D54"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-z-arrow',
            'content' =>"\F0D55"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-z-arrow-lock',
            'content' =>"\F0D56"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-z-rotate-clockwise',
            'content' =>"\F0D57"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'axis-z-rotate-counterclockwise',
            'content' =>"\F0D58"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'babel',
            'content' =>"\F0A25"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baby',
            'content' =>"\F006C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baby-bottle',
            'content' =>"\F0F39"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baby-bottle-outline',
            'content' =>"\F0F3A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baby-buggy',
            'content' =>"\F13E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baby-carriage',
            'content' =>"\F068F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baby-carriage-off',
            'content' =>"\F0FA0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baby-face',
            'content' =>"\F0E7C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baby-face-outline',
            'content' =>"\F0E7D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'backburger',
            'content' =>"\F006D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'backspace',
            'content' =>"\F006E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'backspace-outline',
            'content' =>"\F0B5C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'backspace-reverse',
            'content' =>"\F0E7E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'backspace-reverse-outline',
            'content' =>"\F0E7F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'backup-restore',
            'content' =>"\F006F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bacteria',
            'content' =>"\F0ED5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bacteria-outline',
            'content' =>"\F0ED6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'badge-account',
            'content' =>"\F0DA7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'badge-account-alert',
            'content' =>"\F0DA8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'badge-account-alert-outline',
            'content' =>"\F0DA9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'badge-account-horizontal',
            'content' =>"\F0E0D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'badge-account-horizontal-outline',
            'content' =>"\F0E0E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'badge-account-outline',
            'content' =>"\F0DAA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'badminton',
            'content' =>"\F0851"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bag-carry-on',
            'content' =>"\F0F3B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bag-carry-on-check',
            'content' =>"\F0D65"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bag-carry-on-off',
            'content' =>"\F0F3C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bag-checked',
            'content' =>"\F0F3D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bag-personal',
            'content' =>"\F0E10"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bag-personal-off',
            'content' =>"\F0E11"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bag-personal-off-outline',
            'content' =>"\F0E12"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bag-personal-outline',
            'content' =>"\F0E13"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baguette',
            'content' =>"\F0F3E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'balloon',
            'content' =>"\F0A26"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ballot',
            'content' =>"\F09C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ballot-outline',
            'content' =>"\F09CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ballot-recount',
            'content' =>"\F0C39"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ballot-recount-outline',
            'content' =>"\F0C3A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bandage',
            'content' =>"\F0DAF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bandcamp',
            'content' =>"\F0675"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bank',
            'content' =>"\F0070"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bank-minus',
            'content' =>"\F0DB0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bank-outline',
            'content' =>"\F0E80"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bank-plus',
            'content' =>"\F0DB1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bank-remove',
            'content' =>"\F0DB2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bank-transfer',
            'content' =>"\F0A27"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bank-transfer-in',
            'content' =>"\F0A28"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bank-transfer-out',
            'content' =>"\F0A29"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'barcode',
            'content' =>"\F0071"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'barcode-off',
            'content' =>"\F1236"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'barcode-scan',
            'content' =>"\F0072"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'barley',
            'content' =>"\F0073"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'barley-off',
            'content' =>"\F0B5D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'barn',
            'content' =>"\F0B5E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'barrel',
            'content' =>"\F0074"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baseball',
            'content' =>"\F0852"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'baseball-bat',
            'content' =>"\F0853"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bash',
            'content' =>"\F1183"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'basket',
            'content' =>"\F0076"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'basket-fill',
            'content' =>"\F0077"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'basket-outline',
            'content' =>"\F1181"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'basket-unfill',
            'content' =>"\F0078"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'basketball',
            'content' =>"\F0806"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'basketball-hoop',
            'content' =>"\F0C3B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'basketball-hoop-outline',
            'content' =>"\F0C3C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bat',
            'content' =>"\F0B5F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery',
            'content' =>"\F0079"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-10',
            'content' =>"\F007A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-10-bluetooth',
            'content' =>"\F093E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-20',
            'content' =>"\F007B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-20-bluetooth',
            'content' =>"\F093F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-30',
            'content' =>"\F007C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-30-bluetooth',
            'content' =>"\F0940"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-40',
            'content' =>"\F007D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-40-bluetooth',
            'content' =>"\F0941"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-50',
            'content' =>"\F007E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-50-bluetooth',
            'content' =>"\F0942"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-60',
            'content' =>"\F007F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-60-bluetooth',
            'content' =>"\F0943"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-70',
            'content' =>"\F0080"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-70-bluetooth',
            'content' =>"\F0944"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-80',
            'content' =>"\F0081"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-80-bluetooth',
            'content' =>"\F0945"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-90',
            'content' =>"\F0082"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-90-bluetooth',
            'content' =>"\F0946"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-alert',
            'content' =>"\F0083"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-alert-bluetooth',
            'content' =>"\F0947"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-alert-variant',
            'content' =>"\F10CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-alert-variant-outline',
            'content' =>"\F10CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-bluetooth',
            'content' =>"\F0948"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-bluetooth-variant',
            'content' =>"\F0949"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging',
            'content' =>"\F0084"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-10',
            'content' =>"\F089C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-100',
            'content' =>"\F0085"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-20',
            'content' =>"\F0086"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-30',
            'content' =>"\F0087"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-40',
            'content' =>"\F0088"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-50',
            'content' =>"\F089D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-60',
            'content' =>"\F0089"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-70',
            'content' =>"\F089E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-80',
            'content' =>"\F008A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-90',
            'content' =>"\F008B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-high',
            'content' =>"\F12A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-low',
            'content' =>"\F12A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-medium',
            'content' =>"\F12A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-outline',
            'content' =>"\F089F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless',
            'content' =>"\F0807"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-10',
            'content' =>"\F0808"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-20',
            'content' =>"\F0809"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-30',
            'content' =>"\F080A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-40',
            'content' =>"\F080B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-50',
            'content' =>"\F080C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-60',
            'content' =>"\F080D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-70',
            'content' =>"\F080E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-80',
            'content' =>"\F080F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-90',
            'content' =>"\F0810"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-alert',
            'content' =>"\F0811"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-charging-wireless-outline',
            'content' =>"\F0812"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-heart',
            'content' =>"\F120F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-heart-outline',
            'content' =>"\F1210"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-heart-variant',
            'content' =>"\F1211"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-high',
            'content' =>"\F12A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-low',
            'content' =>"\F12A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-medium',
            'content' =>"\F12A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-minus',
            'content' =>"\F008C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-negative',
            'content' =>"\F008D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-off',
            'content' =>"\F125D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-off-outline',
            'content' =>"\F125E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-outline',
            'content' =>"\F008E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-plus',
            'content' =>"\F008F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-positive',
            'content' =>"\F0090"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-unknown',
            'content' =>"\F0091"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battery-unknown-bluetooth',
            'content' =>"\F094A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'battlenet',
            'content' =>"\F0B60"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beach',
            'content' =>"\F0092"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker',
            'content' =>"\F0CEA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-alert',
            'content' =>"\F1229"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-alert-outline',
            'content' =>"\F122A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-check',
            'content' =>"\F122B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-check-outline',
            'content' =>"\F122C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-minus',
            'content' =>"\F122D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-minus-outline',
            'content' =>"\F122E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-outline',
            'content' =>"\F0690"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-plus',
            'content' =>"\F122F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-plus-outline',
            'content' =>"\F1230"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-question',
            'content' =>"\F1231"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-question-outline',
            'content' =>"\F1232"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-remove',
            'content' =>"\F1233"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beaker-remove-outline',
            'content' =>"\F1234"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed',
            'content' =>"\F02E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-double',
            'content' =>"\F0FD4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-double-outline',
            'content' =>"\F0FD3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-empty',
            'content' =>"\F08A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-king',
            'content' =>"\F0FD2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-king-outline',
            'content' =>"\F0FD1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-outline',
            'content' =>"\F0099"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-queen',
            'content' =>"\F0FD0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-queen-outline',
            'content' =>"\F0FDB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-single',
            'content' =>"\F106D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bed-single-outline',
            'content' =>"\F106E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bee',
            'content' =>"\F0FA1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bee-flower',
            'content' =>"\F0FA2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beehive-off-outline',
            'content' =>"\F13ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beehive-outline',
            'content' =>"\F10CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beer',
            'content' =>"\F0098"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beer-outline',
            'content' =>"\F130C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell',
            'content' =>"\F009A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-alert',
            'content' =>"\F0D59"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-alert-outline',
            'content' =>"\F0E81"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-cancel',
            'content' =>"\F13E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-cancel-outline',
            'content' =>"\F13E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-check',
            'content' =>"\F11E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-check-outline',
            'content' =>"\F11E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-circle',
            'content' =>"\F0D5A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-circle-outline',
            'content' =>"\F0D5B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-minus',
            'content' =>"\F13E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-minus-outline',
            'content' =>"\F13EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-off',
            'content' =>"\F009B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-off-outline',
            'content' =>"\F0A91"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-outline',
            'content' =>"\F009C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-plus',
            'content' =>"\F009D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-plus-outline',
            'content' =>"\F0A92"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-remove',
            'content' =>"\F13EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-remove-outline',
            'content' =>"\F13EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-ring',
            'content' =>"\F009E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-ring-outline',
            'content' =>"\F009F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-sleep',
            'content' =>"\F00A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bell-sleep-outline',
            'content' =>"\F0A93"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'beta',
            'content' =>"\F00A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'betamax',
            'content' =>"\F09CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'biathlon',
            'content' =>"\F0E14"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bicycle',
            'content' =>"\F109C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bicycle-basket',
            'content' =>"\F1235"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bike',
            'content' =>"\F00A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bike-fast',
            'content' =>"\F111F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'billboard',
            'content' =>"\F1010"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'billiards',
            'content' =>"\F0B61"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'billiards-rack',
            'content' =>"\F0B62"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'binoculars',
            'content' =>"\F00A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bio',
            'content' =>"\F00A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'biohazard',
            'content' =>"\F00A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bitbucket',
            'content' =>"\F00A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bitcoin',
            'content' =>"\F0813"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'black-mesa',
            'content' =>"\F00A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blender',
            'content' =>"\F0CEB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blender-software',
            'content' =>"\F00AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blinds',
            'content' =>"\F00AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blinds-open',
            'content' =>"\F1011"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'block-helper',
            'content' =>"\F00AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blogger',
            'content' =>"\F00AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blood-bag',
            'content' =>"\F0CEC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bluetooth',
            'content' =>"\F00AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bluetooth-audio',
            'content' =>"\F00B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bluetooth-connect',
            'content' =>"\F00B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bluetooth-off',
            'content' =>"\F00B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bluetooth-settings',
            'content' =>"\F00B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bluetooth-transfer',
            'content' =>"\F00B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blur',
            'content' =>"\F00B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blur-linear',
            'content' =>"\F00B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blur-off',
            'content' =>"\F00B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blur-radial',
            'content' =>"\F00B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bolnisi-cross',
            'content' =>"\F0CED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bolt',
            'content' =>"\F0DB3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bomb',
            'content' =>"\F0691"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bomb-off',
            'content' =>"\F06C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bone',
            'content' =>"\F00B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book',
            'content' =>"\F00BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-account',
            'content' =>"\F13AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-account-outline',
            'content' =>"\F13AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-alphabet',
            'content' =>"\F061D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-cross',
            'content' =>"\F00A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-information-variant',
            'content' =>"\F106F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-lock',
            'content' =>"\F079A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-lock-open',
            'content' =>"\F079B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-minus',
            'content' =>"\F05D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-minus-multiple',
            'content' =>"\F0A94"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-minus-multiple-outline',
            'content' =>"\F090B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-multiple',
            'content' =>"\F00BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-multiple-outline',
            'content' =>"\F0436"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-music',
            'content' =>"\F0067"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-open',
            'content' =>"\F00BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-open-outline',
            'content' =>"\F0B63"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-open-page-variant',
            'content' =>"\F05DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-open-variant',
            'content' =>"\F00BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-outline',
            'content' =>"\F0B64"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-play',
            'content' =>"\F0E82"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-play-outline',
            'content' =>"\F0E83"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-plus',
            'content' =>"\F05DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-plus-multiple',
            'content' =>"\F0A95"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-plus-multiple-outline',
            'content' =>"\F0ADE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-remove',
            'content' =>"\F0A97"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-remove-multiple',
            'content' =>"\F0A96"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-remove-multiple-outline',
            'content' =>"\F04CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-search',
            'content' =>"\F0E84"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-search-outline',
            'content' =>"\F0E85"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-variant',
            'content' =>"\F00BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'book-variant-multiple',
            'content' =>"\F00BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark',
            'content' =>"\F00C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-check',
            'content' =>"\F00C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-check-outline',
            'content' =>"\F137B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-minus',
            'content' =>"\F09CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-minus-outline',
            'content' =>"\F09CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-multiple',
            'content' =>"\F0E15"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-multiple-outline',
            'content' =>"\F0E16"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-music',
            'content' =>"\F00C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-music-outline',
            'content' =>"\F1379"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-off',
            'content' =>"\F09CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-off-outline',
            'content' =>"\F09CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-outline',
            'content' =>"\F00C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-plus',
            'content' =>"\F00C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-plus-outline',
            'content' =>"\F00C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-remove',
            'content' =>"\F00C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookmark-remove-outline',
            'content' =>"\F137A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bookshelf',
            'content' =>"\F125F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boom-gate',
            'content' =>"\F0E86"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boom-gate-alert',
            'content' =>"\F0E87"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boom-gate-alert-outline',
            'content' =>"\F0E88"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boom-gate-down',
            'content' =>"\F0E89"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boom-gate-down-outline',
            'content' =>"\F0E8A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boom-gate-outline',
            'content' =>"\F0E8B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boom-gate-up',
            'content' =>"\F0E8C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boom-gate-up-outline',
            'content' =>"\F0E8D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boombox',
            'content' =>"\F05DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boomerang',
            'content' =>"\F10CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bootstrap',
            'content' =>"\F06C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-all',
            'content' =>"\F00C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-all-variant',
            'content' =>"\F08A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-bottom',
            'content' =>"\F00C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-bottom-variant',
            'content' =>"\F08A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-color',
            'content' =>"\F00C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-horizontal',
            'content' =>"\F00CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-inside',
            'content' =>"\F00CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-left',
            'content' =>"\F00CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-left-variant',
            'content' =>"\F08A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-none',
            'content' =>"\F00CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-none-variant',
            'content' =>"\F08A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-outside',
            'content' =>"\F00CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-right',
            'content' =>"\F00CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-right-variant',
            'content' =>"\F08A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-style',
            'content' =>"\F00D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-top',
            'content' =>"\F00D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-top-variant',
            'content' =>"\F08A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'border-vertical',
            'content' =>"\F00D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-soda',
            'content' =>"\F1070"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-soda-classic',
            'content' =>"\F1071"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-soda-classic-outline',
            'content' =>"\F1363"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-soda-outline',
            'content' =>"\F1072"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-tonic',
            'content' =>"\F112E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-tonic-outline',
            'content' =>"\F112F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-tonic-plus',
            'content' =>"\F1130"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-tonic-plus-outline',
            'content' =>"\F1131"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-tonic-skull',
            'content' =>"\F1132"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-tonic-skull-outline',
            'content' =>"\F1133"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-wine',
            'content' =>"\F0854"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bottle-wine-outline',
            'content' =>"\F1310"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bow-tie',
            'content' =>"\F0678"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bowl',
            'content' =>"\F028E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bowl-mix',
            'content' =>"\F0617"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bowl-mix-outline',
            'content' =>"\F02E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bowl-outline',
            'content' =>"\F02A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bowling',
            'content' =>"\F00D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'box',
            'content' =>"\F00D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'box-cutter',
            'content' =>"\F00D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'box-cutter-off',
            'content' =>"\F0B4A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'box-shadow',
            'content' =>"\F0637"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'boxing-glove',
            'content' =>"\F0B65"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'braille',
            'content' =>"\F09D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brain',
            'content' =>"\F09D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bread-slice',
            'content' =>"\F0CEE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bread-slice-outline',
            'content' =>"\F0CEF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bridge',
            'content' =>"\F0618"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase',
            'content' =>"\F00D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-account',
            'content' =>"\F0CF0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-account-outline',
            'content' =>"\F0CF1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-check',
            'content' =>"\F00D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-check-outline',
            'content' =>"\F131E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-clock',
            'content' =>"\F10D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-clock-outline',
            'content' =>"\F10D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-download',
            'content' =>"\F00D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-download-outline',
            'content' =>"\F0C3D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-edit',
            'content' =>"\F0A98"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-edit-outline',
            'content' =>"\F0C3E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-minus',
            'content' =>"\F0A2A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-minus-outline',
            'content' =>"\F0C3F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-outline',
            'content' =>"\F0814"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-plus',
            'content' =>"\F0A2B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-plus-outline',
            'content' =>"\F0C40"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-remove',
            'content' =>"\F0A2C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-remove-outline',
            'content' =>"\F0C41"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-search',
            'content' =>"\F0A2D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-search-outline',
            'content' =>"\F0C42"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-upload',
            'content' =>"\F00D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-upload-outline',
            'content' =>"\F0C43"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-variant',
            'content' =>"\F1494"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'briefcase-variant-outline',
            'content' =>"\F1495"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brightness-1',
            'content' =>"\F00DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brightness-2',
            'content' =>"\F00DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brightness-3',
            'content' =>"\F00DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brightness-4',
            'content' =>"\F00DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brightness-5',
            'content' =>"\F00DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brightness-6',
            'content' =>"\F00DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brightness-7',
            'content' =>"\F00E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brightness-auto',
            'content' =>"\F00E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brightness-percent',
            'content' =>"\F0CF2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'broom',
            'content' =>"\F00E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'brush',
            'content' =>"\F00E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bucket',
            'content' =>"\F1415"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bucket-outline',
            'content' =>"\F1416"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'buddhism',
            'content' =>"\F094B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'buffer',
            'content' =>"\F0619"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'buffet',
            'content' =>"\F0578"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bug',
            'content' =>"\F00E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bug-check',
            'content' =>"\F0A2E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bug-check-outline',
            'content' =>"\F0A2F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bug-outline',
            'content' =>"\F0A30"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bugle',
            'content' =>"\F0DB4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bulldozer',
            'content' =>"\F0B22"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bullet',
            'content' =>"\F0CF3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bulletin-board',
            'content' =>"\F00E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bullhorn',
            'content' =>"\F00E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bullhorn-outline',
            'content' =>"\F0B23"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bullseye',
            'content' =>"\F05DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bullseye-arrow',
            'content' =>"\F08C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bulma',
            'content' =>"\F12E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bunk-bed',
            'content' =>"\F1302"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bunk-bed-outline',
            'content' =>"\F0097"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus',
            'content' =>"\F00E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-alert',
            'content' =>"\F0A99"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-articulated-end',
            'content' =>"\F079C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-articulated-front',
            'content' =>"\F079D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-clock',
            'content' =>"\F08CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-double-decker',
            'content' =>"\F079E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-marker',
            'content' =>"\F1212"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-multiple',
            'content' =>"\F0F3F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-school',
            'content' =>"\F079F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-side',
            'content' =>"\F07A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-stop',
            'content' =>"\F1012"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-stop-covered',
            'content' =>"\F1013"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'bus-stop-uncovered',
            'content' =>"\F1014"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cable-data',
            'content' =>"\F1394"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cached',
            'content' =>"\F00E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cactus',
            'content' =>"\F0DB5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cake',
            'content' =>"\F00E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cake-layered',
            'content' =>"\F00EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cake-variant',
            'content' =>"\F00EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calculator',
            'content' =>"\F00EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calculator-variant',
            'content' =>"\F0A9A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar',
            'content' =>"\F00ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-account',
            'content' =>"\F0ED7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-account-outline',
            'content' =>"\F0ED8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-alert',
            'content' =>"\F0A31"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-arrow-left',
            'content' =>"\F1134"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-arrow-right',
            'content' =>"\F1135"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-blank',
            'content' =>"\F00EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-blank-multiple',
            'content' =>"\F1073"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-blank-outline',
            'content' =>"\F0B66"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-check',
            'content' =>"\F00EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-check-outline',
            'content' =>"\F0C44"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-clock',
            'content' =>"\F00F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-edit',
            'content' =>"\F08A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-export',
            'content' =>"\F0B24"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-heart',
            'content' =>"\F09D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-import',
            'content' =>"\F0B25"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-minus',
            'content' =>"\F0D5C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-month',
            'content' =>"\F0E17"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-month-outline',
            'content' =>"\F0E18"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-multiple',
            'content' =>"\F00F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-multiple-check',
            'content' =>"\F00F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-multiselect',
            'content' =>"\F0A32"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-outline',
            'content' =>"\F0B67"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-plus',
            'content' =>"\F00F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-question',
            'content' =>"\F0692"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-range',
            'content' =>"\F0679"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-range-outline',
            'content' =>"\F0B68"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-refresh',
            'content' =>"\F01E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-refresh-outline',
            'content' =>"\F0203"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-remove',
            'content' =>"\F00F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-remove-outline',
            'content' =>"\F0C45"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-search',
            'content' =>"\F094C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-star',
            'content' =>"\F09D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-sync',
            'content' =>"\F0E8E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-sync-outline',
            'content' =>"\F0E8F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-text',
            'content' =>"\F00F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-text-outline',
            'content' =>"\F0C46"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-today',
            'content' =>"\F00F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-week',
            'content' =>"\F0A33"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-week-begin',
            'content' =>"\F0A34"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-weekend',
            'content' =>"\F0ED9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'calendar-weekend-outline',
            'content' =>"\F0EDA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'call-made',
            'content' =>"\F00F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'call-merge',
            'content' =>"\F00F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'call-missed',
            'content' =>"\F00F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'call-received',
            'content' =>"\F00FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'call-split',
            'content' =>"\F00FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camcorder',
            'content' =>"\F00FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camcorder-off',
            'content' =>"\F00FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera',
            'content' =>"\F0100"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-account',
            'content' =>"\F08CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-burst',
            'content' =>"\F0693"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-control',
            'content' =>"\F0B69"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-enhance',
            'content' =>"\F0101"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-enhance-outline',
            'content' =>"\F0B6A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-front',
            'content' =>"\F0102"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-front-variant',
            'content' =>"\F0103"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-gopro',
            'content' =>"\F07A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-image',
            'content' =>"\F08CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-iris',
            'content' =>"\F0104"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-metering-center',
            'content' =>"\F07A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-metering-matrix',
            'content' =>"\F07A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-metering-partial',
            'content' =>"\F07A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-metering-spot',
            'content' =>"\F07A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-off',
            'content' =>"\F05DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-outline',
            'content' =>"\F0D5D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-party-mode',
            'content' =>"\F0105"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-plus',
            'content' =>"\F0EDB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-plus-outline',
            'content' =>"\F0EDC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-rear',
            'content' =>"\F0106"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-rear-variant',
            'content' =>"\F0107"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-retake',
            'content' =>"\F0E19"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-retake-outline',
            'content' =>"\F0E1A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-switch',
            'content' =>"\F0108"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-switch-outline',
            'content' =>"\F084A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-timer',
            'content' =>"\F0109"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-wireless',
            'content' =>"\F0DB6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'camera-wireless-outline',
            'content' =>"\F0DB7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'campfire',
            'content' =>"\F0EDD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cancel',
            'content' =>"\F073A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'candle',
            'content' =>"\F05E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'candycane',
            'content' =>"\F010A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cannabis',
            'content' =>"\F07A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'caps-lock',
            'content' =>"\F0A9B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car',
            'content' =>"\F010B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-2-plus',
            'content' =>"\F1015"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-3-plus',
            'content' =>"\F1016"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-arrow-left',
            'content' =>"\F13B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-arrow-right',
            'content' =>"\F13B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-back',
            'content' =>"\F0E1B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-battery',
            'content' =>"\F010C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-brake-abs',
            'content' =>"\F0C47"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-brake-alert',
            'content' =>"\F0C48"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-brake-hold',
            'content' =>"\F0D5E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-brake-parking',
            'content' =>"\F0D5F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-brake-retarder',
            'content' =>"\F1017"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-child-seat',
            'content' =>"\F0FA3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-clutch',
            'content' =>"\F1018"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-cog',
            'content' =>"\F13CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-connected',
            'content' =>"\F010D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-convertible',
            'content' =>"\F07A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-coolant-level',
            'content' =>"\F1019"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-cruise-control',
            'content' =>"\F0D60"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-defrost-front',
            'content' =>"\F0D61"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-defrost-rear',
            'content' =>"\F0D62"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-door',
            'content' =>"\F0B6B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-door-lock',
            'content' =>"\F109D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-electric',
            'content' =>"\F0B6C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-esp',
            'content' =>"\F0C49"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-estate',
            'content' =>"\F07A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-hatchback',
            'content' =>"\F07A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-info',
            'content' =>"\F11BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-key',
            'content' =>"\F0B6D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-light-dimmed',
            'content' =>"\F0C4A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-light-fog',
            'content' =>"\F0C4B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-light-high',
            'content' =>"\F0C4C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-limousine',
            'content' =>"\F08CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-multiple',
            'content' =>"\F0B6E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-off',
            'content' =>"\F0E1C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-parking-lights',
            'content' =>"\F0D63"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-pickup',
            'content' =>"\F07AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-seat',
            'content' =>"\F0FA4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-seat-cooler',
            'content' =>"\F0FA5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-seat-heater',
            'content' =>"\F0FA6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-settings',
            'content' =>"\F13CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-shift-pattern',
            'content' =>"\F0F40"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-side',
            'content' =>"\F07AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-sports',
            'content' =>"\F07AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-tire-alert',
            'content' =>"\F0C4D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-traction-control',
            'content' =>"\F0D64"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-turbocharger',
            'content' =>"\F101A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-wash',
            'content' =>"\F010E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-windshield',
            'content' =>"\F101B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'car-windshield-outline',
            'content' =>"\F101C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'carabiner',
            'content' =>"\F14C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'caravan',
            'content' =>"\F07AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card',
            'content' =>"\F0B6F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-account-details',
            'content' =>"\F05D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-account-details-outline',
            'content' =>"\F0DAB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-account-details-star',
            'content' =>"\F02A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-account-details-star-outline',
            'content' =>"\F06DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-account-mail',
            'content' =>"\F018E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-account-mail-outline',
            'content' =>"\F0E98"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-account-phone',
            'content' =>"\F0E99"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-account-phone-outline',
            'content' =>"\F0E9A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-bulleted',
            'content' =>"\F0B70"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-bulleted-off',
            'content' =>"\F0B71"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-bulleted-off-outline',
            'content' =>"\F0B72"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-bulleted-outline',
            'content' =>"\F0B73"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-bulleted-settings',
            'content' =>"\F0B74"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-bulleted-settings-outline',
            'content' =>"\F0B75"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-outline',
            'content' =>"\F0B76"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-plus',
            'content' =>"\F11FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-plus-outline',
            'content' =>"\F1200"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-search',
            'content' =>"\F1074"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-search-outline',
            'content' =>"\F1075"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-text',
            'content' =>"\F0B77"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'card-text-outline',
            'content' =>"\F0B78"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cards',
            'content' =>"\F0638"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cards-club',
            'content' =>"\F08CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cards-diamond',
            'content' =>"\F08CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cards-diamond-outline',
            'content' =>"\F101D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cards-heart',
            'content' =>"\F08D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cards-outline',
            'content' =>"\F0639"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cards-playing-outline',
            'content' =>"\F063A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cards-spade',
            'content' =>"\F08D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cards-variant',
            'content' =>"\F06C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'carrot',
            'content' =>"\F010F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cart',
            'content' =>"\F0110"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cart-arrow-down',
            'content' =>"\F0D66"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cart-arrow-right',
            'content' =>"\F0C4E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cart-arrow-up',
            'content' =>"\F0D67"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cart-minus',
            'content' =>"\F0D68"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cart-off',
            'content' =>"\F066B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cart-outline',
            'content' =>"\F0111"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cart-plus',
            'content' =>"\F0112"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cart-remove',
            'content' =>"\F0D69"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'case-sensitive-alt',
            'content' =>"\F0113"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash',
            'content' =>"\F0114"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-100',
            'content' =>"\F0115"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-marker',
            'content' =>"\F0DB8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-minus',
            'content' =>"\F1260"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-multiple',
            'content' =>"\F0116"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-plus',
            'content' =>"\F1261"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-refund',
            'content' =>"\F0A9C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-register',
            'content' =>"\F0CF4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-remove',
            'content' =>"\F1262"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-usd',
            'content' =>"\F1176"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cash-usd-outline',
            'content' =>"\F0117"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cassette',
            'content' =>"\F09D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cast',
            'content' =>"\F0118"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cast-audio',
            'content' =>"\F101E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cast-connected',
            'content' =>"\F0119"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cast-education',
            'content' =>"\F0E1D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cast-off',
            'content' =>"\F078A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'castle',
            'content' =>"\F011A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cat',
            'content' =>"\F011B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cctv',
            'content' =>"\F07AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ceiling-light',
            'content' =>"\F0769"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone',
            'content' =>"\F011C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-android',
            'content' =>"\F011D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-arrow-down',
            'content' =>"\F09D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-basic',
            'content' =>"\F011E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-charging',
            'content' =>"\F1397"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-cog',
            'content' =>"\F0951"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-dock',
            'content' =>"\F011F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-erase',
            'content' =>"\F094D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-information',
            'content' =>"\F0F41"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-iphone',
            'content' =>"\F0120"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-key',
            'content' =>"\F094E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-link',
            'content' =>"\F0121"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-link-off',
            'content' =>"\F0122"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-lock',
            'content' =>"\F094F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-message',
            'content' =>"\F08D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-message-off',
            'content' =>"\F10D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-nfc',
            'content' =>"\F0E90"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-nfc-off',
            'content' =>"\F12D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-off',
            'content' =>"\F0950"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-play',
            'content' =>"\F101F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-screenshot',
            'content' =>"\F0A35"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-settings',
            'content' =>"\F0123"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-sound',
            'content' =>"\F0952"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-text',
            'content' =>"\F08D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cellphone-wireless',
            'content' =>"\F0815"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'celtic-cross',
            'content' =>"\F0CF5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'centos',
            'content' =>"\F111A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'certificate',
            'content' =>"\F0124"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'certificate-outline',
            'content' =>"\F1188"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chair-rolling',
            'content' =>"\F0F48"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chair-school',
            'content' =>"\F0125"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'charity',
            'content' =>"\F0C4F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-arc',
            'content' =>"\F0126"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-areaspline',
            'content' =>"\F0127"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-areaspline-variant',
            'content' =>"\F0E91"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-bar',
            'content' =>"\F0128"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-bar-stacked',
            'content' =>"\F076A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-bell-curve',
            'content' =>"\F0C50"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-bell-curve-cumulative',
            'content' =>"\F0FA7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-bubble',
            'content' =>"\F05E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-donut',
            'content' =>"\F07AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-donut-variant',
            'content' =>"\F07B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-gantt',
            'content' =>"\F066C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-histogram',
            'content' =>"\F0129"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-line',
            'content' =>"\F012A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-line-stacked',
            'content' =>"\F076B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-line-variant',
            'content' =>"\F07B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-multiline',
            'content' =>"\F08D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-multiple',
            'content' =>"\F1213"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-pie',
            'content' =>"\F012B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-ppf',
            'content' =>"\F1380"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-sankey',
            'content' =>"\F11DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-sankey-variant',
            'content' =>"\F11E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-scatter-plot',
            'content' =>"\F0E92"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-scatter-plot-hexbin',
            'content' =>"\F066D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-timeline',
            'content' =>"\F066E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-timeline-variant',
            'content' =>"\F0E93"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chart-tree',
            'content' =>"\F0E94"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat',
            'content' =>"\F0B79"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-alert',
            'content' =>"\F0B7A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-alert-outline',
            'content' =>"\F12C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-minus',
            'content' =>"\F1410"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-minus-outline',
            'content' =>"\F1413"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-outline',
            'content' =>"\F0EDE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-plus',
            'content' =>"\F140F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-plus-outline',
            'content' =>"\F1412"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-processing',
            'content' =>"\F0B7B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-processing-outline',
            'content' =>"\F12CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-remove',
            'content' =>"\F1411"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-remove-outline',
            'content' =>"\F1414"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-sleep',
            'content' =>"\F12D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chat-sleep-outline',
            'content' =>"\F12D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check',
            'content' =>"\F012C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-all',
            'content' =>"\F012D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-bold',
            'content' =>"\F0E1E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-box-multiple-outline',
            'content' =>"\F0C51"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-box-outline',
            'content' =>"\F0C52"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-circle',
            'content' =>"\F05E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-circle-outline',
            'content' =>"\F05E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-decagram',
            'content' =>"\F0791"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-network',
            'content' =>"\F0C53"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-network-outline',
            'content' =>"\F0C54"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-outline',
            'content' =>"\F0855"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-underline',
            'content' =>"\F0E1F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-underline-circle',
            'content' =>"\F0E20"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'check-underline-circle-outline',
            'content' =>"\F0E21"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbook',
            'content' =>"\F0A9D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-blank',
            'content' =>"\F012E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-blank-circle',
            'content' =>"\F012F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-blank-circle-outline',
            'content' =>"\F0130"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-blank-off',
            'content' =>"\F12EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-blank-off-outline',
            'content' =>"\F12ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-blank-outline',
            'content' =>"\F0131"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-intermediate',
            'content' =>"\F0856"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-marked',
            'content' =>"\F0132"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-marked-circle',
            'content' =>"\F0133"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-marked-circle-outline',
            'content' =>"\F0134"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-marked-outline',
            'content' =>"\F0135"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-multiple-blank',
            'content' =>"\F0136"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-multiple-blank-circle',
            'content' =>"\F063B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-multiple-blank-circle-outline',
            'content' =>"\F063C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-multiple-blank-outline',
            'content' =>"\F0137"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-multiple-marked',
            'content' =>"\F0138"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-multiple-marked-circle',
            'content' =>"\F063D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-multiple-marked-circle-outline',
            'content' =>"\F063E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkbox-multiple-marked-outline',
            'content' =>"\F0139"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkerboard',
            'content' =>"\F013A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkerboard-minus',
            'content' =>"\F1202"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkerboard-plus',
            'content' =>"\F1201"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'checkerboard-remove',
            'content' =>"\F1203"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cheese',
            'content' =>"\F12B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cheese-off',
            'content' =>"\F13EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chef-hat',
            'content' =>"\F0B7C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chemical-weapon',
            'content' =>"\F013B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chess-bishop',
            'content' =>"\F085C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chess-king',
            'content' =>"\F0857"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chess-knight',
            'content' =>"\F0858"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chess-pawn',
            'content' =>"\F0859"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chess-queen',
            'content' =>"\F085A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chess-rook',
            'content' =>"\F085B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-double-down',
            'content' =>"\F013C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-double-left',
            'content' =>"\F013D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-double-right',
            'content' =>"\F013E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-double-up',
            'content' =>"\F013F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-down',
            'content' =>"\F0140"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-down-box',
            'content' =>"\F09D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-down-box-outline',
            'content' =>"\F09D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-down-circle',
            'content' =>"\F0B26"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-down-circle-outline',
            'content' =>"\F0B27"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-left',
            'content' =>"\F0141"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-left-box',
            'content' =>"\F09D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-left-box-outline',
            'content' =>"\F09D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-left-circle',
            'content' =>"\F0B28"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-left-circle-outline',
            'content' =>"\F0B29"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-right',
            'content' =>"\F0142"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-right-box',
            'content' =>"\F09DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-right-box-outline',
            'content' =>"\F09DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-right-circle',
            'content' =>"\F0B2A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-right-circle-outline',
            'content' =>"\F0B2B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-triple-down',
            'content' =>"\F0DB9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-triple-left',
            'content' =>"\F0DBA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-triple-right',
            'content' =>"\F0DBB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-triple-up',
            'content' =>"\F0DBC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-up',
            'content' =>"\F0143"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-up-box',
            'content' =>"\F09DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-up-box-outline',
            'content' =>"\F09DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-up-circle',
            'content' =>"\F0B2C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chevron-up-circle-outline',
            'content' =>"\F0B2D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chili-hot',
            'content' =>"\F07B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chili-medium',
            'content' =>"\F07B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chili-mild',
            'content' =>"\F07B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chili-off',
            'content' =>"\F1467"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'chip',
            'content' =>"\F061A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'christianity',
            'content' =>"\F0953"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'christianity-outline',
            'content' =>"\F0CF6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'church',
            'content' =>"\F0144"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cigar',
            'content' =>"\F1189"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cigar-off',
            'content' =>"\F141B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle',
            'content' =>"\F0765"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-double',
            'content' =>"\F0E95"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-edit-outline',
            'content' =>"\F08D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-expand',
            'content' =>"\F0E96"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-half',
            'content' =>"\F1395"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-half-full',
            'content' =>"\F1396"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-medium',
            'content' =>"\F09DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-multiple',
            'content' =>"\F0B38"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-multiple-outline',
            'content' =>"\F0695"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-off-outline',
            'content' =>"\F10D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-outline',
            'content' =>"\F0766"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-slice-1',
            'content' =>"\F0A9E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-slice-2',
            'content' =>"\F0A9F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-slice-3',
            'content' =>"\F0AA0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-slice-4',
            'content' =>"\F0AA1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-slice-5',
            'content' =>"\F0AA2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-slice-6',
            'content' =>"\F0AA3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-slice-7',
            'content' =>"\F0AA4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-slice-8',
            'content' =>"\F0AA5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circle-small',
            'content' =>"\F09DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'circular-saw',
            'content' =>"\F0E22"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'city',
            'content' =>"\F0146"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'city-variant',
            'content' =>"\F0A36"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'city-variant-outline',
            'content' =>"\F0A37"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard',
            'content' =>"\F0147"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-account',
            'content' =>"\F0148"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-account-outline',
            'content' =>"\F0C55"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-alert',
            'content' =>"\F0149"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-alert-outline',
            'content' =>"\F0CF7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-arrow-down',
            'content' =>"\F014A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-arrow-down-outline',
            'content' =>"\F0C56"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-arrow-left',
            'content' =>"\F014B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-arrow-left-outline',
            'content' =>"\F0CF8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-arrow-right',
            'content' =>"\F0CF9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-arrow-right-outline',
            'content' =>"\F0CFA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-arrow-up',
            'content' =>"\F0C57"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-arrow-up-outline',
            'content' =>"\F0C58"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-check',
            'content' =>"\F014E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-check-multiple',
            'content' =>"\F1263"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-check-multiple-outline',
            'content' =>"\F1264"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-check-outline',
            'content' =>"\F08A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-file',
            'content' =>"\F1265"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-file-outline',
            'content' =>"\F1266"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-flow',
            'content' =>"\F06C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-flow-outline',
            'content' =>"\F1117"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-list',
            'content' =>"\F10D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-list-outline',
            'content' =>"\F10D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-multiple',
            'content' =>"\F1267"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-multiple-outline',
            'content' =>"\F1268"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-outline',
            'content' =>"\F014C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-play',
            'content' =>"\F0C59"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-play-multiple',
            'content' =>"\F1269"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-play-multiple-outline',
            'content' =>"\F126A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-play-outline',
            'content' =>"\F0C5A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-plus',
            'content' =>"\F0751"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-plus-outline',
            'content' =>"\F131F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-pulse',
            'content' =>"\F085D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-pulse-outline',
            'content' =>"\F085E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-text',
            'content' =>"\F014D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-text-multiple',
            'content' =>"\F126B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-text-multiple-outline',
            'content' =>"\F126C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-text-outline',
            'content' =>"\F0A38"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-text-play',
            'content' =>"\F0C5B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clipboard-text-play-outline',
            'content' =>"\F0C5C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clippy',
            'content' =>"\F014F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock',
            'content' =>"\F0954"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-alert',
            'content' =>"\F0955"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-alert-outline',
            'content' =>"\F05CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-check',
            'content' =>"\F0FA8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-check-outline',
            'content' =>"\F0FA9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-digital',
            'content' =>"\F0E97"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-end',
            'content' =>"\F0151"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-fast',
            'content' =>"\F0152"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-in',
            'content' =>"\F0153"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-out',
            'content' =>"\F0154"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-outline',
            'content' =>"\F0150"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-start',
            'content' =>"\F0155"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-eight',
            'content' =>"\F1446"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-eight-outline',
            'content' =>"\F1452"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-eleven',
            'content' =>"\F1449"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-eleven-outline',
            'content' =>"\F1455"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-five',
            'content' =>"\F1443"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-five-outline',
            'content' =>"\F144F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-four',
            'content' =>"\F1442"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-four-outline',
            'content' =>"\F144E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-nine',
            'content' =>"\F1447"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-nine-outline',
            'content' =>"\F1453"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-one',
            'content' =>"\F143F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-one-outline',
            'content' =>"\F144B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-seven',
            'content' =>"\F1445"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-seven-outline',
            'content' =>"\F1451"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-six',
            'content' =>"\F1444"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-six-outline',
            'content' =>"\F1450"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-ten',
            'content' =>"\F1448"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-ten-outline',
            'content' =>"\F1454"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-three',
            'content' =>"\F1441"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-three-outline',
            'content' =>"\F144D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-twelve',
            'content' =>"\F144A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-twelve-outline',
            'content' =>"\F1456"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-two',
            'content' =>"\F1440"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clock-time-two-outline',
            'content' =>"\F144C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close',
            'content' =>"\F0156"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-box',
            'content' =>"\F0157"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-box-multiple',
            'content' =>"\F0C5D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-box-multiple-outline',
            'content' =>"\F0C5E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-box-outline',
            'content' =>"\F0158"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-circle',
            'content' =>"\F0159"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-circle-multiple',
            'content' =>"\F062A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-circle-multiple-outline',
            'content' =>"\F0883"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-circle-outline',
            'content' =>"\F015A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-network',
            'content' =>"\F015B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-network-outline',
            'content' =>"\F0C5F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-octagon',
            'content' =>"\F015C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-octagon-outline',
            'content' =>"\F015D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-outline',
            'content' =>"\F06C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'close-thick',
            'content' =>"\F1398"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'closed-caption',
            'content' =>"\F015E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'closed-caption-outline',
            'content' =>"\F0DBD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud',
            'content' =>"\F015F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-alert',
            'content' =>"\F09E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-braces',
            'content' =>"\F07B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-check',
            'content' =>"\F0160"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-check-outline',
            'content' =>"\F12CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-circle',
            'content' =>"\F0161"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-download',
            'content' =>"\F0162"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-download-outline',
            'content' =>"\F0B7D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-lock',
            'content' =>"\F11F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-lock-outline',
            'content' =>"\F11F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-off-outline',
            'content' =>"\F0164"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-outline',
            'content' =>"\F0163"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-print',
            'content' =>"\F0165"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-print-outline',
            'content' =>"\F0166"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-question',
            'content' =>"\F0A39"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-refresh',
            'content' =>"\F052A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-search',
            'content' =>"\F0956"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-search-outline',
            'content' =>"\F0957"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-sync',
            'content' =>"\F063F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-sync-outline',
            'content' =>"\F12D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-tags',
            'content' =>"\F07B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-upload',
            'content' =>"\F0167"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cloud-upload-outline',
            'content' =>"\F0B7E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'clover',
            'content' =>"\F0816"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coach-lamp',
            'content' =>"\F1020"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coat-rack',
            'content' =>"\F109E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-array',
            'content' =>"\F0168"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-braces',
            'content' =>"\F0169"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-braces-box',
            'content' =>"\F10D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-brackets',
            'content' =>"\F016A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-equal',
            'content' =>"\F016B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-greater-than',
            'content' =>"\F016C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-greater-than-or-equal',
            'content' =>"\F016D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-json',
            'content' =>"\F0626"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-less-than',
            'content' =>"\F016E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-less-than-or-equal',
            'content' =>"\F016F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-not-equal',
            'content' =>"\F0170"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-not-equal-variant',
            'content' =>"\F0171"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-parentheses',
            'content' =>"\F0172"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-parentheses-box',
            'content' =>"\F10D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-string',
            'content' =>"\F0173"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-tags',
            'content' =>"\F0174"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'code-tags-check',
            'content' =>"\F0694"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'codepen',
            'content' =>"\F0175"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coffee',
            'content' =>"\F0176"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coffee-maker',
            'content' =>"\F109F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coffee-off',
            'content' =>"\F0FAA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coffee-off-outline',
            'content' =>"\F0FAB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coffee-outline',
            'content' =>"\F06CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coffee-to-go',
            'content' =>"\F0177"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coffee-to-go-outline',
            'content' =>"\F130E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coffin',
            'content' =>"\F0B7F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog',
            'content' =>"\F0493"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-box',
            'content' =>"\F0494"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-clockwise',
            'content' =>"\F11DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-counterclockwise',
            'content' =>"\F11DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-off',
            'content' =>"\F13CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-off-outline',
            'content' =>"\F13CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-outline',
            'content' =>"\F08BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-refresh',
            'content' =>"\F145E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-refresh-outline',
            'content' =>"\F145F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-sync',
            'content' =>"\F1460"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-sync-outline',
            'content' =>"\F1461"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-transfer',
            'content' =>"\F105B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cog-transfer-outline',
            'content' =>"\F105C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cogs',
            'content' =>"\F08D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'collage',
            'content' =>"\F0640"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'collapse-all',
            'content' =>"\F0AA6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'collapse-all-outline',
            'content' =>"\F0AA7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'color-helper',
            'content' =>"\F0179"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comma',
            'content' =>"\F0E23"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comma-box',
            'content' =>"\F0E2B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comma-box-outline',
            'content' =>"\F0E24"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comma-circle',
            'content' =>"\F0E25"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comma-circle-outline',
            'content' =>"\F0E26"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment',
            'content' =>"\F017A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-account',
            'content' =>"\F017B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-account-outline',
            'content' =>"\F017C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-alert',
            'content' =>"\F017D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-alert-outline',
            'content' =>"\F017E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-arrow-left',
            'content' =>"\F09E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-arrow-left-outline',
            'content' =>"\F09E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-arrow-right',
            'content' =>"\F09E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-arrow-right-outline',
            'content' =>"\F09E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-check',
            'content' =>"\F017F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-check-outline',
            'content' =>"\F0180"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-edit',
            'content' =>"\F11BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-edit-outline',
            'content' =>"\F12C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-eye',
            'content' =>"\F0A3A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-eye-outline',
            'content' =>"\F0A3B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-multiple',
            'content' =>"\F085F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-multiple-outline',
            'content' =>"\F0181"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-outline',
            'content' =>"\F0182"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-plus',
            'content' =>"\F09E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-plus-outline',
            'content' =>"\F0183"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-processing',
            'content' =>"\F0184"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-processing-outline',
            'content' =>"\F0185"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-question',
            'content' =>"\F0817"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-question-outline',
            'content' =>"\F0186"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-quote',
            'content' =>"\F1021"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-quote-outline',
            'content' =>"\F1022"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-remove',
            'content' =>"\F05DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-remove-outline',
            'content' =>"\F0187"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-search',
            'content' =>"\F0A3C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-search-outline',
            'content' =>"\F0A3D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-text',
            'content' =>"\F0188"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-text-multiple',
            'content' =>"\F0860"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-text-multiple-outline',
            'content' =>"\F0861"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'comment-text-outline',
            'content' =>"\F0189"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'compare',
            'content' =>"\F018A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'compare-horizontal',
            'content' =>"\F1492"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'compare-vertical',
            'content' =>"\F1493"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'compass',
            'content' =>"\F018B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'compass-off',
            'content' =>"\F0B80"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'compass-off-outline',
            'content' =>"\F0B81"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'compass-outline',
            'content' =>"\F018C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'compass-rose',
            'content' =>"\F1382"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'concourse-ci',
            'content' =>"\F10A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'console',
            'content' =>"\F018D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'console-line',
            'content' =>"\F07B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'console-network',
            'content' =>"\F08A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'console-network-outline',
            'content' =>"\F0C60"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'consolidate',
            'content' =>"\F10D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contactless-payment',
            'content' =>"\F0D6A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contactless-payment-circle',
            'content' =>"\F0321"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contactless-payment-circle-outline',
            'content' =>"\F0408"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contacts',
            'content' =>"\F06CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contacts-outline',
            'content' =>"\F05B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contain',
            'content' =>"\F0A3E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contain-end',
            'content' =>"\F0A3F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contain-start',
            'content' =>"\F0A40"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-copy',
            'content' =>"\F018F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-cut',
            'content' =>"\F0190"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-duplicate',
            'content' =>"\F0191"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-paste',
            'content' =>"\F0192"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save',
            'content' =>"\F0193"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-alert',
            'content' =>"\F0F42"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-alert-outline',
            'content' =>"\F0F43"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-all',
            'content' =>"\F0194"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-all-outline',
            'content' =>"\F0F44"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-cog',
            'content' =>"\F145B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-cog-outline',
            'content' =>"\F145C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-edit',
            'content' =>"\F0CFB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-edit-outline',
            'content' =>"\F0CFC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-move',
            'content' =>"\F0E27"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-move-outline',
            'content' =>"\F0E28"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-outline',
            'content' =>"\F0818"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-settings',
            'content' =>"\F061B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'content-save-settings-outline',
            'content' =>"\F0B2E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contrast',
            'content' =>"\F0195"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contrast-box',
            'content' =>"\F0196"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'contrast-circle',
            'content' =>"\F0197"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'controller-classic',
            'content' =>"\F0B82"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'controller-classic-outline',
            'content' =>"\F0B83"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cookie',
            'content' =>"\F0198"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'coolant-temperature',
            'content' =>"\F03C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'copyright',
            'content' =>"\F05E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cordova',
            'content' =>"\F0958"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'corn',
            'content' =>"\F07B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'corn-off',
            'content' =>"\F13EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cosine-wave',
            'content' =>"\F1479"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'counter',
            'content' =>"\F0199"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cow',
            'content' =>"\F019A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cpu-32-bit',
            'content' =>"\F0EDF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cpu-64-bit',
            'content' =>"\F0EE0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crane',
            'content' =>"\F0862"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'creation',
            'content' =>"\F0674"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'creative-commons',
            'content' =>"\F0D6B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card',
            'content' =>"\F0FEF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-check',
            'content' =>"\F13D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-check-outline',
            'content' =>"\F13D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-clock',
            'content' =>"\F0EE1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-clock-outline',
            'content' =>"\F0EE2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-marker',
            'content' =>"\F06A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-marker-outline',
            'content' =>"\F0DBE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-minus',
            'content' =>"\F0FAC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-minus-outline',
            'content' =>"\F0FAD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-multiple',
            'content' =>"\F0FF0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-multiple-outline',
            'content' =>"\F019C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-off',
            'content' =>"\F0FF1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-off-outline',
            'content' =>"\F05E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-outline',
            'content' =>"\F019B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-plus',
            'content' =>"\F0FF2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-plus-outline',
            'content' =>"\F0676"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-refund',
            'content' =>"\F0FF3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-refund-outline',
            'content' =>"\F0AA8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-remove',
            'content' =>"\F0FAE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-remove-outline',
            'content' =>"\F0FAF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-scan',
            'content' =>"\F0FF4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-scan-outline',
            'content' =>"\F019D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-settings',
            'content' =>"\F0FF5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-settings-outline',
            'content' =>"\F08D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-wireless',
            'content' =>"\F0802"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-wireless-off',
            'content' =>"\F057A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-wireless-off-outline',
            'content' =>"\F057B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'credit-card-wireless-outline',
            'content' =>"\F0D6C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cricket',
            'content' =>"\F0D6D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crop',
            'content' =>"\F019E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crop-free',
            'content' =>"\F019F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crop-landscape',
            'content' =>"\F01A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crop-portrait',
            'content' =>"\F01A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crop-rotate',
            'content' =>"\F0696"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crop-square',
            'content' =>"\F01A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crosshairs',
            'content' =>"\F01A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crosshairs-gps',
            'content' =>"\F01A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crosshairs-off',
            'content' =>"\F0F45"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crosshairs-question',
            'content' =>"\F1136"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crown',
            'content' =>"\F01A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crown-outline',
            'content' =>"\F11D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cryengine',
            'content' =>"\F0959"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'crystal-ball',
            'content' =>"\F0B2F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cube',
            'content' =>"\F01A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cube-off',
            'content' =>"\F141C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cube-off-outline',
            'content' =>"\F141D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cube-outline',
            'content' =>"\F01A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cube-scan',
            'content' =>"\F0B84"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cube-send',
            'content' =>"\F01A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cube-unfolded',
            'content' =>"\F01A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cup',
            'content' =>"\F01AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cup-off',
            'content' =>"\F05E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cup-off-outline',
            'content' =>"\F137D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cup-outline',
            'content' =>"\F130F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cup-water',
            'content' =>"\F01AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cupboard',
            'content' =>"\F0F46"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cupboard-outline',
            'content' =>"\F0F47"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cupcake',
            'content' =>"\F095A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'curling',
            'content' =>"\F0863"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-bdt',
            'content' =>"\F0864"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-brl',
            'content' =>"\F0B85"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-btc',
            'content' =>"\F01AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-cny',
            'content' =>"\F07BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-eth',
            'content' =>"\F07BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-eur',
            'content' =>"\F01AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-eur-off',
            'content' =>"\F1315"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-gbp',
            'content' =>"\F01AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-ils',
            'content' =>"\F0C61"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-inr',
            'content' =>"\F01AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-jpy',
            'content' =>"\F07BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-krw',
            'content' =>"\F07BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-kzt',
            'content' =>"\F0865"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-ngn',
            'content' =>"\F01B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-php',
            'content' =>"\F09E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-rial',
            'content' =>"\F0E9C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-rub',
            'content' =>"\F01B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-sign',
            'content' =>"\F07BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-try',
            'content' =>"\F01B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-twd',
            'content' =>"\F07BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-usd',
            'content' =>"\F01C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-usd-circle',
            'content' =>"\F116B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-usd-circle-outline',
            'content' =>"\F0178"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'currency-usd-off',
            'content' =>"\F067A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'current-ac',
            'content' =>"\F1480"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'current-dc',
            'content' =>"\F095C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cursor-default',
            'content' =>"\F01C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cursor-default-click',
            'content' =>"\F0CFD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cursor-default-click-outline',
            'content' =>"\F0CFE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cursor-default-gesture',
            'content' =>"\F1127"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cursor-default-gesture-outline',
            'content' =>"\F1128"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cursor-default-outline',
            'content' =>"\F01BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cursor-move',
            'content' =>"\F01BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cursor-pointer',
            'content' =>"\F01BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'cursor-text',
            'content' =>"\F05E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database',
            'content' =>"\F01BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-check',
            'content' =>"\F0AA9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-edit',
            'content' =>"\F0B86"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-export',
            'content' =>"\F095E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-import',
            'content' =>"\F095D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-lock',
            'content' =>"\F0AAA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-marker',
            'content' =>"\F12F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-minus',
            'content' =>"\F01BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-plus',
            'content' =>"\F01BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-refresh',
            'content' =>"\F05C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-remove',
            'content' =>"\F0D00"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-search',
            'content' =>"\F0866"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-settings',
            'content' =>"\F0D01"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'database-sync',
            'content' =>"\F0CFF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'death-star',
            'content' =>"\F08D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'death-star-variant',
            'content' =>"\F08D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'deathly-hallows',
            'content' =>"\F0B87"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'debian',
            'content' =>"\F08DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'debug-step-into',
            'content' =>"\F01B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'debug-step-out',
            'content' =>"\F01B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'debug-step-over',
            'content' =>"\F01B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'decagram',
            'content' =>"\F076C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'decagram-outline',
            'content' =>"\F076D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'decimal',
            'content' =>"\F10A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'decimal-comma',
            'content' =>"\F10A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'decimal-comma-decrease',
            'content' =>"\F10A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'decimal-comma-increase',
            'content' =>"\F10A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'decimal-decrease',
            'content' =>"\F01B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'decimal-increase',
            'content' =>"\F01B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete',
            'content' =>"\F01B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-alert',
            'content' =>"\F10A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-alert-outline',
            'content' =>"\F10A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-circle',
            'content' =>"\F0683"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-circle-outline',
            'content' =>"\F0B88"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-empty',
            'content' =>"\F06CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-empty-outline',
            'content' =>"\F0E9D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-forever',
            'content' =>"\F05E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-forever-outline',
            'content' =>"\F0B89"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-off',
            'content' =>"\F10A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-off-outline',
            'content' =>"\F10A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-outline',
            'content' =>"\F09E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-restore',
            'content' =>"\F0819"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-sweep',
            'content' =>"\F05E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-sweep-outline',
            'content' =>"\F0C62"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delete-variant',
            'content' =>"\F01B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'delta',
            'content' =>"\F01C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'desk',
            'content' =>"\F1239"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'desk-lamp',
            'content' =>"\F095F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'deskphone',
            'content' =>"\F01C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'desktop-classic',
            'content' =>"\F07C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'desktop-mac',
            'content' =>"\F01C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'desktop-mac-dashboard',
            'content' =>"\F09E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'desktop-tower',
            'content' =>"\F01C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'desktop-tower-monitor',
            'content' =>"\F0AAB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'details',
            'content' =>"\F01C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dev-to',
            'content' =>"\F0D6E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'developer-board',
            'content' =>"\F0697"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'deviantart',
            'content' =>"\F01C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'devices',
            'content' =>"\F0FB0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diabetes',
            'content' =>"\F1126"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dialpad',
            'content' =>"\F061C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diameter',
            'content' =>"\F0C63"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diameter-outline',
            'content' =>"\F0C64"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diameter-variant',
            'content' =>"\F0C65"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diamond',
            'content' =>"\F0B8A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diamond-outline',
            'content' =>"\F0B8B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diamond-stone',
            'content' =>"\F01C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-1',
            'content' =>"\F01CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-1-outline',
            'content' =>"\F114A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-2',
            'content' =>"\F01CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-2-outline',
            'content' =>"\F114B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-3',
            'content' =>"\F01CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-3-outline',
            'content' =>"\F114C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-4',
            'content' =>"\F01CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-4-outline',
            'content' =>"\F114D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-5',
            'content' =>"\F01CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-5-outline',
            'content' =>"\F114E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-6',
            'content' =>"\F01CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-6-outline',
            'content' =>"\F114F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d10',
            'content' =>"\F1153"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d10-outline',
            'content' =>"\F076F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d12',
            'content' =>"\F1154"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d12-outline',
            'content' =>"\F0867"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d20',
            'content' =>"\F1155"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d20-outline',
            'content' =>"\F05EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d4',
            'content' =>"\F1150"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d4-outline',
            'content' =>"\F05EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d6',
            'content' =>"\F1151"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d6-outline',
            'content' =>"\F05ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d8',
            'content' =>"\F1152"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-d8-outline',
            'content' =>"\F05EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-multiple',
            'content' =>"\F076E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dice-multiple-outline',
            'content' =>"\F1156"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'digital-ocean',
            'content' =>"\F1237"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dip-switch',
            'content' =>"\F07C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'directions',
            'content' =>"\F01D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'directions-fork',
            'content' =>"\F0641"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'disc',
            'content' =>"\F05EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'disc-alert',
            'content' =>"\F01D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'disc-player',
            'content' =>"\F0960"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'discord',
            'content' =>"\F066F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dishwasher',
            'content' =>"\F0AAC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dishwasher-alert',
            'content' =>"\F11B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dishwasher-off',
            'content' =>"\F11B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'disqus',
            'content' =>"\F01D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'distribute-horizontal-center',
            'content' =>"\F11C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'distribute-horizontal-left',
            'content' =>"\F11C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'distribute-horizontal-right',
            'content' =>"\F11CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'distribute-vertical-bottom',
            'content' =>"\F11CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'distribute-vertical-center',
            'content' =>"\F11CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'distribute-vertical-top',
            'content' =>"\F11CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diving-flippers',
            'content' =>"\F0DBF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diving-helmet',
            'content' =>"\F0DC0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diving-scuba',
            'content' =>"\F0DC1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diving-scuba-flag',
            'content' =>"\F0DC2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diving-scuba-tank',
            'content' =>"\F0DC3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diving-scuba-tank-multiple',
            'content' =>"\F0DC4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'diving-snorkel',
            'content' =>"\F0DC5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'division',
            'content' =>"\F01D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'division-box',
            'content' =>"\F01D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dlna',
            'content' =>"\F0A41"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dna',
            'content' =>"\F0684"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dns',
            'content' =>"\F01D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dns-outline',
            'content' =>"\F0B8C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'do-not-disturb',
            'content' =>"\F0698"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'do-not-disturb-off',
            'content' =>"\F0699"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dock-bottom',
            'content' =>"\F10A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dock-left',
            'content' =>"\F10AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dock-right',
            'content' =>"\F10AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dock-window',
            'content' =>"\F10AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'docker',
            'content' =>"\F0868"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'doctor',
            'content' =>"\F0A42"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dog',
            'content' =>"\F0A43"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dog-service',
            'content' =>"\F0AAD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dog-side',
            'content' =>"\F0A44"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dolby',
            'content' =>"\F06B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dolly',
            'content' =>"\F0E9E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'domain',
            'content' =>"\F01D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'domain-off',
            'content' =>"\F0D6F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'domain-plus',
            'content' =>"\F10AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'domain-remove',
            'content' =>"\F10AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dome-light',
            'content' =>"\F141E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'domino-mask',
            'content' =>"\F1023"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'donkey',
            'content' =>"\F07C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'door',
            'content' =>"\F081A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'door-closed',
            'content' =>"\F081B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'door-closed-lock',
            'content' =>"\F10AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'door-open',
            'content' =>"\F081C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'doorbell',
            'content' =>"\F12E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'doorbell-video',
            'content' =>"\F0869"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dot-net',
            'content' =>"\F0AAE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dots-horizontal',
            'content' =>"\F01D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dots-horizontal-circle',
            'content' =>"\F07C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dots-horizontal-circle-outline',
            'content' =>"\F0B8D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dots-vertical',
            'content' =>"\F01D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dots-vertical-circle',
            'content' =>"\F07C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dots-vertical-circle-outline',
            'content' =>"\F0B8E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'douban',
            'content' =>"\F069A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download',
            'content' =>"\F01DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-box',
            'content' =>"\F1462"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-box-outline',
            'content' =>"\F1463"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-circle',
            'content' =>"\F1464"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-circle-outline',
            'content' =>"\F1465"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-lock',
            'content' =>"\F1320"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-lock-outline',
            'content' =>"\F1321"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-multiple',
            'content' =>"\F09E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-network',
            'content' =>"\F06F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-network-outline',
            'content' =>"\F0C66"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-off',
            'content' =>"\F10B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-off-outline',
            'content' =>"\F10B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'download-outline',
            'content' =>"\F0B8F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drag',
            'content' =>"\F01DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drag-horizontal',
            'content' =>"\F01DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drag-horizontal-variant',
            'content' =>"\F12F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drag-variant',
            'content' =>"\F0B90"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drag-vertical',
            'content' =>"\F01DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drag-vertical-variant',
            'content' =>"\F12F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drama-masks',
            'content' =>"\F0D02"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'draw',
            'content' =>"\F0F49"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drawing',
            'content' =>"\F01DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drawing-box',
            'content' =>"\F01DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dresser',
            'content' =>"\F0F4A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dresser-outline',
            'content' =>"\F0F4B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drone',
            'content' =>"\F01E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dropbox',
            'content' =>"\F01E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'drupal',
            'content' =>"\F01E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'duck',
            'content' =>"\F01E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dumbbell',
            'content' =>"\F01E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'dump-truck',
            'content' =>"\F0C67"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ear-hearing',
            'content' =>"\F07C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ear-hearing-off',
            'content' =>"\F0A45"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth',
            'content' =>"\F01E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-arrow-right',
            'content' =>"\F1311"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-box',
            'content' =>"\F06CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-box-minus',
            'content' =>"\F1407"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-box-off',
            'content' =>"\F06CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-box-plus',
            'content' =>"\F1406"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-box-remove',
            'content' =>"\F1408"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-minus',
            'content' =>"\F1404"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-off',
            'content' =>"\F01E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-plus',
            'content' =>"\F1403"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'earth-remove',
            'content' =>"\F1405"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'egg',
            'content' =>"\F0AAF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'egg-easter',
            'content' =>"\F0AB0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'egg-off',
            'content' =>"\F13F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'egg-off-outline',
            'content' =>"\F13F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'egg-outline',
            'content' =>"\F13F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eight-track',
            'content' =>"\F09EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eject',
            'content' =>"\F01EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eject-outline',
            'content' =>"\F0B91"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'electric-switch',
            'content' =>"\F0E9F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'electric-switch-closed',
            'content' =>"\F10D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'electron-framework',
            'content' =>"\F1024"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'elephant',
            'content' =>"\F07C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'elevation-decline',
            'content' =>"\F01EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'elevation-rise',
            'content' =>"\F01EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'elevator',
            'content' =>"\F01ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'elevator-down',
            'content' =>"\F12C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'elevator-passenger',
            'content' =>"\F1381"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'elevator-up',
            'content' =>"\F12C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ellipse',
            'content' =>"\F0EA0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ellipse-outline',
            'content' =>"\F0EA1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email',
            'content' =>"\F01EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-alert',
            'content' =>"\F06CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-alert-outline',
            'content' =>"\F0D42"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-box',
            'content' =>"\F0D03"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-check',
            'content' =>"\F0AB1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-check-outline',
            'content' =>"\F0AB2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-edit',
            'content' =>"\F0EE3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-edit-outline',
            'content' =>"\F0EE4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-lock',
            'content' =>"\F01F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-mark-as-unread',
            'content' =>"\F0B92"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-minus',
            'content' =>"\F0EE5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-minus-outline',
            'content' =>"\F0EE6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-multiple',
            'content' =>"\F0EE7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-multiple-outline',
            'content' =>"\F0EE8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-newsletter',
            'content' =>"\F0FB1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-off',
            'content' =>"\F13E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-off-outline',
            'content' =>"\F13E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-open',
            'content' =>"\F01EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-open-multiple',
            'content' =>"\F0EE9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-open-multiple-outline',
            'content' =>"\F0EEA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-open-outline',
            'content' =>"\F05EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-outline',
            'content' =>"\F01F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-plus',
            'content' =>"\F09EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-plus-outline',
            'content' =>"\F09EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-receive',
            'content' =>"\F10DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-receive-outline',
            'content' =>"\F10DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-search',
            'content' =>"\F0961"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-search-outline',
            'content' =>"\F0962"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-send',
            'content' =>"\F10DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-send-outline',
            'content' =>"\F10DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-sync',
            'content' =>"\F12C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-sync-outline',
            'content' =>"\F12C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'email-variant',
            'content' =>"\F05F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ember',
            'content' =>"\F0B30"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emby',
            'content' =>"\F06B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon',
            'content' =>"\F0C68"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-angry',
            'content' =>"\F0C69"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-angry-outline',
            'content' =>"\F0C6A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-confused',
            'content' =>"\F10DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-confused-outline',
            'content' =>"\F10DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-cool',
            'content' =>"\F0C6B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-cool-outline',
            'content' =>"\F01F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-cry',
            'content' =>"\F0C6C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-cry-outline',
            'content' =>"\F0C6D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-dead',
            'content' =>"\F0C6E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-dead-outline',
            'content' =>"\F069B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-devil',
            'content' =>"\F0C6F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-devil-outline',
            'content' =>"\F01F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-excited',
            'content' =>"\F0C70"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-excited-outline',
            'content' =>"\F069C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-frown',
            'content' =>"\F0F4C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-frown-outline',
            'content' =>"\F0F4D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-happy',
            'content' =>"\F0C71"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-happy-outline',
            'content' =>"\F01F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-kiss',
            'content' =>"\F0C72"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-kiss-outline',
            'content' =>"\F0C73"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-lol',
            'content' =>"\F1214"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-lol-outline',
            'content' =>"\F1215"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-neutral',
            'content' =>"\F0C74"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-neutral-outline',
            'content' =>"\F01F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-outline',
            'content' =>"\F01F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-poop',
            'content' =>"\F01F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-poop-outline',
            'content' =>"\F0C75"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-sad',
            'content' =>"\F0C76"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-sad-outline',
            'content' =>"\F01F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-tongue',
            'content' =>"\F01F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-tongue-outline',
            'content' =>"\F0C77"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-wink',
            'content' =>"\F0C78"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'emoticon-wink-outline',
            'content' =>"\F0C79"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'engine',
            'content' =>"\F01FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'engine-off',
            'content' =>"\F0A46"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'engine-off-outline',
            'content' =>"\F0A47"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'engine-outline',
            'content' =>"\F01FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'epsilon',
            'content' =>"\F10E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'equal',
            'content' =>"\F01FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'equal-box',
            'content' =>"\F01FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'equalizer',
            'content' =>"\F0EA2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'equalizer-outline',
            'content' =>"\F0EA3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eraser',
            'content' =>"\F01FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eraser-variant',
            'content' =>"\F0642"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'escalator',
            'content' =>"\F01FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'escalator-box',
            'content' =>"\F1399"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'escalator-down',
            'content' =>"\F12C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'escalator-up',
            'content' =>"\F12BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eslint',
            'content' =>"\F0C7A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'et',
            'content' =>"\F0AB3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ethereum',
            'content' =>"\F086A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ethernet',
            'content' =>"\F0200"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ethernet-cable',
            'content' =>"\F0201"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ethernet-cable-off',
            'content' =>"\F0202"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ev-station',
            'content' =>"\F05F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'evernote',
            'content' =>"\F0204"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'excavator',
            'content' =>"\F1025"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'exclamation',
            'content' =>"\F0205"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'exclamation-thick',
            'content' =>"\F1238"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'exit-run',
            'content' =>"\F0A48"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'exit-to-app',
            'content' =>"\F0206"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'expand-all',
            'content' =>"\F0AB4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'expand-all-outline',
            'content' =>"\F0AB5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'expansion-card',
            'content' =>"\F08AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'expansion-card-variant',
            'content' =>"\F0FB2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'exponent',
            'content' =>"\F0963"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'exponent-box',
            'content' =>"\F0964"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'export',
            'content' =>"\F0207"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'export-variant',
            'content' =>"\F0B93"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye',
            'content' =>"\F0208"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-check',
            'content' =>"\F0D04"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-check-outline',
            'content' =>"\F0D05"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-circle',
            'content' =>"\F0B94"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-circle-outline',
            'content' =>"\F0B95"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-minus',
            'content' =>"\F1026"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-minus-outline',
            'content' =>"\F1027"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-off',
            'content' =>"\F0209"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-off-outline',
            'content' =>"\F06D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-outline',
            'content' =>"\F06D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-plus',
            'content' =>"\F086B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-plus-outline',
            'content' =>"\F086C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-settings',
            'content' =>"\F086D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eye-settings-outline',
            'content' =>"\F086E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eyedropper',
            'content' =>"\F020A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eyedropper-minus',
            'content' =>"\F13DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eyedropper-off',
            'content' =>"\F13DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eyedropper-plus',
            'content' =>"\F13DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eyedropper-remove',
            'content' =>"\F13DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'eyedropper-variant',
            'content' =>"\F020B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'face',
            'content' =>"\F0643"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'face-agent',
            'content' =>"\F0D70"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'face-outline',
            'content' =>"\F0B96"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'face-profile',
            'content' =>"\F0644"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'face-profile-woman',
            'content' =>"\F1076"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'face-recognition',
            'content' =>"\F0C7B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'face-woman',
            'content' =>"\F1077"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'face-woman-outline',
            'content' =>"\F1078"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'facebook',
            'content' =>"\F020C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'facebook-messenger',
            'content' =>"\F020E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'facebook-workplace',
            'content' =>"\F0B31"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'factory',
            'content' =>"\F020F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan',
            'content' =>"\F0210"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-alert',
            'content' =>"\F146C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-chevron-down',
            'content' =>"\F146D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-chevron-up',
            'content' =>"\F146E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-minus',
            'content' =>"\F1470"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-off',
            'content' =>"\F081D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-plus',
            'content' =>"\F146F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-remove',
            'content' =>"\F1471"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-speed-1',
            'content' =>"\F1472"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-speed-2',
            'content' =>"\F1473"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fan-speed-3',
            'content' =>"\F1474"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fast-forward',
            'content' =>"\F0211"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fast-forward-10',
            'content' =>"\F0D71"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fast-forward-30',
            'content' =>"\F0D06"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fast-forward-5',
            'content' =>"\F11F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fast-forward-outline',
            'content' =>"\F06D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fax',
            'content' =>"\F0212"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'feather',
            'content' =>"\F06D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'feature-search',
            'content' =>"\F0A49"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'feature-search-outline',
            'content' =>"\F0A4A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fedora',
            'content' =>"\F08DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fencing',
            'content' =>"\F14C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ferris-wheel',
            'content' =>"\F0EA4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ferry',
            'content' =>"\F0213"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file',
            'content' =>"\F0214"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-account',
            'content' =>"\F073B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-account-outline',
            'content' =>"\F1028"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-alert',
            'content' =>"\F0A4B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-alert-outline',
            'content' =>"\F0A4C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-cabinet',
            'content' =>"\F0AB6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-cad',
            'content' =>"\F0EEB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-cad-box',
            'content' =>"\F0EEC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-cancel',
            'content' =>"\F0DC6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-cancel-outline',
            'content' =>"\F0DC7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-certificate',
            'content' =>"\F1186"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-certificate-outline',
            'content' =>"\F1187"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-chart',
            'content' =>"\F0215"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-chart-outline',
            'content' =>"\F1029"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-check',
            'content' =>"\F0216"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-check-outline',
            'content' =>"\F0E29"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-clock',
            'content' =>"\F12E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-clock-outline',
            'content' =>"\F12E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-cloud',
            'content' =>"\F0217"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-cloud-outline',
            'content' =>"\F102A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-code',
            'content' =>"\F022E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-code-outline',
            'content' =>"\F102B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-cog',
            'content' =>"\F107B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-cog-outline',
            'content' =>"\F107C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-compare',
            'content' =>"\F08AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-delimited',
            'content' =>"\F0218"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-delimited-outline',
            'content' =>"\F0EA5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-document',
            'content' =>"\F0219"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-document-edit',
            'content' =>"\F0DC8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-document-edit-outline',
            'content' =>"\F0DC9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-document-outline',
            'content' =>"\F09EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-download',
            'content' =>"\F0965"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-download-outline',
            'content' =>"\F0966"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-edit',
            'content' =>"\F11E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-edit-outline',
            'content' =>"\F11E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-excel',
            'content' =>"\F021B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-excel-box',
            'content' =>"\F021C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-excel-box-outline',
            'content' =>"\F102C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-excel-outline',
            'content' =>"\F102D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-export',
            'content' =>"\F021D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-export-outline',
            'content' =>"\F102E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-eye',
            'content' =>"\F0DCA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-eye-outline',
            'content' =>"\F0DCB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-find',
            'content' =>"\F021E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-find-outline',
            'content' =>"\F0B97"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-hidden',
            'content' =>"\F0613"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-image',
            'content' =>"\F021F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-image-outline',
            'content' =>"\F0EB0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-import',
            'content' =>"\F0220"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-import-outline',
            'content' =>"\F102F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-key',
            'content' =>"\F1184"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-key-outline',
            'content' =>"\F1185"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-link',
            'content' =>"\F1177"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-link-outline',
            'content' =>"\F1178"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-lock',
            'content' =>"\F0221"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-lock-outline',
            'content' =>"\F1030"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-move',
            'content' =>"\F0AB9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-move-outline',
            'content' =>"\F1031"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-multiple',
            'content' =>"\F0222"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-multiple-outline',
            'content' =>"\F1032"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-music',
            'content' =>"\F0223"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-music-outline',
            'content' =>"\F0E2A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-outline',
            'content' =>"\F0224"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-pdf',
            'content' =>"\F0225"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-pdf-box',
            'content' =>"\F0226"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-pdf-box-outline',
            'content' =>"\F0FB3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-pdf-outline',
            'content' =>"\F0E2D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-percent',
            'content' =>"\F081E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-percent-outline',
            'content' =>"\F1033"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-phone',
            'content' =>"\F1179"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-phone-outline',
            'content' =>"\F117A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-plus',
            'content' =>"\F0752"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-plus-outline',
            'content' =>"\F0EED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-powerpoint',
            'content' =>"\F0227"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-powerpoint-box',
            'content' =>"\F0228"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-powerpoint-box-outline',
            'content' =>"\F1034"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-powerpoint-outline',
            'content' =>"\F1035"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-presentation-box',
            'content' =>"\F0229"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-question',
            'content' =>"\F086F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-question-outline',
            'content' =>"\F1036"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-refresh',
            'content' =>"\F0918"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-refresh-outline',
            'content' =>"\F0541"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-remove',
            'content' =>"\F0B98"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-remove-outline',
            'content' =>"\F1037"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-replace',
            'content' =>"\F0B32"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-replace-outline',
            'content' =>"\F0B33"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-restore',
            'content' =>"\F0670"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-restore-outline',
            'content' =>"\F1038"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-search',
            'content' =>"\F0C7C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-search-outline',
            'content' =>"\F0C7D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-send',
            'content' =>"\F022A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-send-outline',
            'content' =>"\F1039"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-settings',
            'content' =>"\F1079"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-settings-outline',
            'content' =>"\F107A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-star',
            'content' =>"\F103A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-star-outline',
            'content' =>"\F103B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-swap',
            'content' =>"\F0FB4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-swap-outline',
            'content' =>"\F0FB5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-sync',
            'content' =>"\F1216"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-sync-outline',
            'content' =>"\F1217"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-table',
            'content' =>"\F0C7E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-table-box',
            'content' =>"\F10E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-table-box-multiple',
            'content' =>"\F10E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-table-box-multiple-outline',
            'content' =>"\F10E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-table-box-outline',
            'content' =>"\F10E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-table-outline',
            'content' =>"\F0C7F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-tree',
            'content' =>"\F0645"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-tree-outline',
            'content' =>"\F13D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-undo',
            'content' =>"\F08DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-undo-outline',
            'content' =>"\F103C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-upload',
            'content' =>"\F0A4D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-upload-outline',
            'content' =>"\F0A4E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-video',
            'content' =>"\F022B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-video-outline',
            'content' =>"\F0E2C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-word',
            'content' =>"\F022C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-word-box',
            'content' =>"\F022D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-word-box-outline',
            'content' =>"\F103D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'file-word-outline',
            'content' =>"\F103E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'film',
            'content' =>"\F022F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filmstrip',
            'content' =>"\F0230"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filmstrip-box',
            'content' =>"\F0332"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filmstrip-box-multiple',
            'content' =>"\F0D18"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filmstrip-off',
            'content' =>"\F0231"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter',
            'content' =>"\F0232"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-menu',
            'content' =>"\F10E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-menu-outline',
            'content' =>"\F10E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-minus',
            'content' =>"\F0EEE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-minus-outline',
            'content' =>"\F0EEF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-outline',
            'content' =>"\F0233"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-plus',
            'content' =>"\F0EF0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-plus-outline',
            'content' =>"\F0EF1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-remove',
            'content' =>"\F0234"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-remove-outline',
            'content' =>"\F0235"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-variant',
            'content' =>"\F0236"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-variant-minus',
            'content' =>"\F1112"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-variant-plus',
            'content' =>"\F1113"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'filter-variant-remove',
            'content' =>"\F103F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'finance',
            'content' =>"\F081F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'find-replace',
            'content' =>"\F06D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fingerprint',
            'content' =>"\F0237"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fingerprint-off',
            'content' =>"\F0EB1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fire',
            'content' =>"\F0238"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fire-extinguisher',
            'content' =>"\F0EF2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fire-hydrant',
            'content' =>"\F1137"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fire-hydrant-alert',
            'content' =>"\F1138"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fire-hydrant-off',
            'content' =>"\F1139"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fire-truck',
            'content' =>"\F08AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'firebase',
            'content' =>"\F0967"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'firefox',
            'content' =>"\F0239"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fireplace',
            'content' =>"\F0E2E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fireplace-off',
            'content' =>"\F0E2F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'firework',
            'content' =>"\F0E30"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fish',
            'content' =>"\F023A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fish-off',
            'content' =>"\F13F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fishbowl',
            'content' =>"\F0EF3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fishbowl-outline',
            'content' =>"\F0EF4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fit-to-page',
            'content' =>"\F0EF5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fit-to-page-outline',
            'content' =>"\F0EF6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag',
            'content' =>"\F023B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-checkered',
            'content' =>"\F023C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-minus',
            'content' =>"\F0B99"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-minus-outline',
            'content' =>"\F10B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-outline',
            'content' =>"\F023D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-plus',
            'content' =>"\F0B9A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-plus-outline',
            'content' =>"\F10B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-remove',
            'content' =>"\F0B9B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-remove-outline',
            'content' =>"\F10B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-triangle',
            'content' =>"\F023F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-variant',
            'content' =>"\F0240"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flag-variant-outline',
            'content' =>"\F023E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flare',
            'content' =>"\F0D72"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flash',
            'content' =>"\F0241"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flash-alert',
            'content' =>"\F0EF7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flash-alert-outline',
            'content' =>"\F0EF8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flash-auto',
            'content' =>"\F0242"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flash-circle',
            'content' =>"\F0820"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flash-off',
            'content' =>"\F0243"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flash-outline',
            'content' =>"\F06D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flash-red-eye',
            'content' =>"\F067B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flashlight',
            'content' =>"\F0244"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flashlight-off',
            'content' =>"\F0245"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask',
            'content' =>"\F0093"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty',
            'content' =>"\F0094"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty-minus',
            'content' =>"\F123A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty-minus-outline',
            'content' =>"\F123B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty-off',
            'content' =>"\F13F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty-off-outline',
            'content' =>"\F13F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty-outline',
            'content' =>"\F0095"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty-plus',
            'content' =>"\F123C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty-plus-outline',
            'content' =>"\F123D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty-remove',
            'content' =>"\F123E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-empty-remove-outline',
            'content' =>"\F123F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-minus',
            'content' =>"\F1240"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-minus-outline',
            'content' =>"\F1241"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-off',
            'content' =>"\F13F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-off-outline',
            'content' =>"\F13F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-outline',
            'content' =>"\F0096"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-plus',
            'content' =>"\F1242"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-plus-outline',
            'content' =>"\F1243"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-remove',
            'content' =>"\F1244"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-remove-outline',
            'content' =>"\F1245"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-round-bottom',
            'content' =>"\F124B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-round-bottom-empty',
            'content' =>"\F124C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-round-bottom-empty-outline',
            'content' =>"\F124D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flask-round-bottom-outline',
            'content' =>"\F124E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fleur-de-lis',
            'content' =>"\F1303"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flip-horizontal',
            'content' =>"\F10E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flip-to-back',
            'content' =>"\F0247"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flip-to-front',
            'content' =>"\F0248"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flip-vertical',
            'content' =>"\F10E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'floor-lamp',
            'content' =>"\F08DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'floor-lamp-dual',
            'content' =>"\F1040"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'floor-lamp-variant',
            'content' =>"\F1041"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'floor-plan',
            'content' =>"\F0821"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'floppy',
            'content' =>"\F0249"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'floppy-variant',
            'content' =>"\F09EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flower',
            'content' =>"\F024A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flower-outline',
            'content' =>"\F09F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flower-poppy',
            'content' =>"\F0D08"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flower-tulip',
            'content' =>"\F09F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'flower-tulip-outline',
            'content' =>"\F09F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'focus-auto',
            'content' =>"\F0F4E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'focus-field',
            'content' =>"\F0F4F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'focus-field-horizontal',
            'content' =>"\F0F50"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'focus-field-vertical',
            'content' =>"\F0F51"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder',
            'content' =>"\F024B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-account',
            'content' =>"\F024C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-account-outline',
            'content' =>"\F0B9C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-alert',
            'content' =>"\F0DCC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-alert-outline',
            'content' =>"\F0DCD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-clock',
            'content' =>"\F0ABA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-clock-outline',
            'content' =>"\F0ABB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-cog',
            'content' =>"\F107F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-cog-outline',
            'content' =>"\F1080"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-download',
            'content' =>"\F024D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-download-outline',
            'content' =>"\F10E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-edit',
            'content' =>"\F08DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-edit-outline',
            'content' =>"\F0DCE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-google-drive',
            'content' =>"\F024E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-heart',
            'content' =>"\F10EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-heart-outline',
            'content' =>"\F10EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-home',
            'content' =>"\F10B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-home-outline',
            'content' =>"\F10B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-image',
            'content' =>"\F024F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-information',
            'content' =>"\F10B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-information-outline',
            'content' =>"\F10B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-key',
            'content' =>"\F08AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-key-network',
            'content' =>"\F08AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-key-network-outline',
            'content' =>"\F0C80"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-key-outline',
            'content' =>"\F10EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-lock',
            'content' =>"\F0250"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-lock-open',
            'content' =>"\F0251"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-marker',
            'content' =>"\F126D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-marker-outline',
            'content' =>"\F126E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-move',
            'content' =>"\F0252"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-move-outline',
            'content' =>"\F1246"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-multiple',
            'content' =>"\F0253"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-multiple-image',
            'content' =>"\F0254"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-multiple-outline',
            'content' =>"\F0255"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-multiple-plus',
            'content' =>"\F147E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-multiple-plus-outline',
            'content' =>"\F147F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-music',
            'content' =>"\F1359"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-music-outline',
            'content' =>"\F135A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-network',
            'content' =>"\F0870"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-network-outline',
            'content' =>"\F0C81"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-open',
            'content' =>"\F0770"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-open-outline',
            'content' =>"\F0DCF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-outline',
            'content' =>"\F0256"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-plus',
            'content' =>"\F0257"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-plus-outline',
            'content' =>"\F0B9D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-pound',
            'content' =>"\F0D09"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-pound-outline',
            'content' =>"\F0D0A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-refresh',
            'content' =>"\F0749"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-refresh-outline',
            'content' =>"\F0542"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-remove',
            'content' =>"\F0258"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-remove-outline',
            'content' =>"\F0B9E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-search',
            'content' =>"\F0968"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-search-outline',
            'content' =>"\F0969"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-settings',
            'content' =>"\F107D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-settings-outline',
            'content' =>"\F107E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-star',
            'content' =>"\F069D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-star-multiple',
            'content' =>"\F13D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-star-multiple-outline',
            'content' =>"\F13D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-star-outline',
            'content' =>"\F0B9F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-swap',
            'content' =>"\F0FB6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-swap-outline',
            'content' =>"\F0FB7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-sync',
            'content' =>"\F0D0B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-sync-outline',
            'content' =>"\F0D0C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-table',
            'content' =>"\F12E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-table-outline',
            'content' =>"\F12E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-text',
            'content' =>"\F0C82"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-text-outline',
            'content' =>"\F0C83"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-upload',
            'content' =>"\F0259"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-upload-outline',
            'content' =>"\F10ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-zip',
            'content' =>"\F06EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'folder-zip-outline',
            'content' =>"\F07B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'font-awesome',
            'content' =>"\F003A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food',
            'content' =>"\F025A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-apple',
            'content' =>"\F025B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-apple-outline',
            'content' =>"\F0C84"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-croissant',
            'content' =>"\F07C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-drumstick',
            'content' =>"\F141F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-drumstick-off',
            'content' =>"\F1468"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-drumstick-off-outline',
            'content' =>"\F1469"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-drumstick-outline',
            'content' =>"\F1420"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-fork-drink',
            'content' =>"\F05F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-off',
            'content' =>"\F05F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-steak',
            'content' =>"\F146A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-steak-off',
            'content' =>"\F146B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-variant',
            'content' =>"\F025C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'food-variant-off',
            'content' =>"\F13E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'foot-print',
            'content' =>"\F0F52"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'football',
            'content' =>"\F025D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'football-australian',
            'content' =>"\F025E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'football-helmet',
            'content' =>"\F025F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'forklift',
            'content' =>"\F07C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'form-dropdown',
            'content' =>"\F1400"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'form-select',
            'content' =>"\F1401"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'form-textarea',
            'content' =>"\F1095"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'form-textbox',
            'content' =>"\F060E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'form-textbox-lock',
            'content' =>"\F135D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'form-textbox-password',
            'content' =>"\F07F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-align-bottom',
            'content' =>"\F0753"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-align-center',
            'content' =>"\F0260"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-align-justify',
            'content' =>"\F0261"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-align-left',
            'content' =>"\F0262"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-align-middle',
            'content' =>"\F0754"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-align-right',
            'content' =>"\F0263"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-align-top',
            'content' =>"\F0755"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-annotation-minus',
            'content' =>"\F0ABC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-annotation-plus',
            'content' =>"\F0646"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-bold',
            'content' =>"\F0264"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-clear',
            'content' =>"\F0265"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-color-fill',
            'content' =>"\F0266"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-color-highlight',
            'content' =>"\F0E31"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-color-marker-cancel',
            'content' =>"\F1313"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-color-text',
            'content' =>"\F069E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-columns',
            'content' =>"\F08DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-float-center',
            'content' =>"\F0267"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-float-left',
            'content' =>"\F0268"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-float-none',
            'content' =>"\F0269"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-float-right',
            'content' =>"\F026A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-font',
            'content' =>"\F06D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-font-size-decrease',
            'content' =>"\F09F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-font-size-increase',
            'content' =>"\F09F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-1',
            'content' =>"\F026B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-2',
            'content' =>"\F026C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-3',
            'content' =>"\F026D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-4',
            'content' =>"\F026E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-5',
            'content' =>"\F026F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-6',
            'content' =>"\F0270"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-decrease',
            'content' =>"\F0271"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-equal',
            'content' =>"\F0272"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-increase',
            'content' =>"\F0273"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-header-pound',
            'content' =>"\F0274"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-horizontal-align-center',
            'content' =>"\F061E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-horizontal-align-left',
            'content' =>"\F061F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-horizontal-align-right',
            'content' =>"\F0620"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-indent-decrease',
            'content' =>"\F0275"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-indent-increase',
            'content' =>"\F0276"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-italic',
            'content' =>"\F0277"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-letter-case',
            'content' =>"\F0B34"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-letter-case-lower',
            'content' =>"\F0B35"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-letter-case-upper',
            'content' =>"\F0B36"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-letter-ends-with',
            'content' =>"\F0FB8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-letter-matches',
            'content' =>"\F0FB9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-letter-starts-with',
            'content' =>"\F0FBA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-line-spacing',
            'content' =>"\F0278"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-line-style',
            'content' =>"\F05C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-line-weight',
            'content' =>"\F05C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-list-bulleted',
            'content' =>"\F0279"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-list-bulleted-square',
            'content' =>"\F0DD0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-list-bulleted-triangle',
            'content' =>"\F0EB2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-list-bulleted-type',
            'content' =>"\F027A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-list-checkbox',
            'content' =>"\F096A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-list-checks',
            'content' =>"\F0756"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-list-numbered',
            'content' =>"\F027B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-list-numbered-rtl',
            'content' =>"\F0D0D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-list-text',
            'content' =>"\F126F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-overline',
            'content' =>"\F0EB3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-page-break',
            'content' =>"\F06D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-paint',
            'content' =>"\F027C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-paragraph',
            'content' =>"\F027D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-pilcrow',
            'content' =>"\F06D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-quote-close',
            'content' =>"\F027E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-quote-close-outline',
            'content' =>"\F11A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-quote-open',
            'content' =>"\F0757"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-quote-open-outline',
            'content' =>"\F11A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-rotate-90',
            'content' =>"\F06AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-section',
            'content' =>"\F069F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-size',
            'content' =>"\F027F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-strikethrough',
            'content' =>"\F0280"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-strikethrough-variant',
            'content' =>"\F0281"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-subscript',
            'content' =>"\F0282"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-superscript',
            'content' =>"\F0283"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text',
            'content' =>"\F0284"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-rotation-angle-down',
            'content' =>"\F0FBB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-rotation-angle-up',
            'content' =>"\F0FBC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-rotation-down',
            'content' =>"\F0D73"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-rotation-down-vertical',
            'content' =>"\F0FBD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-rotation-none',
            'content' =>"\F0D74"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-rotation-up',
            'content' =>"\F0FBE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-rotation-vertical',
            'content' =>"\F0FBF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-variant',
            'content' =>"\F0E32"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-wrapping-clip',
            'content' =>"\F0D0E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-wrapping-overflow',
            'content' =>"\F0D0F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-text-wrapping-wrap',
            'content' =>"\F0D10"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-textbox',
            'content' =>"\F0D11"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-textdirection-l-to-r',
            'content' =>"\F0285"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-textdirection-r-to-l',
            'content' =>"\F0286"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-title',
            'content' =>"\F05F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-underline',
            'content' =>"\F0287"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-vertical-align-bottom',
            'content' =>"\F0621"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-vertical-align-center',
            'content' =>"\F0622"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-vertical-align-top',
            'content' =>"\F0623"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-wrap-inline',
            'content' =>"\F0288"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-wrap-square',
            'content' =>"\F0289"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-wrap-tight',
            'content' =>"\F028A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'format-wrap-top-bottom',
            'content' =>"\F028B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'forum',
            'content' =>"\F028C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'forum-outline',
            'content' =>"\F0822"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'forward',
            'content' =>"\F028D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'forwardburger',
            'content' =>"\F0D75"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fountain',
            'content' =>"\F096B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fountain-pen',
            'content' =>"\F0D12"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fountain-pen-tip',
            'content' =>"\F0D13"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'freebsd',
            'content' =>"\F08E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'frequently-asked-questions',
            'content' =>"\F0EB4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fridge',
            'content' =>"\F0290"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fridge-alert',
            'content' =>"\F11B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fridge-alert-outline',
            'content' =>"\F11B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fridge-bottom',
            'content' =>"\F0292"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fridge-off',
            'content' =>"\F11AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fridge-off-outline',
            'content' =>"\F11B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fridge-outline',
            'content' =>"\F028F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fridge-top',
            'content' =>"\F0291"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fruit-cherries',
            'content' =>"\F1042"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fruit-cherries-off',
            'content' =>"\F13F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fruit-citrus',
            'content' =>"\F1043"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fruit-citrus-off',
            'content' =>"\F13F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fruit-grapes',
            'content' =>"\F1044"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fruit-grapes-outline',
            'content' =>"\F1045"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fruit-pineapple',
            'content' =>"\F1046"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fruit-watermelon',
            'content' =>"\F1047"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fuel',
            'content' =>"\F07CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fullscreen',
            'content' =>"\F0293"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fullscreen-exit',
            'content' =>"\F0294"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'function',
            'content' =>"\F0295"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'function-variant',
            'content' =>"\F0871"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'furigana-horizontal',
            'content' =>"\F1081"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'furigana-vertical',
            'content' =>"\F1082"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fuse',
            'content' =>"\F0C85"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fuse-alert',
            'content' =>"\F142D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fuse-blade',
            'content' =>"\F0C86"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'fuse-off',
            'content' =>"\F142C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad',
            'content' =>"\F0296"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-circle',
            'content' =>"\F0E33"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-circle-down',
            'content' =>"\F0E34"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-circle-left',
            'content' =>"\F0E35"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-circle-outline',
            'content' =>"\F0E36"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-circle-right',
            'content' =>"\F0E37"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-circle-up',
            'content' =>"\F0E38"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-down',
            'content' =>"\F0E39"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-left',
            'content' =>"\F0E3A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-right',
            'content' =>"\F0E3B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-round',
            'content' =>"\F0E3C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-round-down',
            'content' =>"\F0E3D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-round-left',
            'content' =>"\F0E3E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-round-outline',
            'content' =>"\F0E3F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-round-right',
            'content' =>"\F0E40"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-round-up',
            'content' =>"\F0E41"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-square',
            'content' =>"\F0EB5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-square-outline',
            'content' =>"\F0EB6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-up',
            'content' =>"\F0E42"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-variant',
            'content' =>"\F0297"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamepad-variant-outline',
            'content' =>"\F0EB7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gamma',
            'content' =>"\F10EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gantry-crane',
            'content' =>"\F0DD1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'garage',
            'content' =>"\F06D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'garage-alert',
            'content' =>"\F0872"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'garage-alert-variant',
            'content' =>"\F12D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'garage-open',
            'content' =>"\F06DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'garage-open-variant',
            'content' =>"\F12D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'garage-variant',
            'content' =>"\F12D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gas-cylinder',
            'content' =>"\F0647"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gas-station',
            'content' =>"\F0298"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gas-station-off',
            'content' =>"\F1409"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gas-station-off-outline',
            'content' =>"\F140A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gas-station-outline',
            'content' =>"\F0EB8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate',
            'content' =>"\F0299"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate-and',
            'content' =>"\F08E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate-arrow-right',
            'content' =>"\F1169"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate-nand',
            'content' =>"\F08E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate-nor',
            'content' =>"\F08E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate-not',
            'content' =>"\F08E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate-open',
            'content' =>"\F116A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate-or',
            'content' =>"\F08E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate-xnor',
            'content' =>"\F08E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gate-xor',
            'content' =>"\F08E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gatsby',
            'content' =>"\F0E43"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gauge',
            'content' =>"\F029A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gauge-empty',
            'content' =>"\F0873"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gauge-full',
            'content' =>"\F0874"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gauge-low',
            'content' =>"\F0875"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gavel',
            'content' =>"\F029B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gender-female',
            'content' =>"\F029C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gender-male',
            'content' =>"\F029D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gender-male-female',
            'content' =>"\F029E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gender-male-female-variant',
            'content' =>"\F113F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gender-non-binary',
            'content' =>"\F1140"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gender-transgender',
            'content' =>"\F029F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gentoo',
            'content' =>"\F08E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture',
            'content' =>"\F07CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-double-tap',
            'content' =>"\F073C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-pinch',
            'content' =>"\F0ABD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-spread',
            'content' =>"\F0ABE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-swipe',
            'content' =>"\F0D76"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-swipe-down',
            'content' =>"\F073D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-swipe-horizontal',
            'content' =>"\F0ABF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-swipe-left',
            'content' =>"\F073E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-swipe-right',
            'content' =>"\F073F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-swipe-up',
            'content' =>"\F0740"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-swipe-vertical',
            'content' =>"\F0AC0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-tap',
            'content' =>"\F0741"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-tap-box',
            'content' =>"\F12A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-tap-button',
            'content' =>"\F12A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-tap-hold',
            'content' =>"\F0D77"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-two-double-tap',
            'content' =>"\F0742"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gesture-two-tap',
            'content' =>"\F0743"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ghost',
            'content' =>"\F02A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ghost-off',
            'content' =>"\F09F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gif',
            'content' =>"\F0D78"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gift',
            'content' =>"\F0E44"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gift-outline',
            'content' =>"\F02A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'git',
            'content' =>"\F02A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'github',
            'content' =>"\F02A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gitlab',
            'content' =>"\F0BA0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'glass-cocktail',
            'content' =>"\F0356"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'glass-flute',
            'content' =>"\F02A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'glass-mug',
            'content' =>"\F02A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'glass-mug-variant',
            'content' =>"\F1116"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'glass-pint-outline',
            'content' =>"\F130D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'glass-stange',
            'content' =>"\F02A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'glass-tulip',
            'content' =>"\F02A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'glass-wine',
            'content' =>"\F0876"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'glasses',
            'content' =>"\F02AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'globe-light',
            'content' =>"\F12D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'globe-model',
            'content' =>"\F08E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gmail',
            'content' =>"\F02AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gnome',
            'content' =>"\F02AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'go-kart',
            'content' =>"\F0D79"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'go-kart-track',
            'content' =>"\F0D7A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gog',
            'content' =>"\F0BA1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gold',
            'content' =>"\F124F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'golf',
            'content' =>"\F0823"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'golf-cart',
            'content' =>"\F11A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'golf-tee',
            'content' =>"\F1083"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gondola',
            'content' =>"\F0686"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'goodreads',
            'content' =>"\F0D7B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google',
            'content' =>"\F02AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-ads',
            'content' =>"\F0C87"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-analytics',
            'content' =>"\F07CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-assistant',
            'content' =>"\F07CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-cardboard',
            'content' =>"\F02AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-chrome',
            'content' =>"\F02AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-circles',
            'content' =>"\F02B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-circles-communities',
            'content' =>"\F02B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-circles-extended',
            'content' =>"\F02B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-circles-group',
            'content' =>"\F02B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-classroom',
            'content' =>"\F02C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-cloud',
            'content' =>"\F11F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-controller',
            'content' =>"\F02B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-controller-off',
            'content' =>"\F02B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-downasaur',
            'content' =>"\F1362"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-drive',
            'content' =>"\F02B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-earth',
            'content' =>"\F02B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-fit',
            'content' =>"\F096C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-glass',
            'content' =>"\F02B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-hangouts',
            'content' =>"\F02C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-home',
            'content' =>"\F0824"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-keep',
            'content' =>"\F06DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-lens',
            'content' =>"\F09F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-maps',
            'content' =>"\F05F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-my-business',
            'content' =>"\F1048"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-nearby',
            'content' =>"\F02B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-photos',
            'content' =>"\F06DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-play',
            'content' =>"\F02BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-plus',
            'content' =>"\F02BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-podcast',
            'content' =>"\F0EB9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-spreadsheet',
            'content' =>"\F09F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-street-view',
            'content' =>"\F0C88"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'google-translate',
            'content' =>"\F02BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'gradient',
            'content' =>"\F06A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'grain',
            'content' =>"\F0D7C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'graph',
            'content' =>"\F1049"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'graph-outline',
            'content' =>"\F104A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'graphql',
            'content' =>"\F0877"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'grave-stone',
            'content' =>"\F0BA2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'grease-pencil',
            'content' =>"\F0648"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'greater-than',
            'content' =>"\F096D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'greater-than-or-equal',
            'content' =>"\F096E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'grid',
            'content' =>"\F02C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'grid-large',
            'content' =>"\F0758"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'grid-off',
            'content' =>"\F02C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'grill',
            'content' =>"\F0E45"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'grill-outline',
            'content' =>"\F118A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'group',
            'content' =>"\F02C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'guitar-acoustic',
            'content' =>"\F0771"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'guitar-electric',
            'content' =>"\F02C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'guitar-pick',
            'content' =>"\F02C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'guitar-pick-outline',
            'content' =>"\F02C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'guy-fawkes-mask',
            'content' =>"\F0825"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hail',
            'content' =>"\F0AC1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hair-dryer',
            'content' =>"\F10EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hair-dryer-outline',
            'content' =>"\F10F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'halloween',
            'content' =>"\F0BA3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hamburger',
            'content' =>"\F0685"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hammer',
            'content' =>"\F08EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hammer-screwdriver',
            'content' =>"\F1322"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hammer-wrench',
            'content' =>"\F1323"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand',
            'content' =>"\F0A4F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-heart',
            'content' =>"\F10F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-left',
            'content' =>"\F0E46"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-okay',
            'content' =>"\F0A50"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-peace',
            'content' =>"\F0A51"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-peace-variant',
            'content' =>"\F0A52"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-pointing-down',
            'content' =>"\F0A53"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-pointing-left',
            'content' =>"\F0A54"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-pointing-right',
            'content' =>"\F02C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-pointing-up',
            'content' =>"\F0A55"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-right',
            'content' =>"\F0E47"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-saw',
            'content' =>"\F0E48"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hand-water',
            'content' =>"\F139F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'handball',
            'content' =>"\F0F53"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'handcuffs',
            'content' =>"\F113E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'handshake',
            'content' =>"\F1218"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hanger',
            'content' =>"\F02C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hard-hat',
            'content' =>"\F096F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'harddisk',
            'content' =>"\F02CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'harddisk-plus',
            'content' =>"\F104B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'harddisk-remove',
            'content' =>"\F104C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hat-fedora',
            'content' =>"\F0BA4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hazard-lights',
            'content' =>"\F0C89"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hdr',
            'content' =>"\F0D7D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hdr-off',
            'content' =>"\F0D7E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head',
            'content' =>"\F135E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-alert',
            'content' =>"\F1338"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-alert-outline',
            'content' =>"\F1339"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-check',
            'content' =>"\F133A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-check-outline',
            'content' =>"\F133B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-cog',
            'content' =>"\F133C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-cog-outline',
            'content' =>"\F133D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-dots-horizontal',
            'content' =>"\F133E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-dots-horizontal-outline',
            'content' =>"\F133F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-flash',
            'content' =>"\F1340"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-flash-outline',
            'content' =>"\F1341"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-heart',
            'content' =>"\F1342"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-heart-outline',
            'content' =>"\F1343"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-lightbulb',
            'content' =>"\F1344"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-lightbulb-outline',
            'content' =>"\F1345"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-minus',
            'content' =>"\F1346"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-minus-outline',
            'content' =>"\F1347"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-outline',
            'content' =>"\F135F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-plus',
            'content' =>"\F1348"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-plus-outline',
            'content' =>"\F1349"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-question',
            'content' =>"\F134A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-question-outline',
            'content' =>"\F134B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-remove',
            'content' =>"\F134C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-remove-outline',
            'content' =>"\F134D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-snowflake',
            'content' =>"\F134E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-snowflake-outline',
            'content' =>"\F134F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-sync',
            'content' =>"\F1350"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'head-sync-outline',
            'content' =>"\F1351"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'headphones',
            'content' =>"\F02CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'headphones-bluetooth',
            'content' =>"\F0970"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'headphones-box',
            'content' =>"\F02CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'headphones-off',
            'content' =>"\F07CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'headphones-settings',
            'content' =>"\F02CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'headset',
            'content' =>"\F02CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'headset-dock',
            'content' =>"\F02CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'headset-off',
            'content' =>"\F02D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart',
            'content' =>"\F02D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-box',
            'content' =>"\F02D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-box-outline',
            'content' =>"\F02D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-broken',
            'content' =>"\F02D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-broken-outline',
            'content' =>"\F0D14"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-circle',
            'content' =>"\F0971"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-circle-outline',
            'content' =>"\F0972"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-flash',
            'content' =>"\F0EF9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-half',
            'content' =>"\F06DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-half-full',
            'content' =>"\F06DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-half-outline',
            'content' =>"\F06E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-minus',
            'content' =>"\F142F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-minus-outline',
            'content' =>"\F1432"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-multiple',
            'content' =>"\F0A56"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-multiple-outline',
            'content' =>"\F0A57"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-off',
            'content' =>"\F0759"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-off-outline',
            'content' =>"\F1434"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-outline',
            'content' =>"\F02D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-plus',
            'content' =>"\F142E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-plus-outline',
            'content' =>"\F1431"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-pulse',
            'content' =>"\F05F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-remove',
            'content' =>"\F1430"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'heart-remove-outline',
            'content' =>"\F1433"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'helicopter',
            'content' =>"\F0AC2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'help',
            'content' =>"\F02D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'help-box',
            'content' =>"\F078B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'help-circle',
            'content' =>"\F02D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'help-circle-outline',
            'content' =>"\F0625"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'help-network',
            'content' =>"\F06F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'help-network-outline',
            'content' =>"\F0C8A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'help-rhombus',
            'content' =>"\F0BA5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'help-rhombus-outline',
            'content' =>"\F0BA6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexadecimal',
            'content' =>"\F12A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon',
            'content' =>"\F02D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon-multiple',
            'content' =>"\F06E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon-multiple-outline',
            'content' =>"\F10F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon-outline',
            'content' =>"\F02D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon-slice-1',
            'content' =>"\F0AC3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon-slice-2',
            'content' =>"\F0AC4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon-slice-3',
            'content' =>"\F0AC5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon-slice-4',
            'content' =>"\F0AC6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon-slice-5',
            'content' =>"\F0AC7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagon-slice-6',
            'content' =>"\F0AC8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagram',
            'content' =>"\F0AC9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hexagram-outline',
            'content' =>"\F0ACA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'high-definition',
            'content' =>"\F07CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'high-definition-box',
            'content' =>"\F0878"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'highway',
            'content' =>"\F05F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hiking',
            'content' =>"\F0D7F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hinduism',
            'content' =>"\F0973"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'history',
            'content' =>"\F02DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hockey-puck',
            'content' =>"\F0879"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hockey-sticks',
            'content' =>"\F087A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hololens',
            'content' =>"\F02DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home',
            'content' =>"\F02DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-account',
            'content' =>"\F0826"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-alert',
            'content' =>"\F087B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-analytics',
            'content' =>"\F0EBA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-assistant',
            'content' =>"\F07D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-automation',
            'content' =>"\F07D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-circle',
            'content' =>"\F07D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-circle-outline',
            'content' =>"\F104D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-city',
            'content' =>"\F0D15"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-city-outline',
            'content' =>"\F0D16"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-currency-usd',
            'content' =>"\F08AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-edit',
            'content' =>"\F1159"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-edit-outline',
            'content' =>"\F115A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-export-outline',
            'content' =>"\F0F9B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-flood',
            'content' =>"\F0EFA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-floor-0',
            'content' =>"\F0DD2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-floor-1',
            'content' =>"\F0D80"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-floor-2',
            'content' =>"\F0D81"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-floor-3',
            'content' =>"\F0D82"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-floor-a',
            'content' =>"\F0D83"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-floor-b',
            'content' =>"\F0D84"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-floor-g',
            'content' =>"\F0D85"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-floor-l',
            'content' =>"\F0D86"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-floor-negative-1',
            'content' =>"\F0DD3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-group',
            'content' =>"\F0DD4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-heart',
            'content' =>"\F0827"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-import-outline',
            'content' =>"\F0F9C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-lightbulb',
            'content' =>"\F1251"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-lightbulb-outline',
            'content' =>"\F1252"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-lock',
            'content' =>"\F08EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-lock-open',
            'content' =>"\F08EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-map-marker',
            'content' =>"\F05F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-minus',
            'content' =>"\F0974"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-minus-outline',
            'content' =>"\F13D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-modern',
            'content' =>"\F02DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-outline',
            'content' =>"\F06A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-plus',
            'content' =>"\F0975"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-plus-outline',
            'content' =>"\F13D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-remove',
            'content' =>"\F1247"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-remove-outline',
            'content' =>"\F13D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-roof',
            'content' =>"\F112B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-search',
            'content' =>"\F13B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-search-outline',
            'content' =>"\F13B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-thermometer',
            'content' =>"\F0F54"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-thermometer-outline',
            'content' =>"\F0F55"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-variant',
            'content' =>"\F02DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'home-variant-outline',
            'content' =>"\F0BA7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hook',
            'content' =>"\F06E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hook-off',
            'content' =>"\F06E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hops',
            'content' =>"\F02DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'horizontal-rotate-clockwise',
            'content' =>"\F10F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'horizontal-rotate-counterclockwise',
            'content' =>"\F10F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'horseshoe',
            'content' =>"\F0A58"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hospital',
            'content' =>"\F0FF6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hospital-box',
            'content' =>"\F02E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hospital-box-outline',
            'content' =>"\F0FF7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hospital-building',
            'content' =>"\F02E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hospital-marker',
            'content' =>"\F02E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hot-tub',
            'content' =>"\F0828"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hours-24',
            'content' =>"\F1478"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hubspot',
            'content' =>"\F0D17"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hulu',
            'content' =>"\F0829"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human',
            'content' =>"\F02E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-baby-changing-table',
            'content' =>"\F138B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-child',
            'content' =>"\F02E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-female',
            'content' =>"\F0649"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-female-boy',
            'content' =>"\F0A59"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-female-female',
            'content' =>"\F0A5A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-female-girl',
            'content' =>"\F0A5B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-greeting',
            'content' =>"\F064A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-handsdown',
            'content' =>"\F064B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-handsup',
            'content' =>"\F064C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-male',
            'content' =>"\F064D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-male-boy',
            'content' =>"\F0A5C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-male-child',
            'content' =>"\F138C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-male-female',
            'content' =>"\F02E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-male-girl',
            'content' =>"\F0A5D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-male-height',
            'content' =>"\F0EFB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-male-height-variant',
            'content' =>"\F0EFC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-male-male',
            'content' =>"\F0A5E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-pregnant',
            'content' =>"\F05CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'human-wheelchair',
            'content' =>"\F138D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'humble-bundle',
            'content' =>"\F0744"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hvac',
            'content' =>"\F1352"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hydraulic-oil-level',
            'content' =>"\F1324"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hydraulic-oil-temperature',
            'content' =>"\F1325"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'hydro-power',
            'content' =>"\F12E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ice-cream',
            'content' =>"\F082A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ice-cream-off',
            'content' =>"\F0E52"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ice-pop',
            'content' =>"\F0EFD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'id-card',
            'content' =>"\F0FC0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'identifier',
            'content' =>"\F0EFE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ideogram-cjk',
            'content' =>"\F1331"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ideogram-cjk-variant',
            'content' =>"\F1332"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe',
            'content' =>"\F0C8B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe-array',
            'content' =>"\F10F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe-array-outline',
            'content' =>"\F10F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe-braces',
            'content' =>"\F10F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe-braces-outline',
            'content' =>"\F10F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe-outline',
            'content' =>"\F0C8C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe-parentheses',
            'content' =>"\F10F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe-parentheses-outline',
            'content' =>"\F10FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe-variable',
            'content' =>"\F10FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iframe-variable-outline',
            'content' =>"\F10FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image',
            'content' =>"\F02E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-album',
            'content' =>"\F02EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-area',
            'content' =>"\F02EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-area-close',
            'content' =>"\F02EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-auto-adjust',
            'content' =>"\F0FC1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-broken',
            'content' =>"\F02ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-broken-variant',
            'content' =>"\F02EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-edit',
            'content' =>"\F11E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-edit-outline',
            'content' =>"\F11E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-black-white',
            'content' =>"\F02F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-center-focus',
            'content' =>"\F02F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-center-focus-strong',
            'content' =>"\F0EFF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-center-focus-strong-outline',
            'content' =>"\F0F00"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-center-focus-weak',
            'content' =>"\F02F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-drama',
            'content' =>"\F02F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-frames',
            'content' =>"\F02F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-hdr',
            'content' =>"\F02F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-none',
            'content' =>"\F02F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-tilt-shift',
            'content' =>"\F02F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-filter-vintage',
            'content' =>"\F02F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-frame',
            'content' =>"\F0E49"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-minus',
            'content' =>"\F1419"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-move',
            'content' =>"\F09F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-multiple',
            'content' =>"\F02F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-multiple-outline',
            'content' =>"\F02EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-off',
            'content' =>"\F082B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-off-outline',
            'content' =>"\F11D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-outline',
            'content' =>"\F0976"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-plus',
            'content' =>"\F087C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-remove',
            'content' =>"\F1418"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-search',
            'content' =>"\F0977"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-search-outline',
            'content' =>"\F0978"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-size-select-actual',
            'content' =>"\F0C8D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-size-select-large',
            'content' =>"\F0C8E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'image-size-select-small',
            'content' =>"\F0C8F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'import',
            'content' =>"\F02FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox',
            'content' =>"\F0687"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox-arrow-down',
            'content' =>"\F02FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox-arrow-down-outline',
            'content' =>"\F1270"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox-arrow-up',
            'content' =>"\F03D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox-arrow-up-outline',
            'content' =>"\F1271"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox-full',
            'content' =>"\F1272"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox-full-outline',
            'content' =>"\F1273"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox-multiple',
            'content' =>"\F08B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox-multiple-outline',
            'content' =>"\F0BA8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'inbox-outline',
            'content' =>"\F1274"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'incognito',
            'content' =>"\F05F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'incognito-circle',
            'content' =>"\F1421"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'incognito-circle-off',
            'content' =>"\F1422"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'incognito-off',
            'content' =>"\F0075"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'infinity',
            'content' =>"\F06E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'information',
            'content' =>"\F02FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'information-outline',
            'content' =>"\F02FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'information-variant',
            'content' =>"\F064E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'instagram',
            'content' =>"\F02FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'instrument-triangle',
            'content' =>"\F104E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'invert-colors',
            'content' =>"\F0301"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'invert-colors-off',
            'content' =>"\F0E4A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iobroker',
            'content' =>"\F12E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ip',
            'content' =>"\F0A5F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ip-network',
            'content' =>"\F0A60"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ip-network-outline',
            'content' =>"\F0C90"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ipod',
            'content' =>"\F0C91"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'islam',
            'content' =>"\F0979"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'island',
            'content' =>"\F104F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'iv-bag',
            'content' =>"\F10B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'jabber',
            'content' =>"\F0DD5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'jeepney',
            'content' =>"\F0302"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'jellyfish',
            'content' =>"\F0F01"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'jellyfish-outline',
            'content' =>"\F0F02"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'jira',
            'content' =>"\F0303"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'jquery',
            'content' =>"\F087D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'jsfiddle',
            'content' =>"\F0304"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'judaism',
            'content' =>"\F097A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'jump-rope',
            'content' =>"\F12FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kabaddi',
            'content' =>"\F0D87"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'karate',
            'content' =>"\F082C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keg',
            'content' =>"\F0305"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kettle',
            'content' =>"\F05FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kettle-alert',
            'content' =>"\F1317"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kettle-alert-outline',
            'content' =>"\F1318"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kettle-off',
            'content' =>"\F131B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kettle-off-outline',
            'content' =>"\F131C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kettle-outline',
            'content' =>"\F0F56"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kettle-steam',
            'content' =>"\F1319"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kettle-steam-outline',
            'content' =>"\F131A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kettlebell',
            'content' =>"\F1300"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key',
            'content' =>"\F0306"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-arrow-right',
            'content' =>"\F1312"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-change',
            'content' =>"\F0307"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-link',
            'content' =>"\F119F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-minus',
            'content' =>"\F0308"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-outline',
            'content' =>"\F0DD6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-plus',
            'content' =>"\F0309"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-remove',
            'content' =>"\F030A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-star',
            'content' =>"\F119E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-variant',
            'content' =>"\F030B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'key-wireless',
            'content' =>"\F0FC2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard',
            'content' =>"\F030C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-backspace',
            'content' =>"\F030D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-caps',
            'content' =>"\F030E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-close',
            'content' =>"\F030F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-esc',
            'content' =>"\F12B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f1',
            'content' =>"\F12AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f10',
            'content' =>"\F12B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f11',
            'content' =>"\F12B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f12',
            'content' =>"\F12B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f2',
            'content' =>"\F12AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f3',
            'content' =>"\F12AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f4',
            'content' =>"\F12AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f5',
            'content' =>"\F12AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f6',
            'content' =>"\F12B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f7',
            'content' =>"\F12B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f8',
            'content' =>"\F12B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-f9',
            'content' =>"\F12B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-off',
            'content' =>"\F0310"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-off-outline',
            'content' =>"\F0E4B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-outline',
            'content' =>"\F097B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-return',
            'content' =>"\F0311"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-settings',
            'content' =>"\F09F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-settings-outline',
            'content' =>"\F09FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-space',
            'content' =>"\F1050"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-tab',
            'content' =>"\F0312"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'keyboard-variant',
            'content' =>"\F0313"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'khanda',
            'content' =>"\F10FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kickstarter',
            'content' =>"\F0745"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'klingon',
            'content' =>"\F135B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'knife',
            'content' =>"\F09FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'knife-military',
            'content' =>"\F09FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kodi',
            'content' =>"\F0314"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'kubernetes',
            'content' =>"\F10FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label',
            'content' =>"\F0315"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label-multiple',
            'content' =>"\F1375"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label-multiple-outline',
            'content' =>"\F1376"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label-off',
            'content' =>"\F0ACB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label-off-outline',
            'content' =>"\F0ACC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label-outline',
            'content' =>"\F0316"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label-percent',
            'content' =>"\F12EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label-percent-outline',
            'content' =>"\F12EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label-variant',
            'content' =>"\F0ACD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'label-variant-outline',
            'content' =>"\F0ACE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ladybug',
            'content' =>"\F082D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lambda',
            'content' =>"\F0627"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lamp',
            'content' =>"\F06B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lan',
            'content' =>"\F0317"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lan-check',
            'content' =>"\F12AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lan-connect',
            'content' =>"\F0318"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lan-disconnect',
            'content' =>"\F0319"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lan-pending',
            'content' =>"\F031A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-c',
            'content' =>"\F0671"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-cpp',
            'content' =>"\F0672"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-csharp',
            'content' =>"\F031B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-css3',
            'content' =>"\F031C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-fortran',
            'content' =>"\F121A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-go',
            'content' =>"\F07D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-haskell',
            'content' =>"\F0C92"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-html5',
            'content' =>"\F031D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-java',
            'content' =>"\F0B37"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-javascript',
            'content' =>"\F031E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-kotlin',
            'content' =>"\F1219"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-lua',
            'content' =>"\F08B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-markdown',
            'content' =>"\F0354"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-markdown-outline',
            'content' =>"\F0F5B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-php',
            'content' =>"\F031F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-python',
            'content' =>"\F0320"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-r',
            'content' =>"\F07D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-ruby',
            'content' =>"\F0D2D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-ruby-on-rails',
            'content' =>"\F0ACF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-swift',
            'content' =>"\F06E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-typescript',
            'content' =>"\F06E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'language-xaml',
            'content' =>"\F0673"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'laptop',
            'content' =>"\F0322"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'laptop-chromebook',
            'content' =>"\F0323"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'laptop-mac',
            'content' =>"\F0324"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'laptop-off',
            'content' =>"\F06E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'laptop-windows',
            'content' =>"\F0325"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'laravel',
            'content' =>"\F0AD0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'laser-pointer',
            'content' =>"\F1484"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lasso',
            'content' =>"\F0F03"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lastpass',
            'content' =>"\F0446"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'latitude',
            'content' =>"\F0F57"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'launch',
            'content' =>"\F0327"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lava-lamp',
            'content' =>"\F07D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers',
            'content' =>"\F0328"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-minus',
            'content' =>"\F0E4C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-off',
            'content' =>"\F0329"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-off-outline',
            'content' =>"\F09FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-outline',
            'content' =>"\F09FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-plus',
            'content' =>"\F0E4D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-remove',
            'content' =>"\F0E4E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-search',
            'content' =>"\F1206"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-search-outline',
            'content' =>"\F1207"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-triple',
            'content' =>"\F0F58"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'layers-triple-outline',
            'content' =>"\F0F59"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lead-pencil',
            'content' =>"\F064F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'leaf',
            'content' =>"\F032A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'leaf-maple',
            'content' =>"\F0C93"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'leaf-maple-off',
            'content' =>"\F12DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'leaf-off',
            'content' =>"\F12D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'leak',
            'content' =>"\F0DD7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'leak-off',
            'content' =>"\F0DD8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'led-off',
            'content' =>"\F032B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'led-on',
            'content' =>"\F032C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'led-outline',
            'content' =>"\F032D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'led-strip',
            'content' =>"\F07D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'led-strip-variant',
            'content' =>"\F1051"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'led-variant-off',
            'content' =>"\F032E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'led-variant-on',
            'content' =>"\F032F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'led-variant-outline',
            'content' =>"\F0330"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'leek',
            'content' =>"\F117D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'less-than',
            'content' =>"\F097C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'less-than-or-equal',
            'content' =>"\F097D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'library',
            'content' =>"\F0331"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'library-shelves',
            'content' =>"\F0BA9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'license',
            'content' =>"\F0FC3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lifebuoy',
            'content' =>"\F087E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'light-switch',
            'content' =>"\F097E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb',
            'content' =>"\F0335"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-cfl',
            'content' =>"\F1208"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-cfl-off',
            'content' =>"\F1209"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-cfl-spiral',
            'content' =>"\F1275"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-cfl-spiral-off',
            'content' =>"\F12C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-group',
            'content' =>"\F1253"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-group-off',
            'content' =>"\F12CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-group-off-outline',
            'content' =>"\F12CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-group-outline',
            'content' =>"\F1254"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-multiple',
            'content' =>"\F1255"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-multiple-off',
            'content' =>"\F12CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-multiple-off-outline',
            'content' =>"\F12D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-multiple-outline',
            'content' =>"\F1256"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-off',
            'content' =>"\F0E4F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-off-outline',
            'content' =>"\F0E50"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-on',
            'content' =>"\F06E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-on-outline',
            'content' =>"\F06E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightbulb-outline',
            'content' =>"\F0336"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lighthouse',
            'content' =>"\F09FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lighthouse-on',
            'content' =>"\F0A00"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightning-bolt',
            'content' =>"\F140B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lightning-bolt-outline',
            'content' =>"\F140C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lingerie',
            'content' =>"\F1476"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link',
            'content' =>"\F0337"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-box',
            'content' =>"\F0D1A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-box-outline',
            'content' =>"\F0D1B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-box-variant',
            'content' =>"\F0D1C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-box-variant-outline',
            'content' =>"\F0D1D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-lock',
            'content' =>"\F10BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-off',
            'content' =>"\F0338"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-plus',
            'content' =>"\F0C94"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-variant',
            'content' =>"\F0339"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-variant-minus',
            'content' =>"\F10FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-variant-off',
            'content' =>"\F033A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-variant-plus',
            'content' =>"\F1100"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'link-variant-remove',
            'content' =>"\F1101"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'linkedin',
            'content' =>"\F033B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'linux',
            'content' =>"\F033D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'linux-mint',
            'content' =>"\F08ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lipstick',
            'content' =>"\F13B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'litecoin',
            'content' =>"\F0A61"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'loading',
            'content' =>"\F0772"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'location-enter',
            'content' =>"\F0FC4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'location-exit',
            'content' =>"\F0FC5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock',
            'content' =>"\F033E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-alert',
            'content' =>"\F08EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-check',
            'content' =>"\F139A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-clock',
            'content' =>"\F097F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-open',
            'content' =>"\F033F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-open-alert',
            'content' =>"\F139B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-open-check',
            'content' =>"\F139C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-open-outline',
            'content' =>"\F0340"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-open-variant',
            'content' =>"\F0FC6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-open-variant-outline',
            'content' =>"\F0FC7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-outline',
            'content' =>"\F0341"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-pattern',
            'content' =>"\F06EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-plus',
            'content' =>"\F05FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-question',
            'content' =>"\F08EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-reset',
            'content' =>"\F0773"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lock-smart',
            'content' =>"\F08B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'locker',
            'content' =>"\F07D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'locker-multiple',
            'content' =>"\F07D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'login',
            'content' =>"\F0342"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'login-variant',
            'content' =>"\F05FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'logout',
            'content' =>"\F0343"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'logout-variant',
            'content' =>"\F05FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'longitude',
            'content' =>"\F0F5A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'looks',
            'content' =>"\F0344"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'loupe',
            'content' =>"\F0345"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lumx',
            'content' =>"\F0346"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'lungs',
            'content' =>"\F1084"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnet',
            'content' =>"\F0347"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnet-on',
            'content' =>"\F0348"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify',
            'content' =>"\F0349"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-close',
            'content' =>"\F0980"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-minus',
            'content' =>"\F034A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-minus-cursor',
            'content' =>"\F0A62"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-minus-outline',
            'content' =>"\F06EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-plus',
            'content' =>"\F034B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-plus-cursor',
            'content' =>"\F0A63"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-plus-outline',
            'content' =>"\F06ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-remove-cursor',
            'content' =>"\F120C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-remove-outline',
            'content' =>"\F120D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'magnify-scan',
            'content' =>"\F1276"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mail',
            'content' =>"\F0EBB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mailbox',
            'content' =>"\F06EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mailbox-open',
            'content' =>"\F0D88"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mailbox-open-outline',
            'content' =>"\F0D89"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mailbox-open-up',
            'content' =>"\F0D8A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mailbox-open-up-outline',
            'content' =>"\F0D8B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mailbox-outline',
            'content' =>"\F0D8C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mailbox-up',
            'content' =>"\F0D8D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mailbox-up-outline',
            'content' =>"\F0D8E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map',
            'content' =>"\F034D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-check',
            'content' =>"\F0EBC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-check-outline',
            'content' =>"\F0EBD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-clock',
            'content' =>"\F0D1E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-clock-outline',
            'content' =>"\F0D1F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-legend',
            'content' =>"\F0A01"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker',
            'content' =>"\F034E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-alert',
            'content' =>"\F0F05"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-alert-outline',
            'content' =>"\F0F06"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-check',
            'content' =>"\F0C95"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-check-outline',
            'content' =>"\F12FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-circle',
            'content' =>"\F034F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-distance',
            'content' =>"\F08F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-down',
            'content' =>"\F1102"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-left',
            'content' =>"\F12DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-left-outline',
            'content' =>"\F12DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-minus',
            'content' =>"\F0650"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-minus-outline',
            'content' =>"\F12F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-multiple',
            'content' =>"\F0350"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-multiple-outline',
            'content' =>"\F1277"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-off',
            'content' =>"\F0351"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-off-outline',
            'content' =>"\F12FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-outline',
            'content' =>"\F07D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-path',
            'content' =>"\F0D20"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-plus',
            'content' =>"\F0651"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-plus-outline',
            'content' =>"\F12F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-question',
            'content' =>"\F0F07"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-question-outline',
            'content' =>"\F0F08"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-radius',
            'content' =>"\F0352"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-radius-outline',
            'content' =>"\F12FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-remove',
            'content' =>"\F0F09"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-remove-outline',
            'content' =>"\F12FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-remove-variant',
            'content' =>"\F0F0A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-right',
            'content' =>"\F12DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-right-outline',
            'content' =>"\F12DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-marker-up',
            'content' =>"\F1103"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-minus',
            'content' =>"\F0981"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-outline',
            'content' =>"\F0982"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-plus',
            'content' =>"\F0983"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-search',
            'content' =>"\F0984"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'map-search-outline',
            'content' =>"\F0985"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mapbox',
            'content' =>"\F0BAA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'margin',
            'content' =>"\F0353"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'marker',
            'content' =>"\F0652"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'marker-cancel',
            'content' =>"\F0DD9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'marker-check',
            'content' =>"\F0355"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mastodon',
            'content' =>"\F0AD1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'material-design',
            'content' =>"\F0986"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'material-ui',
            'content' =>"\F0357"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'math-compass',
            'content' =>"\F0358"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'math-cos',
            'content' =>"\F0C96"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'math-integral',
            'content' =>"\F0FC8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'math-integral-box',
            'content' =>"\F0FC9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'math-log',
            'content' =>"\F1085"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'math-norm',
            'content' =>"\F0FCA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'math-norm-box',
            'content' =>"\F0FCB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'math-sin',
            'content' =>"\F0C97"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'math-tan',
            'content' =>"\F0C98"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'matrix',
            'content' =>"\F0628"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'medal',
            'content' =>"\F0987"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'medal-outline',
            'content' =>"\F1326"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'medical-bag',
            'content' =>"\F06EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'meditation',
            'content' =>"\F117B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'memory',
            'content' =>"\F035B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu',
            'content' =>"\F035C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-down',
            'content' =>"\F035D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-down-outline',
            'content' =>"\F06B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-left',
            'content' =>"\F035E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-left-outline',
            'content' =>"\F0A02"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-open',
            'content' =>"\F0BAB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-right',
            'content' =>"\F035F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-right-outline',
            'content' =>"\F0A03"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-swap',
            'content' =>"\F0A64"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-swap-outline',
            'content' =>"\F0A65"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-up',
            'content' =>"\F0360"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'menu-up-outline',
            'content' =>"\F06B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'merge',
            'content' =>"\F0F5C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message',
            'content' =>"\F0361"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-alert',
            'content' =>"\F0362"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-alert-outline',
            'content' =>"\F0A04"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-arrow-left',
            'content' =>"\F12F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-arrow-left-outline',
            'content' =>"\F12F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-arrow-right',
            'content' =>"\F12F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-arrow-right-outline',
            'content' =>"\F12F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-bulleted',
            'content' =>"\F06A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-bulleted-off',
            'content' =>"\F06A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-cog',
            'content' =>"\F06F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-cog-outline',
            'content' =>"\F1172"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-draw',
            'content' =>"\F0363"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-image',
            'content' =>"\F0364"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-image-outline',
            'content' =>"\F116C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-lock',
            'content' =>"\F0FCC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-lock-outline',
            'content' =>"\F116D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-minus',
            'content' =>"\F116E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-minus-outline',
            'content' =>"\F116F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-outline',
            'content' =>"\F0365"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-plus',
            'content' =>"\F0653"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-plus-outline',
            'content' =>"\F10BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-processing',
            'content' =>"\F0366"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-processing-outline',
            'content' =>"\F1170"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-reply',
            'content' =>"\F0367"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-reply-text',
            'content' =>"\F0368"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-settings',
            'content' =>"\F06F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-settings-outline',
            'content' =>"\F1171"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-text',
            'content' =>"\F0369"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-text-clock',
            'content' =>"\F1173"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-text-clock-outline',
            'content' =>"\F1174"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-text-lock',
            'content' =>"\F0FCD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-text-lock-outline',
            'content' =>"\F1175"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-text-outline',
            'content' =>"\F036A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'message-video',
            'content' =>"\F036B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'meteor',
            'content' =>"\F0629"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'metronome',
            'content' =>"\F07DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'metronome-tick',
            'content' =>"\F07DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'micro-sd',
            'content' =>"\F07DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microphone',
            'content' =>"\F036C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microphone-minus',
            'content' =>"\F08B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microphone-off',
            'content' =>"\F036D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microphone-outline',
            'content' =>"\F036E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microphone-plus',
            'content' =>"\F08B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microphone-settings',
            'content' =>"\F036F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microphone-variant',
            'content' =>"\F0370"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microphone-variant-off',
            'content' =>"\F0371"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microscope',
            'content' =>"\F0654"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft',
            'content' =>"\F0372"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-access',
            'content' =>"\F138E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-azure',
            'content' =>"\F0805"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-azure-devops',
            'content' =>"\F0FD5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-bing',
            'content' =>"\F00A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-dynamics-365',
            'content' =>"\F0988"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-edge',
            'content' =>"\F01E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-edge-legacy',
            'content' =>"\F1250"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-excel',
            'content' =>"\F138F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-internet-explorer',
            'content' =>"\F0300"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-office',
            'content' =>"\F03C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-onedrive',
            'content' =>"\F03CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-onenote',
            'content' =>"\F0747"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-outlook',
            'content' =>"\F0D22"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-powerpoint',
            'content' =>"\F1390"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-sharepoint',
            'content' =>"\F1391"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-teams',
            'content' =>"\F02BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-visual-studio',
            'content' =>"\F0610"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-visual-studio-code',
            'content' =>"\F0A1E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-windows',
            'content' =>"\F05B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-windows-classic',
            'content' =>"\F0A21"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-word',
            'content' =>"\F1392"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox',
            'content' =>"\F05B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller',
            'content' =>"\F05BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-battery-alert',
            'content' =>"\F074B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-battery-charging',
            'content' =>"\F0A22"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-battery-empty',
            'content' =>"\F074C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-battery-full',
            'content' =>"\F074D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-battery-low',
            'content' =>"\F074E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-battery-medium',
            'content' =>"\F074F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-battery-unknown',
            'content' =>"\F0750"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-menu',
            'content' =>"\F0E6F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-off',
            'content' =>"\F05BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-xbox-controller-view',
            'content' =>"\F0E70"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microsoft-yammer',
            'content' =>"\F0789"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microwave',
            'content' =>"\F0C99"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'microwave-off',
            'content' =>"\F1423"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'middleware',
            'content' =>"\F0F5D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'middleware-outline',
            'content' =>"\F0F5E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'midi',
            'content' =>"\F08F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'midi-port',
            'content' =>"\F08F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mine',
            'content' =>"\F0DDA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minecraft',
            'content' =>"\F0373"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mini-sd',
            'content' =>"\F0A05"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minidisc',
            'content' =>"\F0A06"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus',
            'content' =>"\F0374"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-box',
            'content' =>"\F0375"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-box-multiple',
            'content' =>"\F1141"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-box-multiple-outline',
            'content' =>"\F1142"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-box-outline',
            'content' =>"\F06F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-circle',
            'content' =>"\F0376"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-circle-multiple',
            'content' =>"\F035A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-circle-multiple-outline',
            'content' =>"\F0AD3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-circle-off',
            'content' =>"\F1459"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-circle-off-outline',
            'content' =>"\F145A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-circle-outline',
            'content' =>"\F0377"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-network',
            'content' =>"\F0378"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'minus-network-outline',
            'content' =>"\F0C9A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mirror',
            'content' =>"\F11FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mixed-martial-arts',
            'content' =>"\F0D8F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mixed-reality',
            'content' =>"\F087F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mixer',
            'content' =>"\F07DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'molecule',
            'content' =>"\F0BAC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'molecule-co',
            'content' =>"\F12FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'molecule-co2',
            'content' =>"\F07E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor',
            'content' =>"\F0379"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-cellphone',
            'content' =>"\F0989"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-cellphone-star',
            'content' =>"\F098A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-clean',
            'content' =>"\F1104"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-dashboard',
            'content' =>"\F0A07"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-edit',
            'content' =>"\F12C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-eye',
            'content' =>"\F13B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-lock',
            'content' =>"\F0DDB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-multiple',
            'content' =>"\F037A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-off',
            'content' =>"\F0D90"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-screenshot',
            'content' =>"\F0E51"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-share',
            'content' =>"\F1483"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-speaker',
            'content' =>"\F0F5F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-speaker-off',
            'content' =>"\F0F60"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'monitor-star',
            'content' =>"\F0DDC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'moon-first-quarter',
            'content' =>"\F0F61"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'moon-full',
            'content' =>"\F0F62"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'moon-last-quarter',
            'content' =>"\F0F63"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'moon-new',
            'content' =>"\F0F64"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'moon-waning-crescent',
            'content' =>"\F0F65"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'moon-waning-gibbous',
            'content' =>"\F0F66"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'moon-waxing-crescent',
            'content' =>"\F0F67"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'moon-waxing-gibbous',
            'content' =>"\F0F68"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'moped',
            'content' =>"\F1086"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'more',
            'content' =>"\F037B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mother-heart',
            'content' =>"\F1314"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mother-nurse',
            'content' =>"\F0D21"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'motion-sensor',
            'content' =>"\F0D91"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'motion-sensor-off',
            'content' =>"\F1435"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'motorbike',
            'content' =>"\F037C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mouse',
            'content' =>"\F037D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mouse-bluetooth',
            'content' =>"\F098B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mouse-off',
            'content' =>"\F037E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mouse-variant',
            'content' =>"\F037F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mouse-variant-off',
            'content' =>"\F0380"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'move-resize',
            'content' =>"\F0655"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'move-resize-variant',
            'content' =>"\F0656"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie',
            'content' =>"\F0381"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-edit',
            'content' =>"\F1122"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-edit-outline',
            'content' =>"\F1123"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-filter',
            'content' =>"\F1124"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-filter-outline',
            'content' =>"\F1125"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-open',
            'content' =>"\F0FCE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-open-outline',
            'content' =>"\F0FCF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-outline',
            'content' =>"\F0DDD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-roll',
            'content' =>"\F07DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-search',
            'content' =>"\F11D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'movie-search-outline',
            'content' =>"\F11D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'muffin',
            'content' =>"\F098C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'multiplication',
            'content' =>"\F0382"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'multiplication-box',
            'content' =>"\F0383"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mushroom',
            'content' =>"\F07DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mushroom-off',
            'content' =>"\F13FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mushroom-off-outline',
            'content' =>"\F13FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'mushroom-outline',
            'content' =>"\F07E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music',
            'content' =>"\F075A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-accidental-double-flat',
            'content' =>"\F0F69"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-accidental-double-sharp',
            'content' =>"\F0F6A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-accidental-flat',
            'content' =>"\F0F6B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-accidental-natural',
            'content' =>"\F0F6C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-accidental-sharp',
            'content' =>"\F0F6D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-box',
            'content' =>"\F0384"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-box-multiple',
            'content' =>"\F0333"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-box-multiple-outline',
            'content' =>"\F0F04"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-box-outline',
            'content' =>"\F0385"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-circle',
            'content' =>"\F0386"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-circle-outline',
            'content' =>"\F0AD4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-clef-alto',
            'content' =>"\F0F6E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-clef-bass',
            'content' =>"\F0F6F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-clef-treble',
            'content' =>"\F0F70"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note',
            'content' =>"\F0387"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-bluetooth',
            'content' =>"\F05FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-bluetooth-off',
            'content' =>"\F05FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-eighth',
            'content' =>"\F0388"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-eighth-dotted',
            'content' =>"\F0F71"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-half',
            'content' =>"\F0389"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-half-dotted',
            'content' =>"\F0F72"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-off',
            'content' =>"\F038A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-off-outline',
            'content' =>"\F0F73"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-outline',
            'content' =>"\F0F74"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-plus',
            'content' =>"\F0DDE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-quarter',
            'content' =>"\F038B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-quarter-dotted',
            'content' =>"\F0F75"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-sixteenth',
            'content' =>"\F038C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-sixteenth-dotted',
            'content' =>"\F0F76"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-whole',
            'content' =>"\F038D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-note-whole-dotted',
            'content' =>"\F0F77"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-off',
            'content' =>"\F075B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-rest-eighth',
            'content' =>"\F0F78"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-rest-half',
            'content' =>"\F0F79"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-rest-quarter',
            'content' =>"\F0F7A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-rest-sixteenth',
            'content' =>"\F0F7B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'music-rest-whole',
            'content' =>"\F0F7C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nail',
            'content' =>"\F0DDF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nas',
            'content' =>"\F08F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nativescript',
            'content' =>"\F0880"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nature',
            'content' =>"\F038E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nature-people',
            'content' =>"\F038F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'navigation',
            'content' =>"\F0390"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'near-me',
            'content' =>"\F05CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'necklace',
            'content' =>"\F0F0B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'needle',
            'content' =>"\F0391"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'netflix',
            'content' =>"\F0746"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network',
            'content' =>"\F06F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-off',
            'content' =>"\F0C9B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-off-outline',
            'content' =>"\F0C9C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-outline',
            'content' =>"\F0C9D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-1',
            'content' =>"\F08F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-1-alert',
            'content' =>"\F08F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-2',
            'content' =>"\F08F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-2-alert',
            'content' =>"\F08F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-3',
            'content' =>"\F08F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-3-alert',
            'content' =>"\F08F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-4',
            'content' =>"\F08FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-4-alert',
            'content' =>"\F08FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-off',
            'content' =>"\F08FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-off-outline',
            'content' =>"\F08FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'network-strength-outline',
            'content' =>"\F08FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'new-box',
            'content' =>"\F0394"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'newspaper',
            'content' =>"\F0395"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'newspaper-minus',
            'content' =>"\F0F0C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'newspaper-plus',
            'content' =>"\F0F0D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'newspaper-variant',
            'content' =>"\F1001"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'newspaper-variant-multiple',
            'content' =>"\F1002"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'newspaper-variant-multiple-outline',
            'content' =>"\F1003"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'newspaper-variant-outline',
            'content' =>"\F1004"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nfc',
            'content' =>"\F0396"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nfc-search-variant',
            'content' =>"\F0E53"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nfc-tap',
            'content' =>"\F0397"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nfc-variant',
            'content' =>"\F0398"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nfc-variant-off',
            'content' =>"\F0E54"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ninja',
            'content' =>"\F0774"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nintendo-game-boy',
            'content' =>"\F1393"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nintendo-switch',
            'content' =>"\F07E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nintendo-wii',
            'content' =>"\F05AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nintendo-wiiu',
            'content' =>"\F072D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nix',
            'content' =>"\F1105"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nodejs',
            'content' =>"\F0399"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'noodles',
            'content' =>"\F117E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'not-equal',
            'content' =>"\F098D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'not-equal-variant',
            'content' =>"\F098E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'note',
            'content' =>"\F039A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'note-multiple',
            'content' =>"\F06B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'note-multiple-outline',
            'content' =>"\F06B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'note-outline',
            'content' =>"\F039B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'note-plus',
            'content' =>"\F039C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'note-plus-outline',
            'content' =>"\F039D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'note-text',
            'content' =>"\F039E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'note-text-outline',
            'content' =>"\F11D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'notebook',
            'content' =>"\F082E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'notebook-multiple',
            'content' =>"\F0E55"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'notebook-outline',
            'content' =>"\F0EBF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'notification-clear-all',
            'content' =>"\F039F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'npm',
            'content' =>"\F06F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nuke',
            'content' =>"\F06A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'null',
            'content' =>"\F07E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric',
            'content' =>"\F03A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-0',
            'content' =>"\F0B39"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-0-box',
            'content' =>"\F03A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-0-box-multiple',
            'content' =>"\F0F0E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-0-box-multiple-outline',
            'content' =>"\F03A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-0-box-outline',
            'content' =>"\F03A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-0-circle',
            'content' =>"\F0C9E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-0-circle-outline',
            'content' =>"\F0C9F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-1',
            'content' =>"\F0B3A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-1-box',
            'content' =>"\F03A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-1-box-multiple',
            'content' =>"\F0F0F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-1-box-multiple-outline',
            'content' =>"\F03A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-1-box-outline',
            'content' =>"\F03A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-1-circle',
            'content' =>"\F0CA0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-1-circle-outline',
            'content' =>"\F0CA1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-10',
            'content' =>"\F0FE9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-10-box',
            'content' =>"\F0F7D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-10-box-multiple',
            'content' =>"\F0FEA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-10-box-multiple-outline',
            'content' =>"\F0FEB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-10-box-outline',
            'content' =>"\F0F7E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-10-circle',
            'content' =>"\F0FEC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-10-circle-outline',
            'content' =>"\F0FED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-2',
            'content' =>"\F0B3B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-2-box',
            'content' =>"\F03A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-2-box-multiple',
            'content' =>"\F0F10"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-2-box-multiple-outline',
            'content' =>"\F03A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-2-box-outline',
            'content' =>"\F03A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-2-circle',
            'content' =>"\F0CA2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-2-circle-outline',
            'content' =>"\F0CA3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-3',
            'content' =>"\F0B3C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-3-box',
            'content' =>"\F03AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-3-box-multiple',
            'content' =>"\F0F11"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-3-box-multiple-outline',
            'content' =>"\F03AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-3-box-outline',
            'content' =>"\F03AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-3-circle',
            'content' =>"\F0CA4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-3-circle-outline',
            'content' =>"\F0CA5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-4',
            'content' =>"\F0B3D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-4-box',
            'content' =>"\F03AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-4-box-multiple',
            'content' =>"\F0F12"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-4-box-multiple-outline',
            'content' =>"\F03B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-4-box-outline',
            'content' =>"\F03AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-4-circle',
            'content' =>"\F0CA6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-4-circle-outline',
            'content' =>"\F0CA7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-5',
            'content' =>"\F0B3E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-5-box',
            'content' =>"\F03B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-5-box-multiple',
            'content' =>"\F0F13"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-5-box-multiple-outline',
            'content' =>"\F03AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-5-box-outline',
            'content' =>"\F03B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-5-circle',
            'content' =>"\F0CA8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-5-circle-outline',
            'content' =>"\F0CA9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-6',
            'content' =>"\F0B3F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-6-box',
            'content' =>"\F03B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-6-box-multiple',
            'content' =>"\F0F14"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-6-box-multiple-outline',
            'content' =>"\F03B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-6-box-outline',
            'content' =>"\F03B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-6-circle',
            'content' =>"\F0CAA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-6-circle-outline',
            'content' =>"\F0CAB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-7',
            'content' =>"\F0B40"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-7-box',
            'content' =>"\F03B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-7-box-multiple',
            'content' =>"\F0F15"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-7-box-multiple-outline',
            'content' =>"\F03B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-7-box-outline',
            'content' =>"\F03B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-7-circle',
            'content' =>"\F0CAC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-7-circle-outline',
            'content' =>"\F0CAD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-8',
            'content' =>"\F0B41"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-8-box',
            'content' =>"\F03B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-8-box-multiple',
            'content' =>"\F0F16"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-8-box-multiple-outline',
            'content' =>"\F03BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-8-box-outline',
            'content' =>"\F03BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-8-circle',
            'content' =>"\F0CAE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-8-circle-outline',
            'content' =>"\F0CAF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9',
            'content' =>"\F0B42"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-box',
            'content' =>"\F03BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-box-multiple',
            'content' =>"\F0F17"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-box-multiple-outline',
            'content' =>"\F03BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-box-outline',
            'content' =>"\F03BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-circle',
            'content' =>"\F0CB0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-circle-outline',
            'content' =>"\F0CB1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-plus',
            'content' =>"\F0FEE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-plus-box',
            'content' =>"\F03BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-plus-box-multiple',
            'content' =>"\F0F18"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-plus-box-multiple-outline',
            'content' =>"\F03C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-plus-box-outline',
            'content' =>"\F03C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-plus-circle',
            'content' =>"\F0CB2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-9-plus-circle-outline',
            'content' =>"\F0CB3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'numeric-negative-1',
            'content' =>"\F1052"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nut',
            'content' =>"\F06F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nutrition',
            'content' =>"\F03C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'nuxt',
            'content' =>"\F1106"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'oar',
            'content' =>"\F067C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ocarina',
            'content' =>"\F0DE0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'oci',
            'content' =>"\F12E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ocr',
            'content' =>"\F113A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'octagon',
            'content' =>"\F03C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'octagon-outline',
            'content' =>"\F03C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'octagram',
            'content' =>"\F06F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'octagram-outline',
            'content' =>"\F0775"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'odnoklassniki',
            'content' =>"\F03C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'offer',
            'content' =>"\F121B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'office-building',
            'content' =>"\F0991"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'oil',
            'content' =>"\F03C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'oil-lamp',
            'content' =>"\F0F19"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'oil-level',
            'content' =>"\F1053"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'oil-temperature',
            'content' =>"\F0FF8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'omega',
            'content' =>"\F03C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'one-up',
            'content' =>"\F0BAD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'onepassword',
            'content' =>"\F0881"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'opacity',
            'content' =>"\F05CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'open-in-app',
            'content' =>"\F03CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'open-in-new',
            'content' =>"\F03CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'open-source-initiative',
            'content' =>"\F0BAE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'openid',
            'content' =>"\F03CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'opera',
            'content' =>"\F03CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'orbit',
            'content' =>"\F0018"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'order-alphabetical-ascending',
            'content' =>"\F020D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'order-alphabetical-descending',
            'content' =>"\F0D07"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'order-bool-ascending',
            'content' =>"\F02BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'order-bool-ascending-variant',
            'content' =>"\F098F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'order-bool-descending',
            'content' =>"\F1384"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'order-bool-descending-variant',
            'content' =>"\F0990"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'order-numeric-ascending',
            'content' =>"\F0545"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'order-numeric-descending',
            'content' =>"\F0546"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'origin',
            'content' =>"\F0B43"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ornament',
            'content' =>"\F03CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ornament-variant',
            'content' =>"\F03D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'outdoor-lamp',
            'content' =>"\F1054"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'overscan',
            'content' =>"\F1005"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'owl',
            'content' =>"\F03D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pac-man',
            'content' =>"\F0BAF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'package',
            'content' =>"\F03D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'package-down',
            'content' =>"\F03D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'package-up',
            'content' =>"\F03D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'package-variant',
            'content' =>"\F03D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'package-variant-closed',
            'content' =>"\F03D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-first',
            'content' =>"\F0600"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-last',
            'content' =>"\F0601"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-layout-body',
            'content' =>"\F06FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-layout-footer',
            'content' =>"\F06FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-layout-header',
            'content' =>"\F06FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-layout-header-footer',
            'content' =>"\F0F7F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-layout-sidebar-left',
            'content' =>"\F06FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-layout-sidebar-right',
            'content' =>"\F06FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-next',
            'content' =>"\F0BB0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-next-outline',
            'content' =>"\F0BB1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-previous',
            'content' =>"\F0BB2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'page-previous-outline',
            'content' =>"\F0BB3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail',
            'content' =>"\F1417"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail-minus',
            'content' =>"\F1437"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail-minus-outline',
            'content' =>"\F143C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail-off',
            'content' =>"\F1439"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail-off-outline',
            'content' =>"\F143E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail-outline',
            'content' =>"\F143A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail-plus',
            'content' =>"\F1436"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail-plus-outline',
            'content' =>"\F143B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail-remove',
            'content' =>"\F1438"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pail-remove-outline',
            'content' =>"\F143D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'palette',
            'content' =>"\F03D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'palette-advanced',
            'content' =>"\F03D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'palette-outline',
            'content' =>"\F0E0C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'palette-swatch',
            'content' =>"\F08B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'palette-swatch-outline',
            'content' =>"\F135C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'palm-tree',
            'content' =>"\F1055"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan',
            'content' =>"\F0BB4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-bottom-left',
            'content' =>"\F0BB5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-bottom-right',
            'content' =>"\F0BB6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-down',
            'content' =>"\F0BB7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-horizontal',
            'content' =>"\F0BB8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-left',
            'content' =>"\F0BB9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-right',
            'content' =>"\F0BBA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-top-left',
            'content' =>"\F0BBB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-top-right',
            'content' =>"\F0BBC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-up',
            'content' =>"\F0BBD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pan-vertical',
            'content' =>"\F0BBE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'panda',
            'content' =>"\F03DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pandora',
            'content' =>"\F03DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'panorama',
            'content' =>"\F03DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'panorama-fisheye',
            'content' =>"\F03DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'panorama-horizontal',
            'content' =>"\F03DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'panorama-vertical',
            'content' =>"\F03DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'panorama-wide-angle',
            'content' =>"\F03E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'paper-cut-vertical',
            'content' =>"\F03E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'paper-roll',
            'content' =>"\F1157"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'paper-roll-outline',
            'content' =>"\F1158"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'paperclip',
            'content' =>"\F03E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'parachute',
            'content' =>"\F0CB4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'parachute-outline',
            'content' =>"\F0CB5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'parking',
            'content' =>"\F03E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'party-popper',
            'content' =>"\F1056"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'passport',
            'content' =>"\F07E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'passport-biometric',
            'content' =>"\F0DE1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pasta',
            'content' =>"\F1160"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'patio-heater',
            'content' =>"\F0F80"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'patreon',
            'content' =>"\F0882"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pause',
            'content' =>"\F03E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pause-circle',
            'content' =>"\F03E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pause-circle-outline',
            'content' =>"\F03E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pause-octagon',
            'content' =>"\F03E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pause-octagon-outline',
            'content' =>"\F03E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'paw',
            'content' =>"\F03E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'paw-off',
            'content' =>"\F0657"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pdf-box',
            'content' =>"\F0E56"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'peace',
            'content' =>"\F0884"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'peanut',
            'content' =>"\F0FFC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'peanut-off',
            'content' =>"\F0FFD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'peanut-off-outline',
            'content' =>"\F0FFF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'peanut-outline',
            'content' =>"\F0FFE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pen',
            'content' =>"\F03EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pen-lock',
            'content' =>"\F0DE2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pen-minus',
            'content' =>"\F0DE3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pen-off',
            'content' =>"\F0DE4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pen-plus',
            'content' =>"\F0DE5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pen-remove',
            'content' =>"\F0DE6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil',
            'content' =>"\F03EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-box',
            'content' =>"\F03EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-box-multiple',
            'content' =>"\F1144"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-box-multiple-outline',
            'content' =>"\F1145"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-box-outline',
            'content' =>"\F03ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-circle',
            'content' =>"\F06FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-circle-outline',
            'content' =>"\F0776"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-lock',
            'content' =>"\F03EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-lock-outline',
            'content' =>"\F0DE7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-minus',
            'content' =>"\F0DE8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-minus-outline',
            'content' =>"\F0DE9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-off',
            'content' =>"\F03EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-off-outline',
            'content' =>"\F0DEA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-outline',
            'content' =>"\F0CB6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-plus',
            'content' =>"\F0DEB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-plus-outline',
            'content' =>"\F0DEC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-remove',
            'content' =>"\F0DED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-remove-outline',
            'content' =>"\F0DEE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pencil-ruler',
            'content' =>"\F1353"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'penguin',
            'content' =>"\F0EC0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pentagon',
            'content' =>"\F0701"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pentagon-outline',
            'content' =>"\F0700"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'percent',
            'content' =>"\F03F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'percent-outline',
            'content' =>"\F1278"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'periodic-table',
            'content' =>"\F08B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'perspective-less',
            'content' =>"\F0D23"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'perspective-more',
            'content' =>"\F0D24"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pharmacy',
            'content' =>"\F03F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone',
            'content' =>"\F03F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-alert',
            'content' =>"\F0F1A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-alert-outline',
            'content' =>"\F118E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-bluetooth',
            'content' =>"\F03F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-bluetooth-outline',
            'content' =>"\F118F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-cancel',
            'content' =>"\F10BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-cancel-outline',
            'content' =>"\F1190"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-check',
            'content' =>"\F11A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-check-outline',
            'content' =>"\F11AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-classic',
            'content' =>"\F0602"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-classic-off',
            'content' =>"\F1279"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-forward',
            'content' =>"\F03F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-forward-outline',
            'content' =>"\F1191"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-hangup',
            'content' =>"\F03F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-hangup-outline',
            'content' =>"\F1192"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-in-talk',
            'content' =>"\F03F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-in-talk-outline',
            'content' =>"\F1182"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-incoming',
            'content' =>"\F03F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-incoming-outline',
            'content' =>"\F1193"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-lock',
            'content' =>"\F03F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-lock-outline',
            'content' =>"\F1194"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-log',
            'content' =>"\F03F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-log-outline',
            'content' =>"\F1195"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-message',
            'content' =>"\F1196"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-message-outline',
            'content' =>"\F1197"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-minus',
            'content' =>"\F0658"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-minus-outline',
            'content' =>"\F1198"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-missed',
            'content' =>"\F03FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-missed-outline',
            'content' =>"\F11A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-off',
            'content' =>"\F0DEF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-off-outline',
            'content' =>"\F11A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-outgoing',
            'content' =>"\F03FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-outgoing-outline',
            'content' =>"\F1199"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-outline',
            'content' =>"\F0DF0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-paused',
            'content' =>"\F03FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-paused-outline',
            'content' =>"\F119A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-plus',
            'content' =>"\F0659"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-plus-outline',
            'content' =>"\F119B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-return',
            'content' =>"\F082F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-return-outline',
            'content' =>"\F119C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-ring',
            'content' =>"\F11AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-ring-outline',
            'content' =>"\F11AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-rotate-landscape',
            'content' =>"\F0885"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-rotate-portrait',
            'content' =>"\F0886"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-settings',
            'content' =>"\F03FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-settings-outline',
            'content' =>"\F119D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'phone-voip',
            'content' =>"\F03FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pi',
            'content' =>"\F03FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pi-box',
            'content' =>"\F0400"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pi-hole',
            'content' =>"\F0DF1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'piano',
            'content' =>"\F067D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pickaxe',
            'content' =>"\F08B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'picture-in-picture-bottom-right',
            'content' =>"\F0E57"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'picture-in-picture-bottom-right-outline',
            'content' =>"\F0E58"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'picture-in-picture-top-right',
            'content' =>"\F0E59"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'picture-in-picture-top-right-outline',
            'content' =>"\F0E5A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pier',
            'content' =>"\F0887"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pier-crane',
            'content' =>"\F0888"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pig',
            'content' =>"\F0401"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pig-variant',
            'content' =>"\F1006"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'piggy-bank',
            'content' =>"\F1007"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pill',
            'content' =>"\F0402"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pillar',
            'content' =>"\F0702"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pin',
            'content' =>"\F0403"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pin-off',
            'content' =>"\F0404"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pin-off-outline',
            'content' =>"\F0930"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pin-outline',
            'content' =>"\F0931"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pine-tree',
            'content' =>"\F0405"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pine-tree-box',
            'content' =>"\F0406"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pine-tree-fire',
            'content' =>"\F141A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pinterest',
            'content' =>"\F0407"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pinwheel',
            'content' =>"\F0AD5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pinwheel-outline',
            'content' =>"\F0AD6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pipe',
            'content' =>"\F07E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pipe-disconnected',
            'content' =>"\F07E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pipe-leak',
            'content' =>"\F0889"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pipe-wrench',
            'content' =>"\F1354"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pirate',
            'content' =>"\F0A08"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pistol',
            'content' =>"\F0703"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'piston',
            'content' =>"\F088A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pizza',
            'content' =>"\F0409"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play',
            'content' =>"\F040A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-box',
            'content' =>"\F127A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-box-multiple',
            'content' =>"\F0D19"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-box-multiple-outline',
            'content' =>"\F13E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-box-outline',
            'content' =>"\F040B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-circle',
            'content' =>"\F040C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-circle-outline',
            'content' =>"\F040D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-network',
            'content' =>"\F088B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-network-outline',
            'content' =>"\F0CB7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-outline',
            'content' =>"\F0F1B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-pause',
            'content' =>"\F040E"
        ]);

        factory(App\MaterialIcon::class)->create([
            'name' => 'play-protected-content',
            'content' =>"\F040F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'play-speed',
            'content' =>"\F08FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'playlist-check',
            'content' =>"\F05C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'playlist-edit',
            'content' =>"\F0900"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'playlist-minus',
            'content' =>"\F0410"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'playlist-music',
            'content' =>"\F0CB8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'playlist-music-outline',
            'content' =>"\F0CB9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'playlist-play',
            'content' =>"\F0411"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'playlist-plus',
            'content' =>"\F0412"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'playlist-remove',
            'content' =>"\F0413"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'playlist-star',
            'content' =>"\F0DF2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plex',
            'content' =>"\F06BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus',
            'content' =>"\F0415"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-box',
            'content' =>"\F0416"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-box-multiple',
            'content' =>"\F0334"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-box-multiple-outline',
            'content' =>"\F1143"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-box-outline',
            'content' =>"\F0704"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-circle',
            'content' =>"\F0417"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-circle-multiple',
            'content' =>"\F034C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-circle-multiple-outline',
            'content' =>"\F0418"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-circle-outline',
            'content' =>"\F0419"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-minus',
            'content' =>"\F0992"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-minus-box',
            'content' =>"\F0993"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-minus-variant',
            'content' =>"\F14C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-network',
            'content' =>"\F041A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-network-outline',
            'content' =>"\F0CBA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-one',
            'content' =>"\F041B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-outline',
            'content' =>"\F0705"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'plus-thick',
            'content' =>"\F11EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'podcast',
            'content' =>"\F0994"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'podium',
            'content' =>"\F0D25"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'podium-bronze',
            'content' =>"\F0D26"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'podium-gold',
            'content' =>"\F0D27"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'podium-silver',
            'content' =>"\F0D28"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'point-of-sale',
            'content' =>"\F0D92"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pokeball',
            'content' =>"\F041D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pokemon-go',
            'content' =>"\F0A09"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'poker-chip',
            'content' =>"\F0830"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'polaroid',
            'content' =>"\F041E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'police-badge',
            'content' =>"\F1167"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'police-badge-outline',
            'content' =>"\F1168"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'poll',
            'content' =>"\F041F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'poll-box',
            'content' =>"\F0420"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'poll-box-outline',
            'content' =>"\F127B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'polo',
            'content' =>"\F14C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'polymer',
            'content' =>"\F0421"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pool',
            'content' =>"\F0606"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'popcorn',
            'content' =>"\F0422"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'post',
            'content' =>"\F1008"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'post-outline',
            'content' =>"\F1009"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'postage-stamp',
            'content' =>"\F0CBB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pot',
            'content' =>"\F02E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pot-mix',
            'content' =>"\F065B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pot-mix-outline',
            'content' =>"\F0677"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pot-outline',
            'content' =>"\F02FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pot-steam',
            'content' =>"\F065A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pot-steam-outline',
            'content' =>"\F0326"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pound',
            'content' =>"\F0423"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pound-box',
            'content' =>"\F0424"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pound-box-outline',
            'content' =>"\F117F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power',
            'content' =>"\F0425"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-cycle',
            'content' =>"\F0901"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-off',
            'content' =>"\F0902"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-on',
            'content' =>"\F0903"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-plug',
            'content' =>"\F06A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-plug-off',
            'content' =>"\F06A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-plug-off-outline',
            'content' =>"\F1424"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-plug-outline',
            'content' =>"\F1425"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-settings',
            'content' =>"\F0426"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-sleep',
            'content' =>"\F0904"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-socket',
            'content' =>"\F0427"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-socket-au',
            'content' =>"\F0905"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-socket-de',
            'content' =>"\F1107"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-socket-eu',
            'content' =>"\F07E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-socket-fr',
            'content' =>"\F1108"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-socket-jp',
            'content' =>"\F1109"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-socket-uk',
            'content' =>"\F07E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-socket-us',
            'content' =>"\F07E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'power-standby',
            'content' =>"\F0906"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'powershell',
            'content' =>"\F0A0A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'prescription',
            'content' =>"\F0706"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'presentation',
            'content' =>"\F0428"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'presentation-play',
            'content' =>"\F0429"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer',
            'content' =>"\F042A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-3d',
            'content' =>"\F042B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-3d-nozzle',
            'content' =>"\F0E5B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-3d-nozzle-alert',
            'content' =>"\F11C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-3d-nozzle-alert-outline',
            'content' =>"\F11C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-3d-nozzle-outline',
            'content' =>"\F0E5C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-alert',
            'content' =>"\F042C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-check',
            'content' =>"\F1146"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-eye',
            'content' =>"\F1458"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-off',
            'content' =>"\F0E5D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-pos',
            'content' =>"\F1057"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-search',
            'content' =>"\F1457"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-settings',
            'content' =>"\F0707"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'printer-wireless',
            'content' =>"\F0A0B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'priority-high',
            'content' =>"\F0603"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'priority-low',
            'content' =>"\F0604"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'professional-hexagon',
            'content' =>"\F042D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'progress-alert',
            'content' =>"\F0CBC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'progress-check',
            'content' =>"\F0995"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'progress-clock',
            'content' =>"\F0996"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'progress-close',
            'content' =>"\F110A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'progress-download',
            'content' =>"\F0997"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'progress-upload',
            'content' =>"\F0998"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'progress-wrench',
            'content' =>"\F0CBD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'projector',
            'content' =>"\F042E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'projector-screen',
            'content' =>"\F042F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'propane-tank',
            'content' =>"\F1357"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'propane-tank-outline',
            'content' =>"\F1358"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'protocol',
            'content' =>"\F0FD8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'publish',
            'content' =>"\F06A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pulse',
            'content' =>"\F0430"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pump',
            'content' =>"\F1402"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'pumpkin',
            'content' =>"\F0BBF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'purse',
            'content' =>"\F0F1C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'purse-outline',
            'content' =>"\F0F1D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle',
            'content' =>"\F0431"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-check',
            'content' =>"\F1426"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-check-outline',
            'content' =>"\F1427"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-edit',
            'content' =>"\F14D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-edit-outline',
            'content' =>"\F14D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-heart',
            'content' =>"\F14D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-heart-outline',
            'content' =>"\F14DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-minus',
            'content' =>"\F14D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-minus-outline',
            'content' =>"\F14D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-outline',
            'content' =>"\F0A66"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-plus',
            'content' =>"\F14D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-plus-outline',
            'content' =>"\F14D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-remove',
            'content' =>"\F14D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-remove-outline',
            'content' =>"\F14D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-star',
            'content' =>"\F14D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'puzzle-star-outline',
            'content' =>"\F14DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'qi',
            'content' =>"\F0999"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'qqchat',
            'content' =>"\F0605"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'qrcode',
            'content' =>"\F0432"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'qrcode-edit',
            'content' =>"\F08B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'qrcode-minus',
            'content' =>"\F118C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'qrcode-plus',
            'content' =>"\F118B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'qrcode-remove',
            'content' =>"\F118D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'qrcode-scan',
            'content' =>"\F0433"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'quadcopter',
            'content' =>"\F0434"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'quality-high',
            'content' =>"\F0435"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'quality-low',
            'content' =>"\F0A0C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'quality-medium',
            'content' =>"\F0A0D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'quora',
            'content' =>"\F0D29"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rabbit',
            'content' =>"\F0907"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'racing-helmet',
            'content' =>"\F0D93"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'racquetball',
            'content' =>"\F0D94"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radar',
            'content' =>"\F0437"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radiator',
            'content' =>"\F0438"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radiator-disabled',
            'content' =>"\F0AD7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radiator-off',
            'content' =>"\F0AD8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radio',
            'content' =>"\F0439"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radio-am',
            'content' =>"\F0CBE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radio-fm',
            'content' =>"\F0CBF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radio-handheld',
            'content' =>"\F043A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radio-off',
            'content' =>"\F121C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radio-tower',
            'content' =>"\F043B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radioactive',
            'content' =>"\F043C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radioactive-off',
            'content' =>"\F0EC1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radiobox-blank',
            'content' =>"\F043D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radiobox-marked',
            'content' =>"\F043E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radiology-box',
            'content' =>"\F14C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radiology-box-outline',
            'content' =>"\F14C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radius',
            'content' =>"\F0CC0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'radius-outline',
            'content' =>"\F0CC1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'railroad-light',
            'content' =>"\F0F1E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'raspberry-pi',
            'content' =>"\F043F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ray-end',
            'content' =>"\F0440"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ray-end-arrow',
            'content' =>"\F0441"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ray-start',
            'content' =>"\F0442"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ray-start-arrow',
            'content' =>"\F0443"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ray-start-end',
            'content' =>"\F0444"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ray-vertex',
            'content' =>"\F0445"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'react',
            'content' =>"\F0708"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'read',
            'content' =>"\F0447"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'receipt',
            'content' =>"\F0449"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'record',
            'content' =>"\F044A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'record-circle',
            'content' =>"\F0EC2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'record-circle-outline',
            'content' =>"\F0EC3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'record-player',
            'content' =>"\F099A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'record-rec',
            'content' =>"\F044B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rectangle',
            'content' =>"\F0E5E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rectangle-outline',
            'content' =>"\F0E5F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'recycle',
            'content' =>"\F044C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'recycle-variant',
            'content' =>"\F139D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reddit',
            'content' =>"\F044D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'redhat',
            'content' =>"\F111B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'redo',
            'content' =>"\F044E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'redo-variant',
            'content' =>"\F044F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reflect-horizontal',
            'content' =>"\F0A0E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reflect-vertical',
            'content' =>"\F0A0F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'refresh',
            'content' =>"\F0450"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'refresh-circle',
            'content' =>"\F1377"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'regex',
            'content' =>"\F0451"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'registered-trademark',
            'content' =>"\F0A67"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-many-to-many',
            'content' =>"\F1496"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-many-to-one',
            'content' =>"\F1497"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-many-to-one-or-many',
            'content' =>"\F1498"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-many-to-only-one',
            'content' =>"\F1499"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-many-to-zero-or-many',
            'content' =>"\F149A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-many-to-zero-or-one',
            'content' =>"\F149B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-or-many-to-many',
            'content' =>"\F149C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-or-many-to-one',
            'content' =>"\F149D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-or-many-to-one-or-many',
            'content' =>"\F149E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-or-many-to-only-one',
            'content' =>"\F149F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-or-many-to-zero-or-many',
            'content' =>"\F14A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-or-many-to-zero-or-one',
            'content' =>"\F14A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-to-many',
            'content' =>"\F14A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-to-one',
            'content' =>"\F14A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-to-one-or-many',
            'content' =>"\F14A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-to-only-one',
            'content' =>"\F14A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-to-zero-or-many',
            'content' =>"\F14A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-one-to-zero-or-one',
            'content' =>"\F14A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-only-one-to-many',
            'content' =>"\F14A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-only-one-to-one',
            'content' =>"\F14A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-only-one-to-one-or-many',
            'content' =>"\F14AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-only-one-to-only-one',
            'content' =>"\F14AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-only-one-to-zero-or-many',
            'content' =>"\F14AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-only-one-to-zero-or-one',
            'content' =>"\F14AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-many-to-many',
            'content' =>"\F14AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-many-to-one',
            'content' =>"\F14AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-many-to-one-or-many',
            'content' =>"\F14B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-many-to-only-one',
            'content' =>"\F14B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-many-to-zero-or-many',
            'content' =>"\F14B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-many-to-zero-or-one',
            'content' =>"\F14B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-one-to-many',
            'content' =>"\F14B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-one-to-one',
            'content' =>"\F14B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-one-to-one-or-many',
            'content' =>"\F14B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-one-to-only-one',
            'content' =>"\F14B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-one-to-zero-or-many',
            'content' =>"\F14B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relation-zero-or-one-to-zero-or-one',
            'content' =>"\F14B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'relative-scale',
            'content' =>"\F0452"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reload',
            'content' =>"\F0453"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reload-alert',
            'content' =>"\F110B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reminder',
            'content' =>"\F088C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'remote',
            'content' =>"\F0454"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'remote-desktop',
            'content' =>"\F08B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'remote-off',
            'content' =>"\F0EC4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'remote-tv',
            'content' =>"\F0EC5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'remote-tv-off',
            'content' =>"\F0EC6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rename-box',
            'content' =>"\F0455"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reorder-horizontal',
            'content' =>"\F0688"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reorder-vertical',
            'content' =>"\F0689"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'repeat',
            'content' =>"\F0456"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'repeat-off',
            'content' =>"\F0457"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'repeat-once',
            'content' =>"\F0458"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'replay',
            'content' =>"\F0459"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reply',
            'content' =>"\F045A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reply-all',
            'content' =>"\F045B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reply-all-outline',
            'content' =>"\F0F1F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reply-circle',
            'content' =>"\F11AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reply-outline',
            'content' =>"\F0F20"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'reproduction',
            'content' =>"\F045C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'resistor',
            'content' =>"\F0B44"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'resistor-nodes',
            'content' =>"\F0B45"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'resize',
            'content' =>"\F0A68"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'resize-bottom-right',
            'content' =>"\F045D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'responsive',
            'content' =>"\F045E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'restart',
            'content' =>"\F0709"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'restart-alert',
            'content' =>"\F110C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'restart-off',
            'content' =>"\F0D95"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'restore',
            'content' =>"\F099B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'restore-alert',
            'content' =>"\F110D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rewind',
            'content' =>"\F045F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rewind-10',
            'content' =>"\F0D2A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rewind-30',
            'content' =>"\F0D96"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rewind-5',
            'content' =>"\F11F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rewind-outline',
            'content' =>"\F070A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rhombus',
            'content' =>"\F070B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rhombus-medium',
            'content' =>"\F0A10"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rhombus-medium-outline',
            'content' =>"\F14DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rhombus-outline',
            'content' =>"\F070C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rhombus-split',
            'content' =>"\F0A11"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rhombus-split-outline',
            'content' =>"\F14DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ribbon',
            'content' =>"\F0460"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rice',
            'content' =>"\F07EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ring',
            'content' =>"\F07EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rivet',
            'content' =>"\F0E60"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'road',
            'content' =>"\F0461"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'road-variant',
            'content' =>"\F0462"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'robber',
            'content' =>"\F1058"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'robot',
            'content' =>"\F06A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'robot-industrial',
            'content' =>"\F0B46"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'robot-mower',
            'content' =>"\F11F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'robot-mower-outline',
            'content' =>"\F11F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'robot-vacuum',
            'content' =>"\F070D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'robot-vacuum-variant',
            'content' =>"\F0908"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rocket',
            'content' =>"\F0463"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rocket-launch',
            'content' =>"\F14DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rocket-launch-outline',
            'content' =>"\F14DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rocket-outline',
            'content' =>"\F13AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rodent',
            'content' =>"\F1327"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roller-skate',
            'content' =>"\F0D2B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roller-skate-off',
            'content' =>"\F0145"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rollerblade',
            'content' =>"\F0D2C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rollerblade-off',
            'content' =>"\F002E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rollupjs',
            'content' =>"\F0BC0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-1',
            'content' =>"\F1088"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-10',
            'content' =>"\F1091"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-2',
            'content' =>"\F1089"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-3',
            'content' =>"\F108A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-4',
            'content' =>"\F108B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-5',
            'content' =>"\F108C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-6',
            'content' =>"\F108D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-7',
            'content' =>"\F108E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-8',
            'content' =>"\F108F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'roman-numeral-9',
            'content' =>"\F1090"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'room-service',
            'content' =>"\F088D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'room-service-outline',
            'content' =>"\F0D97"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rotate-3d',
            'content' =>"\F0EC7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rotate-3d-variant',
            'content' =>"\F0464"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rotate-left',
            'content' =>"\F0465"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rotate-left-variant',
            'content' =>"\F0466"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rotate-orbit',
            'content' =>"\F0D98"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rotate-right',
            'content' =>"\F0467"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rotate-right-variant',
            'content' =>"\F0468"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rounded-corner',
            'content' =>"\F0607"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'router',
            'content' =>"\F11E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'router-network',
            'content' =>"\F1087"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'router-wireless',
            'content' =>"\F0469"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'router-wireless-settings',
            'content' =>"\F0A69"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'routes',
            'content' =>"\F046A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'routes-clock',
            'content' =>"\F1059"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rowing',
            'content' =>"\F0608"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rss',
            'content' =>"\F046B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rss-box',
            'content' =>"\F046C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rss-off',
            'content' =>"\F0F21"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rug',
            'content' =>"\F1475"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rugby',
            'content' =>"\F0D99"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ruler',
            'content' =>"\F046D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ruler-square',
            'content' =>"\F0CC2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ruler-square-compass',
            'content' =>"\F0EBE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'run',
            'content' =>"\F070E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'run-fast',
            'content' =>"\F046E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'rv-truck',
            'content' =>"\F11D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sack',
            'content' =>"\F0D2E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sack-percent',
            'content' =>"\F0D2F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'safe',
            'content' =>"\F0A6A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'safe-square',
            'content' =>"\F127C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'safe-square-outline',
            'content' =>"\F127D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'safety-goggles',
            'content' =>"\F0D30"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sail-boat',
            'content' =>"\F0EC8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sale',
            'content' =>"\F046F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'salesforce',
            'content' =>"\F088E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sass',
            'content' =>"\F07EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'satellite',
            'content' =>"\F0470"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'satellite-uplink',
            'content' =>"\F0909"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'satellite-variant',
            'content' =>"\F0471"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sausage',
            'content' =>"\F08BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'saw-blade',
            'content' =>"\F0E61"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sawtooth-wave',
            'content' =>"\F147A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'saxophone',
            'content' =>"\F0609"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scale',
            'content' =>"\F0472"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scale-balance',
            'content' =>"\F05D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scale-bathroom',
            'content' =>"\F0473"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scale-off',
            'content' =>"\F105A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scan-helper',
            'content' =>"\F13D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scanner',
            'content' =>"\F06AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scanner-off',
            'content' =>"\F090A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scatter-plot',
            'content' =>"\F0EC9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scatter-plot-outline',
            'content' =>"\F0ECA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'school',
            'content' =>"\F0474"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'school-outline',
            'content' =>"\F1180"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scissors-cutting',
            'content' =>"\F0A6B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scooter',
            'content' =>"\F11E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scoreboard',
            'content' =>"\F127E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'scoreboard-outline',
            'content' =>"\F127F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'screen-rotation',
            'content' =>"\F0475"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'screen-rotation-lock',
            'content' =>"\F0478"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'screw-flat-top',
            'content' =>"\F0DF3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'screw-lag',
            'content' =>"\F0DF4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'screw-machine-flat-top',
            'content' =>"\F0DF5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'screw-machine-round-top',
            'content' =>"\F0DF6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'screw-round-top',
            'content' =>"\F0DF7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'screwdriver',
            'content' =>"\F0476"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'script',
            'content' =>"\F0BC1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'script-outline',
            'content' =>"\F0477"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'script-text',
            'content' =>"\F0BC2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'script-text-outline',
            'content' =>"\F0BC3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sd',
            'content' =>"\F0479"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seal',
            'content' =>"\F047A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seal-variant',
            'content' =>"\F0FD9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'search-web',
            'content' =>"\F070F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat',
            'content' =>"\F0CC3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-flat',
            'content' =>"\F047B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-flat-angled',
            'content' =>"\F047C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-individual-suite',
            'content' =>"\F047D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-legroom-extra',
            'content' =>"\F047E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-legroom-normal',
            'content' =>"\F047F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-legroom-reduced',
            'content' =>"\F0480"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-outline',
            'content' =>"\F0CC4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-passenger',
            'content' =>"\F1249"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-recline-extra',
            'content' =>"\F0481"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seat-recline-normal',
            'content' =>"\F0482"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seatbelt',
            'content' =>"\F0CC5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'security',
            'content' =>"\F0483"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'security-network',
            'content' =>"\F0484"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seed',
            'content' =>"\F0E62"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seed-off',
            'content' =>"\F13FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seed-off-outline',
            'content' =>"\F13FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'seed-outline',
            'content' =>"\F0E63"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'segment',
            'content' =>"\F0ECB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select',
            'content' =>"\F0485"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-all',
            'content' =>"\F0486"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-color',
            'content' =>"\F0D31"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-compare',
            'content' =>"\F0AD9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-drag',
            'content' =>"\F0A6C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-group',
            'content' =>"\F0F82"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-inverse',
            'content' =>"\F0487"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-marker',
            'content' =>"\F1280"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-multiple',
            'content' =>"\F1281"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-multiple-marker',
            'content' =>"\F1282"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-off',
            'content' =>"\F0488"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-place',
            'content' =>"\F0FDA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'select-search',
            'content' =>"\F1204"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'selection',
            'content' =>"\F0489"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'selection-drag',
            'content' =>"\F0A6D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'selection-ellipse',
            'content' =>"\F0D32"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'selection-ellipse-arrow-inside',
            'content' =>"\F0F22"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'selection-marker',
            'content' =>"\F1283"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'selection-multiple',
            'content' =>"\F1285"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'selection-multiple-marker',
            'content' =>"\F1284"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'selection-off',
            'content' =>"\F0777"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'selection-search',
            'content' =>"\F1205"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'semantic-web',
            'content' =>"\F1316"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send',
            'content' =>"\F048A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send-check',
            'content' =>"\F1161"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send-check-outline',
            'content' =>"\F1162"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send-circle',
            'content' =>"\F0DF8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send-circle-outline',
            'content' =>"\F0DF9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send-clock',
            'content' =>"\F1163"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send-clock-outline',
            'content' =>"\F1164"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send-lock',
            'content' =>"\F07ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send-lock-outline',
            'content' =>"\F1166"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'send-outline',
            'content' =>"\F1165"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'serial-port',
            'content' =>"\F065C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'server',
            'content' =>"\F048B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'server-minus',
            'content' =>"\F048C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'server-network',
            'content' =>"\F048D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'server-network-off',
            'content' =>"\F048E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'server-off',
            'content' =>"\F048F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'server-plus',
            'content' =>"\F0490"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'server-remove',
            'content' =>"\F0491"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'server-security',
            'content' =>"\F0492"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-all',
            'content' =>"\F0778"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-center',
            'content' =>"\F0779"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-center-right',
            'content' =>"\F077A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-left',
            'content' =>"\F077B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-left-center',
            'content' =>"\F077C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-left-right',
            'content' =>"\F077D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-merge',
            'content' =>"\F14E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-none',
            'content' =>"\F077E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-right',
            'content' =>"\F077F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-split',
            'content' =>"\F14E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-square',
            'content' =>"\F145D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'set-top-box',
            'content' =>"\F099F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'settings-helper',
            'content' =>"\F0A6E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shaker',
            'content' =>"\F110E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shaker-outline',
            'content' =>"\F110F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shape',
            'content' =>"\F0831"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shape-circle-plus',
            'content' =>"\F065D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shape-outline',
            'content' =>"\F0832"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shape-oval-plus',
            'content' =>"\F11FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shape-plus',
            'content' =>"\F0495"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shape-polygon-plus',
            'content' =>"\F065E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shape-rectangle-plus',
            'content' =>"\F065F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shape-square-plus',
            'content' =>"\F0660"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'share',
            'content' =>"\F0496"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'share-all',
            'content' =>"\F11F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'share-all-outline',
            'content' =>"\F11F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'share-circle',
            'content' =>"\F11AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'share-off',
            'content' =>"\F0F23"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'share-off-outline',
            'content' =>"\F0F24"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'share-outline',
            'content' =>"\F0932"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'share-variant',
            'content' =>"\F0497"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sheep',
            'content' =>"\F0CC6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield',
            'content' =>"\F0498"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-account',
            'content' =>"\F088F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-account-outline',
            'content' =>"\F0A12"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-airplane',
            'content' =>"\F06BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-airplane-outline',
            'content' =>"\F0CC7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-alert',
            'content' =>"\F0ECC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-alert-outline',
            'content' =>"\F0ECD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-bug',
            'content' =>"\F13DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-bug-outline',
            'content' =>"\F13DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-car',
            'content' =>"\F0F83"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-check',
            'content' =>"\F0565"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-check-outline',
            'content' =>"\F0CC8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-cross',
            'content' =>"\F0CC9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-cross-outline',
            'content' =>"\F0CCA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-edit',
            'content' =>"\F11A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-edit-outline',
            'content' =>"\F11A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-half',
            'content' =>"\F1360"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-half-full',
            'content' =>"\F0780"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-home',
            'content' =>"\F068A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-home-outline',
            'content' =>"\F0CCB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-key',
            'content' =>"\F0BC4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-key-outline',
            'content' =>"\F0BC5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-link-variant',
            'content' =>"\F0D33"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-link-variant-outline',
            'content' =>"\F0D34"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-lock',
            'content' =>"\F099D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-lock-outline',
            'content' =>"\F0CCC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-off',
            'content' =>"\F099E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-off-outline',
            'content' =>"\F099C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-outline',
            'content' =>"\F0499"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-plus',
            'content' =>"\F0ADA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-plus-outline',
            'content' =>"\F0ADB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-refresh',
            'content' =>"\F00AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-refresh-outline',
            'content' =>"\F01E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-remove',
            'content' =>"\F0ADC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-remove-outline',
            'content' =>"\F0ADD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-search',
            'content' =>"\F0D9A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-star',
            'content' =>"\F113B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-star-outline',
            'content' =>"\F113C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-sun',
            'content' =>"\F105D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-sun-outline',
            'content' =>"\F105E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-sync',
            'content' =>"\F11A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shield-sync-outline',
            'content' =>"\F11A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ship-wheel',
            'content' =>"\F0833"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shoe-formal',
            'content' =>"\F0B47"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shoe-heel',
            'content' =>"\F0B48"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shoe-print',
            'content' =>"\F0DFA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shopping',
            'content' =>"\F049A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shopping-music',
            'content' =>"\F049B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shopping-outline',
            'content' =>"\F11D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shopping-search',
            'content' =>"\F0F84"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shovel',
            'content' =>"\F0710"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shovel-off',
            'content' =>"\F0711"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shower',
            'content' =>"\F09A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shower-head',
            'content' =>"\F09A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shredder',
            'content' =>"\F049C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shuffle',
            'content' =>"\F049D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shuffle-disabled',
            'content' =>"\F049E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shuffle-variant',
            'content' =>"\F049F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'shuriken',
            'content' =>"\F137F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sigma',
            'content' =>"\F04A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sigma-lower',
            'content' =>"\F062B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sign-caution',
            'content' =>"\F04A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sign-direction',
            'content' =>"\F0781"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sign-direction-minus',
            'content' =>"\F1000"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sign-direction-plus',
            'content' =>"\F0FDC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sign-direction-remove',
            'content' =>"\F0FDD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sign-real-estate',
            'content' =>"\F1118"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sign-text',
            'content' =>"\F0782"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal',
            'content' =>"\F04A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-2g',
            'content' =>"\F0712"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-3g',
            'content' =>"\F0713"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-4g',
            'content' =>"\F0714"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-5g',
            'content' =>"\F0A6F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-cellular-1',
            'content' =>"\F08BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-cellular-2',
            'content' =>"\F08BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-cellular-3',
            'content' =>"\F08BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-cellular-outline',
            'content' =>"\F08BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-distance-variant',
            'content' =>"\F0E64"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-hspa',
            'content' =>"\F0715"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-hspa-plus',
            'content' =>"\F0716"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-off',
            'content' =>"\F0783"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signal-variant',
            'content' =>"\F060A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signature',
            'content' =>"\F0DFB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signature-freehand',
            'content' =>"\F0DFC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signature-image',
            'content' =>"\F0DFD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'signature-text',
            'content' =>"\F0DFE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'silo',
            'content' =>"\F0B49"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'silverware',
            'content' =>"\F04A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'silverware-clean',
            'content' =>"\F0FDE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'silverware-fork',
            'content' =>"\F04A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'silverware-fork-knife',
            'content' =>"\F0A70"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'silverware-spoon',
            'content' =>"\F04A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'silverware-variant',
            'content' =>"\F04A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sim',
            'content' =>"\F04A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sim-alert',
            'content' =>"\F04A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sim-off',
            'content' =>"\F04A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'simple-icons',
            'content' =>"\F131D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sina-weibo',
            'content' =>"\F0ADF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sine-wave',
            'content' =>"\F095B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sitemap',
            'content' =>"\F04AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'size-l',
            'content' =>"\F13A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'size-m',
            'content' =>"\F13A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'size-s',
            'content' =>"\F13A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'size-xl',
            'content' =>"\F13A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'size-xs',
            'content' =>"\F13A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'size-xxl',
            'content' =>"\F13A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'size-xxs',
            'content' =>"\F13A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'size-xxxl',
            'content' =>"\F13A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skate',
            'content' =>"\F0D35"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skateboard',
            'content' =>"\F14C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skew-less',
            'content' =>"\F0D36"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skew-more',
            'content' =>"\F0D37"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ski',
            'content' =>"\F1304"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ski-cross-country',
            'content' =>"\F1305"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ski-water',
            'content' =>"\F1306"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-backward',
            'content' =>"\F04AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-backward-outline',
            'content' =>"\F0F25"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-forward',
            'content' =>"\F04AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-forward-outline',
            'content' =>"\F0F26"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-next',
            'content' =>"\F04AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-next-circle',
            'content' =>"\F0661"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-next-circle-outline',
            'content' =>"\F0662"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-next-outline',
            'content' =>"\F0F27"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-previous',
            'content' =>"\F04AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-previous-circle',
            'content' =>"\F0663"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-previous-circle-outline',
            'content' =>"\F0664"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skip-previous-outline',
            'content' =>"\F0F28"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skull',
            'content' =>"\F068C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skull-crossbones',
            'content' =>"\F0BC6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skull-crossbones-outline',
            'content' =>"\F0BC7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skull-outline',
            'content' =>"\F0BC8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skull-scan',
            'content' =>"\F14C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skull-scan-outline',
            'content' =>"\F14C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skype',
            'content' =>"\F04AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'skype-business',
            'content' =>"\F04B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'slack',
            'content' =>"\F04B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'slash-forward',
            'content' =>"\F0FDF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'slash-forward-box',
            'content' =>"\F0FE0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sleep',
            'content' =>"\F04B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sleep-off',
            'content' =>"\F04B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'slope-downhill',
            'content' =>"\F0DFF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'slope-uphill',
            'content' =>"\F0E00"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'slot-machine',
            'content' =>"\F1114"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'slot-machine-outline',
            'content' =>"\F1115"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smart-card',
            'content' =>"\F10BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smart-card-outline',
            'content' =>"\F10BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smart-card-reader',
            'content' =>"\F10BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smart-card-reader-outline',
            'content' =>"\F10C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smog',
            'content' =>"\F0A71"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smoke-detector',
            'content' =>"\F0392"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smoking',
            'content' =>"\F04B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smoking-off',
            'content' =>"\F04B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smoking-pipe',
            'content' =>"\F140D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'smoking-pipe-off',
            'content' =>"\F1428"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'snapchat',
            'content' =>"\F04B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'snowboard',
            'content' =>"\F1307"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'snowflake',
            'content' =>"\F0717"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'snowflake-alert',
            'content' =>"\F0F29"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'snowflake-melt',
            'content' =>"\F12CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'snowflake-variant',
            'content' =>"\F0F2A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'snowman',
            'content' =>"\F04B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'soccer',
            'content' =>"\F04B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'soccer-field',
            'content' =>"\F0834"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sofa',
            'content' =>"\F04B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'solar-panel',
            'content' =>"\F0D9B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'solar-panel-large',
            'content' =>"\F0D9C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'solar-power',
            'content' =>"\F0A72"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'soldering-iron',
            'content' =>"\F1092"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'solid',
            'content' =>"\F068D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sony-playstation',
            'content' =>"\F0414"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort',
            'content' =>"\F04BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-alphabetical-ascending',
            'content' =>"\F05BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-alphabetical-ascending-variant',
            'content' =>"\F1148"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-alphabetical-descending',
            'content' =>"\F05BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-alphabetical-descending-variant',
            'content' =>"\F1149"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-alphabetical-variant',
            'content' =>"\F04BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-ascending',
            'content' =>"\F04BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-bool-ascending',
            'content' =>"\F1385"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-bool-ascending-variant',
            'content' =>"\F1386"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-bool-descending',
            'content' =>"\F1387"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-bool-descending-variant',
            'content' =>"\F1388"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-descending',
            'content' =>"\F04BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-numeric-ascending',
            'content' =>"\F1389"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-numeric-ascending-variant',
            'content' =>"\F090D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-numeric-descending',
            'content' =>"\F138A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-numeric-descending-variant',
            'content' =>"\F0AD2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-numeric-variant',
            'content' =>"\F04BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-reverse-variant',
            'content' =>"\F033C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-variant',
            'content' =>"\F04BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-variant-lock',
            'content' =>"\F0CCD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-variant-lock-open',
            'content' =>"\F0CCE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sort-variant-remove',
            'content' =>"\F1147"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'soundcloud',
            'content' =>"\F04C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-branch',
            'content' =>"\F062C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-branch-check',
            'content' =>"\F14CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-branch-minus',
            'content' =>"\F14CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-branch-plus',
            'content' =>"\F14CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-branch-refresh',
            'content' =>"\F14CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-branch-remove',
            'content' =>"\F14CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-branch-sync',
            'content' =>"\F14CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-commit',
            'content' =>"\F0718"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-commit-end',
            'content' =>"\F0719"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-commit-end-local',
            'content' =>"\F071A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-commit-local',
            'content' =>"\F071B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-commit-next-local',
            'content' =>"\F071C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-commit-start',
            'content' =>"\F071D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-commit-start-next-local',
            'content' =>"\F071E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-fork',
            'content' =>"\F04C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-merge',
            'content' =>"\F062D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-pull',
            'content' =>"\F04C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-repository',
            'content' =>"\F0CCF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'source-repository-multiple',
            'content' =>"\F0CD0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'soy-sauce',
            'content' =>"\F07EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'soy-sauce-off',
            'content' =>"\F13FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spa',
            'content' =>"\F0CD1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spa-outline',
            'content' =>"\F0CD2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'space-invaders',
            'content' =>"\F0BC9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'space-station',
            'content' =>"\F1383"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spade',
            'content' =>"\F0E65"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'speaker',
            'content' =>"\F04C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'speaker-bluetooth',
            'content' =>"\F09A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'speaker-multiple',
            'content' =>"\F0D38"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'speaker-off',
            'content' =>"\F04C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'speaker-wireless',
            'content' =>"\F071F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'speedometer',
            'content' =>"\F04C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'speedometer-medium',
            'content' =>"\F0F85"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'speedometer-slow',
            'content' =>"\F0F86"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spellcheck',
            'content' =>"\F04C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spider',
            'content' =>"\F11EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spider-thread',
            'content' =>"\F11EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spider-web',
            'content' =>"\F0BCA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spoon-sugar',
            'content' =>"\F1429"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spotify',
            'content' =>"\F04C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spotlight',
            'content' =>"\F04C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spotlight-beam',
            'content' =>"\F04C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spray',
            'content' =>"\F0665"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'spray-bottle',
            'content' =>"\F0AE0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sprinkler',
            'content' =>"\F105F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sprinkler-variant',
            'content' =>"\F1060"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sprout',
            'content' =>"\F0E66"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sprout-outline',
            'content' =>"\F0E67"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square',
            'content' =>"\F0764"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-edit-outline',
            'content' =>"\F090C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-medium',
            'content' =>"\F0A13"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-medium-outline',
            'content' =>"\F0A14"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-off',
            'content' =>"\F12EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-off-outline',
            'content' =>"\F12EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-outline',
            'content' =>"\F0763"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-root',
            'content' =>"\F0784"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-root-box',
            'content' =>"\F09A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-small',
            'content' =>"\F0A15"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'square-wave',
            'content' =>"\F147B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'squeegee',
            'content' =>"\F0AE1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ssh',
            'content' =>"\F08C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stack-exchange',
            'content' =>"\F060B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stack-overflow',
            'content' =>"\F04CC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stackpath',
            'content' =>"\F0359"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stadium',
            'content' =>"\F0FF9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stadium-variant',
            'content' =>"\F0720"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stairs',
            'content' =>"\F04CD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stairs-box',
            'content' =>"\F139E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stairs-down',
            'content' =>"\F12BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stairs-up',
            'content' =>"\F12BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stamper',
            'content' =>"\F0D39"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'standard-definition',
            'content' =>"\F07EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star',
            'content' =>"\F04CE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-box',
            'content' =>"\F0A73"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-box-multiple',
            'content' =>"\F1286"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-box-multiple-outline',
            'content' =>"\F1287"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-box-outline',
            'content' =>"\F0A74"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-circle',
            'content' =>"\F04CF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-circle-outline',
            'content' =>"\F09A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-face',
            'content' =>"\F09A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-four-points',
            'content' =>"\F0AE2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-four-points-outline',
            'content' =>"\F0AE3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-half',
            'content' =>"\F0246"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-half-full',
            'content' =>"\F04D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-off',
            'content' =>"\F04D1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-outline',
            'content' =>"\F04D2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-three-points',
            'content' =>"\F0AE4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'star-three-points-outline',
            'content' =>"\F0AE5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'state-machine',
            'content' =>"\F11EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'steam',
            'content' =>"\F04D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'steering',
            'content' =>"\F04D4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'steering-off',
            'content' =>"\F090E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'step-backward',
            'content' =>"\F04D5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'step-backward-2',
            'content' =>"\F04D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'step-forward',
            'content' =>"\F04D7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'step-forward-2',
            'content' =>"\F04D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stethoscope',
            'content' =>"\F04D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker',
            'content' =>"\F1364"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-alert',
            'content' =>"\F1365"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-alert-outline',
            'content' =>"\F1366"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-check',
            'content' =>"\F1367"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-check-outline',
            'content' =>"\F1368"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-circle-outline',
            'content' =>"\F05D0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-emoji',
            'content' =>"\F0785"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-minus',
            'content' =>"\F1369"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-minus-outline',
            'content' =>"\F136A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-outline',
            'content' =>"\F136B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-plus',
            'content' =>"\F136C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-plus-outline',
            'content' =>"\F136D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-remove',
            'content' =>"\F136E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sticker-remove-outline',
            'content' =>"\F136F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stocking',
            'content' =>"\F04DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stomach',
            'content' =>"\F1093"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stop',
            'content' =>"\F04DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stop-circle',
            'content' =>"\F0666"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stop-circle-outline',
            'content' =>"\F0667"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'store',
            'content' =>"\F04DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'store-24-hour',
            'content' =>"\F04DD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'store-outline',
            'content' =>"\F1361"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'storefront',
            'content' =>"\F07C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'storefront-outline',
            'content' =>"\F10C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stove',
            'content' =>"\F04DE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'strategy',
            'content' =>"\F11D6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stretch-to-page',
            'content' =>"\F0F2B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'stretch-to-page-outline',
            'content' =>"\F0F2C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'string-lights',
            'content' =>"\F12BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'string-lights-off',
            'content' =>"\F12BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'subdirectory-arrow-left',
            'content' =>"\F060C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'subdirectory-arrow-right',
            'content' =>"\F060D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'subtitles',
            'content' =>"\F0A16"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'subtitles-outline',
            'content' =>"\F0A17"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'subway',
            'content' =>"\F06AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'subway-alert-variant',
            'content' =>"\F0D9D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'subway-variant',
            'content' =>"\F04DF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'summit',
            'content' =>"\F0786"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sunglasses',
            'content' =>"\F04E0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'surround-sound',
            'content' =>"\F05C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'surround-sound-2-0',
            'content' =>"\F07F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'surround-sound-3-1',
            'content' =>"\F07F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'surround-sound-5-1',
            'content' =>"\F07F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'surround-sound-7-1',
            'content' =>"\F07F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'svg',
            'content' =>"\F0721"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-horizontal',
            'content' =>"\F04E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-horizontal-bold',
            'content' =>"\F0BCD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-horizontal-circle',
            'content' =>"\F0FE1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-horizontal-circle-outline',
            'content' =>"\F0FE2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-horizontal-variant',
            'content' =>"\F08C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-vertical',
            'content' =>"\F04E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-vertical-bold',
            'content' =>"\F0BCE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-vertical-circle',
            'content' =>"\F0FE3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-vertical-circle-outline',
            'content' =>"\F0FE4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swap-vertical-variant',
            'content' =>"\F08C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'swim',
            'content' =>"\F04E3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'switch',
            'content' =>"\F04E4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sword',
            'content' =>"\F04E5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sword-cross',
            'content' =>"\F0787"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'syllabary-hangul',
            'content' =>"\F1333"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'syllabary-hiragana',
            'content' =>"\F1334"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'syllabary-katakana',
            'content' =>"\F1335"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'syllabary-katakana-halfwidth',
            'content' =>"\F1336"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'symfony',
            'content' =>"\F0AE6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sync',
            'content' =>"\F04E6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sync-alert',
            'content' =>"\F04E7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sync-circle',
            'content' =>"\F1378"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'sync-off',
            'content' =>"\F04E8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tab',
            'content' =>"\F04E9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tab-minus',
            'content' =>"\F0B4B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tab-plus',
            'content' =>"\F075C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tab-remove',
            'content' =>"\F0B4C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tab-unselected',
            'content' =>"\F04EA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table',
            'content' =>"\F04EB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-account',
            'content' =>"\F13B9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-alert',
            'content' =>"\F13BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-arrow-down',
            'content' =>"\F13BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-arrow-left',
            'content' =>"\F13BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-arrow-right',
            'content' =>"\F13BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-arrow-up',
            'content' =>"\F13BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-border',
            'content' =>"\F0A18"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-cancel',
            'content' =>"\F13BF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-chair',
            'content' =>"\F1061"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-check',
            'content' =>"\F13C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-clock',
            'content' =>"\F13C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-cog',
            'content' =>"\F13C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-column',
            'content' =>"\F0835"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-column-plus-after',
            'content' =>"\F04EC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-column-plus-before',
            'content' =>"\F04ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-column-remove',
            'content' =>"\F04EE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-column-width',
            'content' =>"\F04EF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-edit',
            'content' =>"\F04F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-eye',
            'content' =>"\F1094"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-eye-off',
            'content' =>"\F13C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-furniture',
            'content' =>"\F05BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-headers-eye',
            'content' =>"\F121D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-headers-eye-off',
            'content' =>"\F121E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-heart',
            'content' =>"\F13C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-key',
            'content' =>"\F13C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-large',
            'content' =>"\F04F1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-large-plus',
            'content' =>"\F0F87"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-large-remove',
            'content' =>"\F0F88"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-lock',
            'content' =>"\F13C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-merge-cells',
            'content' =>"\F09A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-minus',
            'content' =>"\F13C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-multiple',
            'content' =>"\F13C8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-network',
            'content' =>"\F13C9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-of-contents',
            'content' =>"\F0836"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-off',
            'content' =>"\F13CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-plus',
            'content' =>"\F0A75"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-refresh',
            'content' =>"\F13A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-remove',
            'content' =>"\F0A76"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-row',
            'content' =>"\F0837"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-row-height',
            'content' =>"\F04F2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-row-plus-after',
            'content' =>"\F04F3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-row-plus-before',
            'content' =>"\F04F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-row-remove',
            'content' =>"\F04F5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-search',
            'content' =>"\F090F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-settings',
            'content' =>"\F0838"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-split-cell',
            'content' =>"\F142A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-star',
            'content' =>"\F13CB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-sync',
            'content' =>"\F13A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'table-tennis',
            'content' =>"\F0E68"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tablet',
            'content' =>"\F04F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tablet-android',
            'content' =>"\F04F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tablet-cellphone',
            'content' =>"\F09A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tablet-dashboard',
            'content' =>"\F0ECE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tablet-ipad',
            'content' =>"\F04F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'taco',
            'content' =>"\F0762"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag',
            'content' =>"\F04F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-faces',
            'content' =>"\F04FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-heart',
            'content' =>"\F068B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-heart-outline',
            'content' =>"\F0BCF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-minus',
            'content' =>"\F0910"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-minus-outline',
            'content' =>"\F121F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-multiple',
            'content' =>"\F04FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-multiple-outline',
            'content' =>"\F12F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-off',
            'content' =>"\F1220"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-off-outline',
            'content' =>"\F1221"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-outline',
            'content' =>"\F04FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-plus',
            'content' =>"\F0722"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-plus-outline',
            'content' =>"\F1222"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-remove',
            'content' =>"\F0723"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-remove-outline',
            'content' =>"\F1223"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-text',
            'content' =>"\F1224"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tag-text-outline',
            'content' =>"\F04FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tailwind',
            'content' =>"\F13FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tank',
            'content' =>"\F0D3A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tanker-truck',
            'content' =>"\F0FE5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tape-measure',
            'content' =>"\F0B4D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'target',
            'content' =>"\F04FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'target-account',
            'content' =>"\F0BD0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'target-variant',
            'content' =>"\F0A77"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'taxi',
            'content' =>"\F04FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tea',
            'content' =>"\F0D9E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tea-outline',
            'content' =>"\F0D9F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'teach',
            'content' =>"\F0890"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'teamviewer',
            'content' =>"\F0500"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'telegram',
            'content' =>"\F0501"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'telescope',
            'content' =>"\F0B4E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television',
            'content' =>"\F0502"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-ambient-light',
            'content' =>"\F1356"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-box',
            'content' =>"\F0839"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-classic',
            'content' =>"\F07F4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-classic-off',
            'content' =>"\F083A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-clean',
            'content' =>"\F1110"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-guide',
            'content' =>"\F0503"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-off',
            'content' =>"\F083B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-pause',
            'content' =>"\F0F89"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-play',
            'content' =>"\F0ECF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'television-stop',
            'content' =>"\F0F8A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'temperature-celsius',
            'content' =>"\F0504"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'temperature-fahrenheit',
            'content' =>"\F0505"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'temperature-kelvin',
            'content' =>"\F0506"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tennis',
            'content' =>"\F0DA0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tennis-ball',
            'content' =>"\F0507"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tent',
            'content' =>"\F0508"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'terraform',
            'content' =>"\F1062"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'terrain',
            'content' =>"\F0509"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'test-tube',
            'content' =>"\F0668"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'test-tube-empty',
            'content' =>"\F0911"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'test-tube-off',
            'content' =>"\F0912"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text',
            'content' =>"\F09A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box',
            'content' =>"\F021A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-check',
            'content' =>"\F0EA6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-check-outline',
            'content' =>"\F0EA7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-minus',
            'content' =>"\F0EA8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-minus-outline',
            'content' =>"\F0EA9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-multiple',
            'content' =>"\F0AB7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-multiple-outline',
            'content' =>"\F0AB8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-outline',
            'content' =>"\F09ED"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-plus',
            'content' =>"\F0EAA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-plus-outline',
            'content' =>"\F0EAB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-remove',
            'content' =>"\F0EAC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-remove-outline',
            'content' =>"\F0EAD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-search',
            'content' =>"\F0EAE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-box-search-outline',
            'content' =>"\F0EAF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-recognition',
            'content' =>"\F113D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-search',
            'content' =>"\F13B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-shadow',
            'content' =>"\F0669"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-short',
            'content' =>"\F09A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-subject',
            'content' =>"\F09AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-to-speech',
            'content' =>"\F050A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'text-to-speech-off',
            'content' =>"\F050B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'texture',
            'content' =>"\F050C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'texture-box',
            'content' =>"\F0FE6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'theater',
            'content' =>"\F050D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'theme-light-dark',
            'content' =>"\F050E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermometer',
            'content' =>"\F050F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermometer-alert',
            'content' =>"\F0E01"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermometer-chevron-down',
            'content' =>"\F0E02"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermometer-chevron-up',
            'content' =>"\F0E03"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermometer-high',
            'content' =>"\F10C2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermometer-lines',
            'content' =>"\F0510"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermometer-low',
            'content' =>"\F10C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermometer-minus',
            'content' =>"\F0E04"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermometer-plus',
            'content' =>"\F0E05"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermostat',
            'content' =>"\F0393"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thermostat-box',
            'content' =>"\F0891"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thought-bubble',
            'content' =>"\F07F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thought-bubble-outline',
            'content' =>"\F07F7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thumb-down',
            'content' =>"\F0511"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thumb-down-outline',
            'content' =>"\F0512"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thumb-up',
            'content' =>"\F0513"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thumb-up-outline',
            'content' =>"\F0514"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'thumbs-up-down',
            'content' =>"\F0515"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ticket',
            'content' =>"\F0516"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ticket-account',
            'content' =>"\F0517"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ticket-confirmation',
            'content' =>"\F0518"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ticket-confirmation-outline',
            'content' =>"\F13AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ticket-outline',
            'content' =>"\F0913"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ticket-percent',
            'content' =>"\F0724"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ticket-percent-outline',
            'content' =>"\F142B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tie',
            'content' =>"\F0519"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tilde',
            'content' =>"\F0725"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timelapse',
            'content' =>"\F051A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline',
            'content' =>"\F0BD1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-alert',
            'content' =>"\F0F95"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-alert-outline',
            'content' =>"\F0F98"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-clock',
            'content' =>"\F11FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-clock-outline',
            'content' =>"\F11FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-help',
            'content' =>"\F0F99"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-help-outline',
            'content' =>"\F0F9A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-outline',
            'content' =>"\F0BD2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-plus',
            'content' =>"\F0F96"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-plus-outline',
            'content' =>"\F0F97"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-text',
            'content' =>"\F0BD3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timeline-text-outline',
            'content' =>"\F0BD4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timer',
            'content' =>"\F13AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timer-10',
            'content' =>"\F051C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timer-3',
            'content' =>"\F051D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timer-off',
            'content' =>"\F13AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timer-off-outline',
            'content' =>"\F051E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timer-outline',
            'content' =>"\F051B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timer-sand',
            'content' =>"\F051F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timer-sand-empty',
            'content' =>"\F06AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timer-sand-full',
            'content' =>"\F078C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'timetable',
            'content' =>"\F0520"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toaster',
            'content' =>"\F1063"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toaster-off',
            'content' =>"\F11B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toaster-oven',
            'content' =>"\F0CD3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toggle-switch',
            'content' =>"\F0521"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toggle-switch-off',
            'content' =>"\F0522"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toggle-switch-off-outline',
            'content' =>"\F0A19"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toggle-switch-outline',
            'content' =>"\F0A1A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toilet',
            'content' =>"\F09AB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toolbox',
            'content' =>"\F09AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toolbox-outline',
            'content' =>"\F09AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tools',
            'content' =>"\F1064"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip',
            'content' =>"\F0523"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-account',
            'content' =>"\F000C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-edit',
            'content' =>"\F0524"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-edit-outline',
            'content' =>"\F12C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-image',
            'content' =>"\F0525"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-image-outline',
            'content' =>"\F0BD5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-outline',
            'content' =>"\F0526"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-plus',
            'content' =>"\F0BD6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-plus-outline',
            'content' =>"\F0527"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-text',
            'content' =>"\F0528"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooltip-text-outline',
            'content' =>"\F0BD7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooth',
            'content' =>"\F08C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tooth-outline',
            'content' =>"\F0529"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toothbrush',
            'content' =>"\F1129"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toothbrush-electric',
            'content' =>"\F112C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toothbrush-paste',
            'content' =>"\F112A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tortoise',
            'content' =>"\F0D3B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toslink',
            'content' =>"\F12B8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tournament',
            'content' =>"\F09AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tow-truck',
            'content' =>"\F083C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tower-beach',
            'content' =>"\F0681"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tower-fire',
            'content' =>"\F0682"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick',
            'content' =>"\F1288"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-marker',
            'content' =>"\F1289"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-marker-outline',
            'content' =>"\F128A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-minus',
            'content' =>"\F128B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-minus-outline',
            'content' =>"\F128C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-outline',
            'content' =>"\F128D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-plus',
            'content' =>"\F128E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-plus-outline',
            'content' =>"\F128F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-remove',
            'content' =>"\F1290"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-remove-outline',
            'content' =>"\F1291"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-search',
            'content' =>"\F1292"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'toy-brick-search-outline',
            'content' =>"\F1293"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'track-light',
            'content' =>"\F0914"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trackpad',
            'content' =>"\F07F8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trackpad-lock',
            'content' =>"\F0933"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tractor',
            'content' =>"\F0892"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tractor-variant',
            'content' =>"\F14C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trademark',
            'content' =>"\F0A78"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'traffic-cone',
            'content' =>"\F137C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'traffic-light',
            'content' =>"\F052B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'train',
            'content' =>"\F052C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'train-car',
            'content' =>"\F0BD8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'train-variant',
            'content' =>"\F08C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tram',
            'content' =>"\F052D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tram-side',
            'content' =>"\F0FE7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transcribe',
            'content' =>"\F052E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transcribe-close',
            'content' =>"\F052F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transfer',
            'content' =>"\F1065"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transfer-down',
            'content' =>"\F0DA1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transfer-left',
            'content' =>"\F0DA2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transfer-right',
            'content' =>"\F0530"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transfer-up',
            'content' =>"\F0DA3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transit-connection',
            'content' =>"\F0D3C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transit-connection-variant',
            'content' =>"\F0D3D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transit-detour',
            'content' =>"\F0F8B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transit-transfer',
            'content' =>"\F06AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transition',
            'content' =>"\F0915"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transition-masked',
            'content' =>"\F0916"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'translate',
            'content' =>"\F05CA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'translate-off',
            'content' =>"\F0E06"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'transmission-tower',
            'content' =>"\F0D3E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trash-can',
            'content' =>"\F0A79"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trash-can-outline',
            'content' =>"\F0A7A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tray',
            'content' =>"\F1294"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tray-alert',
            'content' =>"\F1295"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tray-full',
            'content' =>"\F1296"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tray-minus',
            'content' =>"\F1297"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tray-plus',
            'content' =>"\F1298"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tray-remove',
            'content' =>"\F1299"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'treasure-chest',
            'content' =>"\F0726"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tree',
            'content' =>"\F0531"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tree-outline',
            'content' =>"\F0E69"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trello',
            'content' =>"\F0532"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trending-down',
            'content' =>"\F0533"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trending-neutral',
            'content' =>"\F0534"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trending-up',
            'content' =>"\F0535"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'triangle',
            'content' =>"\F0536"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'triangle-outline',
            'content' =>"\F0537"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'triangle-wave',
            'content' =>"\F147C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'triforce',
            'content' =>"\F0BD9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trophy',
            'content' =>"\F0538"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trophy-award',
            'content' =>"\F0539"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trophy-broken',
            'content' =>"\F0DA4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trophy-outline',
            'content' =>"\F053A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trophy-variant',
            'content' =>"\F053B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trophy-variant-outline',
            'content' =>"\F053C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'truck',
            'content' =>"\F053D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'truck-check',
            'content' =>"\F0CD4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'truck-check-outline',
            'content' =>"\F129A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'truck-delivery',
            'content' =>"\F053E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'truck-delivery-outline',
            'content' =>"\F129B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'truck-fast',
            'content' =>"\F0788"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'truck-fast-outline',
            'content' =>"\F129C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'truck-outline',
            'content' =>"\F129D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'truck-trailer',
            'content' =>"\F0727"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'trumpet',
            'content' =>"\F1096"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tshirt-crew',
            'content' =>"\F0A7B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tshirt-crew-outline',
            'content' =>"\F053F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tshirt-v',
            'content' =>"\F0A7C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tshirt-v-outline',
            'content' =>"\F0540"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tumble-dryer',
            'content' =>"\F0917"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tumble-dryer-alert',
            'content' =>"\F11BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tumble-dryer-off',
            'content' =>"\F11BB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tune',
            'content' =>"\F062E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'tune-vertical',
            'content' =>"\F066A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'turnstile',
            'content' =>"\F0CD5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'turnstile-outline',
            'content' =>"\F0CD6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'turtle',
            'content' =>"\F0CD7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'twitch',
            'content' =>"\F0543"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'twitter',
            'content' =>"\F0544"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'twitter-retweet',
            'content' =>"\F0547"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'two-factor-authentication',
            'content' =>"\F09AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'typewriter',
            'content' =>"\F0F2D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ubisoft',
            'content' =>"\F0BDA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ubuntu',
            'content' =>"\F0548"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ufo',
            'content' =>"\F10C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ufo-outline',
            'content' =>"\F10C5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ultra-high-definition',
            'content' =>"\F07F9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'umbraco',
            'content' =>"\F0549"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'umbrella',
            'content' =>"\F054A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'umbrella-closed',
            'content' =>"\F09B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'umbrella-closed-outline',
            'content' =>"\F13E2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'umbrella-closed-variant',
            'content' =>"\F13E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'umbrella-outline',
            'content' =>"\F054B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'undo',
            'content' =>"\F054C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'undo-variant',
            'content' =>"\F054D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'unfold-less-horizontal',
            'content' =>"\F054E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'unfold-less-vertical',
            'content' =>"\F0760"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'unfold-more-horizontal',
            'content' =>"\F054F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'unfold-more-vertical',
            'content' =>"\F0761"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'ungroup',
            'content' =>"\F0550"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'unicode',
            'content' =>"\F0ED0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'unity',
            'content' =>"\F06AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'unreal',
            'content' =>"\F09B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'untappd',
            'content' =>"\F0551"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'update',
            'content' =>"\F06B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'upload',
            'content' =>"\F0552"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'upload-lock',
            'content' =>"\F1373"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'upload-lock-outline',
            'content' =>"\F1374"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'upload-multiple',
            'content' =>"\F083D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'upload-network',
            'content' =>"\F06F6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'upload-network-outline',
            'content' =>"\F0CD8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'upload-off',
            'content' =>"\F10C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'upload-off-outline',
            'content' =>"\F10C7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'upload-outline',
            'content' =>"\F0E07"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'usb',
            'content' =>"\F0553"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'usb-flash-drive',
            'content' =>"\F129E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'usb-flash-drive-outline',
            'content' =>"\F129F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'usb-port',
            'content' =>"\F11F0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'valve',
            'content' =>"\F1066"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'valve-closed',
            'content' =>"\F1067"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'valve-open',
            'content' =>"\F1068"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'van-passenger',
            'content' =>"\F07FA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'van-utility',
            'content' =>"\F07FB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vanish',
            'content' =>"\F07FC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vanity-light',
            'content' =>"\F11E1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'variable',
            'content' =>"\F0AE7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'variable-box',
            'content' =>"\F1111"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-arrange-above',
            'content' =>"\F0554"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-arrange-below',
            'content' =>"\F0555"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-bezier',
            'content' =>"\F0AE8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-circle',
            'content' =>"\F0556"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-circle-variant',
            'content' =>"\F0557"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-combine',
            'content' =>"\F0558"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-curve',
            'content' =>"\F0559"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-difference',
            'content' =>"\F055A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-difference-ab',
            'content' =>"\F055B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-difference-ba',
            'content' =>"\F055C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-ellipse',
            'content' =>"\F0893"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-intersection',
            'content' =>"\F055D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-line',
            'content' =>"\F055E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-link',
            'content' =>"\F0FE8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-point',
            'content' =>"\F055F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-polygon',
            'content' =>"\F0560"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-polyline',
            'content' =>"\F0561"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-polyline-edit',
            'content' =>"\F1225"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-polyline-minus',
            'content' =>"\F1226"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-polyline-plus',
            'content' =>"\F1227"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-polyline-remove',
            'content' =>"\F1228"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-radius',
            'content' =>"\F074A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-rectangle',
            'content' =>"\F05C6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-selection',
            'content' =>"\F0562"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-square',
            'content' =>"\F0001"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-triangle',
            'content' =>"\F0563"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vector-union',
            'content' =>"\F0564"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vhs',
            'content' =>"\F0A1B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vibrate',
            'content' =>"\F0566"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vibrate-off',
            'content' =>"\F0CD9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video',
            'content' =>"\F0567"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-3d',
            'content' =>"\F07FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-3d-off',
            'content' =>"\F13D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-3d-variant',
            'content' =>"\F0ED1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-4k-box',
            'content' =>"\F083E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-account',
            'content' =>"\F0919"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-box',
            'content' =>"\F00FD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-box-off',
            'content' =>"\F00FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-check',
            'content' =>"\F1069"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-check-outline',
            'content' =>"\F106A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-image',
            'content' =>"\F091A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-input-antenna',
            'content' =>"\F083F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-input-component',
            'content' =>"\F0840"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-input-hdmi',
            'content' =>"\F0841"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-input-scart',
            'content' =>"\F0F8C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-input-svideo',
            'content' =>"\F0842"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-minus',
            'content' =>"\F09B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-minus-outline',
            'content' =>"\F02BA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-off',
            'content' =>"\F0568"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-off-outline',
            'content' =>"\F0BDB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-outline',
            'content' =>"\F0BDC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-plus',
            'content' =>"\F09B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-plus-outline',
            'content' =>"\F01D3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-stabilization',
            'content' =>"\F091B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-switch',
            'content' =>"\F0569"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-switch-outline',
            'content' =>"\F0790"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-vintage',
            'content' =>"\F0A1C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-wireless',
            'content' =>"\F0ED2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'video-wireless-outline',
            'content' =>"\F0ED3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-agenda',
            'content' =>"\F056A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-agenda-outline',
            'content' =>"\F11D8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-array',
            'content' =>"\F056B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-array-outline',
            'content' =>"\F1485"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-carousel',
            'content' =>"\F056C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-carousel-outline',
            'content' =>"\F1486"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-column',
            'content' =>"\F056D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-column-outline',
            'content' =>"\F1487"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-comfy',
            'content' =>"\F0E6A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-comfy-outline',
            'content' =>"\F1488"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-compact',
            'content' =>"\F0E6B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-compact-outline',
            'content' =>"\F0E6C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-dashboard',
            'content' =>"\F056E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-dashboard-outline',
            'content' =>"\F0A1D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-dashboard-variant',
            'content' =>"\F0843"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-dashboard-variant-outline',
            'content' =>"\F1489"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-day',
            'content' =>"\F056F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-day-outline',
            'content' =>"\F148A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-grid',
            'content' =>"\F0570"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-grid-outline',
            'content' =>"\F11D9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-grid-plus',
            'content' =>"\F0F8D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-grid-plus-outline',
            'content' =>"\F11DA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-headline',
            'content' =>"\F0571"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-list',
            'content' =>"\F0572"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-list-outline',
            'content' =>"\F148B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-module',
            'content' =>"\F0573"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-module-outline',
            'content' =>"\F148C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-parallel',
            'content' =>"\F0728"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-parallel-outline',
            'content' =>"\F148D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-quilt',
            'content' =>"\F0574"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-quilt-outline',
            'content' =>"\F148E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-sequential',
            'content' =>"\F0729"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-sequential-outline',
            'content' =>"\F148F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-split-horizontal',
            'content' =>"\F0BCB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-split-vertical',
            'content' =>"\F0BCC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-stream',
            'content' =>"\F0575"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-stream-outline',
            'content' =>"\F1490"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-week',
            'content' =>"\F0576"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'view-week-outline',
            'content' =>"\F1491"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vimeo',
            'content' =>"\F0577"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'violin',
            'content' =>"\F060F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'virtual-reality',
            'content' =>"\F0894"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'virus',
            'content' =>"\F13B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'virus-outline',
            'content' =>"\F13B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vk',
            'content' =>"\F0579"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vlc',
            'content' =>"\F057C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'voice-off',
            'content' =>"\F0ED4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'voicemail',
            'content' =>"\F057D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volleyball',
            'content' =>"\F09B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-high',
            'content' =>"\F057E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-low',
            'content' =>"\F057F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-medium',
            'content' =>"\F0580"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-minus',
            'content' =>"\F075E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-mute',
            'content' =>"\F075F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-off',
            'content' =>"\F0581"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-plus',
            'content' =>"\F075D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-source',
            'content' =>"\F1120"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-variant-off',
            'content' =>"\F0E08"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'volume-vibrate',
            'content' =>"\F1121"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vote',
            'content' =>"\F0A1F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vote-outline',
            'content' =>"\F0A20"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vpn',
            'content' =>"\F0582"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vuejs',
            'content' =>"\F0844"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'vuetify',
            'content' =>"\F0E6D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'walk',
            'content' =>"\F0583"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wall',
            'content' =>"\F07FE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wall-sconce',
            'content' =>"\F091C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wall-sconce-flat',
            'content' =>"\F091D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wall-sconce-flat-variant',
            'content' =>"\F041C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wall-sconce-round',
            'content' =>"\F0748"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wall-sconce-round-variant',
            'content' =>"\F091E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wallet',
            'content' =>"\F0584"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wallet-giftcard',
            'content' =>"\F0585"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wallet-membership',
            'content' =>"\F0586"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wallet-outline',
            'content' =>"\F0BDD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wallet-plus',
            'content' =>"\F0F8E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wallet-plus-outline',
            'content' =>"\F0F8F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wallet-travel',
            'content' =>"\F0587"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wallpaper',
            'content' =>"\F0E09"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wan',
            'content' =>"\F0588"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wardrobe',
            'content' =>"\F0F90"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wardrobe-outline',
            'content' =>"\F0F91"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'warehouse',
            'content' =>"\F0F81"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'washing-machine',
            'content' =>"\F072A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'washing-machine-alert',
            'content' =>"\F11BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'washing-machine-off',
            'content' =>"\F11BD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watch',
            'content' =>"\F0589"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watch-export',
            'content' =>"\F058A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watch-export-variant',
            'content' =>"\F0895"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watch-import',
            'content' =>"\F058B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watch-import-variant',
            'content' =>"\F0896"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watch-variant',
            'content' =>"\F0897"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watch-vibrate',
            'content' =>"\F06B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watch-vibrate-off',
            'content' =>"\F0CDA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water',
            'content' =>"\F058C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-boiler',
            'content' =>"\F0F92"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-boiler-alert',
            'content' =>"\F11B3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-boiler-off',
            'content' =>"\F11B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-off',
            'content' =>"\F058D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-outline',
            'content' =>"\F0E0A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-percent',
            'content' =>"\F058E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-polo',
            'content' =>"\F12A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-pump',
            'content' =>"\F058F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-pump-off',
            'content' =>"\F0F93"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-well',
            'content' =>"\F106B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'water-well-outline',
            'content' =>"\F106C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watering-can',
            'content' =>"\F1481"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watering-can-outline',
            'content' =>"\F1482"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'watermark',
            'content' =>"\F0612"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wave',
            'content' =>"\F0F2E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'waveform',
            'content' =>"\F147D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'waves',
            'content' =>"\F078D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'waze',
            'content' =>"\F0BDE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-cloudy',
            'content' =>"\F0590"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-cloudy-alert',
            'content' =>"\F0F2F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-cloudy-arrow-right',
            'content' =>"\F0E6E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-fog',
            'content' =>"\F0591"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-hail',
            'content' =>"\F0592"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-hazy',
            'content' =>"\F0F30"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-hurricane',
            'content' =>"\F0898"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-lightning',
            'content' =>"\F0593"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-lightning-rainy',
            'content' =>"\F067E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-night',
            'content' =>"\F0594"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-night-partly-cloudy',
            'content' =>"\F0F31"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-partly-cloudy',
            'content' =>"\F0595"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-partly-lightning',
            'content' =>"\F0F32"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-partly-rainy',
            'content' =>"\F0F33"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-partly-snowy',
            'content' =>"\F0F34"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-partly-snowy-rainy',
            'content' =>"\F0F35"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-pouring',
            'content' =>"\F0596"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-rainy',
            'content' =>"\F0597"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-snowy',
            'content' =>"\F0598"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-snowy-heavy',
            'content' =>"\F0F36"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-snowy-rainy',
            'content' =>"\F067F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-sunny',
            'content' =>"\F0599"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-sunny-alert',
            'content' =>"\F0F37"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-sunset',
            'content' =>"\F059A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-sunset-down',
            'content' =>"\F059B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-sunset-up',
            'content' =>"\F059C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-tornado',
            'content' =>"\F0F38"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-windy',
            'content' =>"\F059D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weather-windy-variant',
            'content' =>"\F059E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'web',
            'content' =>"\F059F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'web-box',
            'content' =>"\F0F94"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'web-clock',
            'content' =>"\F124A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'webcam',
            'content' =>"\F05A0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'webhook',
            'content' =>"\F062F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'webpack',
            'content' =>"\F072B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'webrtc',
            'content' =>"\F1248"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wechat',
            'content' =>"\F0611"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weight',
            'content' =>"\F05A1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weight-gram',
            'content' =>"\F0D3F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weight-kilogram',
            'content' =>"\F05A2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weight-lifter',
            'content' =>"\F115D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'weight-pound',
            'content' =>"\F09B5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'whatsapp',
            'content' =>"\F05A3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wheelchair-accessibility',
            'content' =>"\F05A4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'whistle',
            'content' =>"\F09B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'whistle-outline',
            'content' =>"\F12BC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'white-balance-auto',
            'content' =>"\F05A5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'white-balance-incandescent',
            'content' =>"\F05A6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'white-balance-iridescent',
            'content' =>"\F05A7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'white-balance-sunny',
            'content' =>"\F05A8"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'widgets',
            'content' =>"\F072C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'widgets-outline',
            'content' =>"\F1355"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi',
            'content' =>"\F05A9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-off',
            'content' =>"\F05AA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-star',
            'content' =>"\F0E0B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-1',
            'content' =>"\F091F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-1-alert',
            'content' =>"\F0920"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-1-lock',
            'content' =>"\F0921"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-2',
            'content' =>"\F0922"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-2-alert',
            'content' =>"\F0923"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-2-lock',
            'content' =>"\F0924"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-3',
            'content' =>"\F0925"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-3-alert',
            'content' =>"\F0926"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-3-lock',
            'content' =>"\F0927"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-4',
            'content' =>"\F0928"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-4-alert',
            'content' =>"\F0929"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-4-lock',
            'content' =>"\F092A"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-alert-outline',
            'content' =>"\F092B"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-lock-outline',
            'content' =>"\F092C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-off',
            'content' =>"\F092D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-off-outline',
            'content' =>"\F092E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wifi-strength-outline',
            'content' =>"\F092F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wikipedia',
            'content' =>"\F05AC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wind-turbine',
            'content' =>"\F0DA5"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-close',
            'content' =>"\F05AD"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-closed',
            'content' =>"\F05AE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-closed-variant',
            'content' =>"\F11DB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-maximize',
            'content' =>"\F05AF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-minimize',
            'content' =>"\F05B0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-open',
            'content' =>"\F05B1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-open-variant',
            'content' =>"\F11DC"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-restore',
            'content' =>"\F05B2"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-shutter',
            'content' =>"\F111C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-shutter-alert',
            'content' =>"\F111D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'window-shutter-open',
            'content' =>"\F111E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wiper',
            'content' =>"\F0AE9"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wiper-wash',
            'content' =>"\F0DA6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wizard-hat',
            'content' =>"\F1477"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wordpress',
            'content' =>"\F05B4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wrap',
            'content' =>"\F05B6"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wrap-disabled',
            'content' =>"\F0BDF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wrench',
            'content' =>"\F05B7"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'wrench-outline',
            'content' =>"\F0BE0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'xamarin',
            'content' =>"\F0845"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'xamarin-outline',
            'content' =>"\F0846"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'xing',
            'content' =>"\F05BE"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'xml',
            'content' =>"\F05C0"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'xmpp',
            'content' =>"\F07FF"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'y-combinator',
            'content' =>"\F0624"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'yahoo',
            'content' =>"\F0B4F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'yeast',
            'content' =>"\F05C1"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'yin-yang',
            'content' =>"\F0680"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'yoga',
            'content' =>"\F117C"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'youtube',
            'content' =>"\F05C3"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'youtube-gaming',
            'content' =>"\F0848"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'youtube-studio',
            'content' =>"\F0847"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'youtube-subscription',
            'content' =>"\F0D40"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'youtube-tv',
            'content' =>"\F0448"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'z-wave',
            'content' =>"\F0AEA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zend',
            'content' =>"\F0AEB"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zigbee',
            'content' =>"\F0D41"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zip-box',
            'content' =>"\F05C4"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zip-box-outline',
            'content' =>"\F0FFA"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zip-disk',
            'content' =>"\F0A23"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-aquarius',
            'content' =>"\F0A7D"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-aries',
            'content' =>"\F0A7E"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-cancer',
            'content' =>"\F0A7F"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-capricorn',
            'content' =>"\F0A80"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-gemini',
            'content' =>"\F0A81"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-leo',
            'content' =>"\F0A82"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-libra',
            'content' =>"\F0A83"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-pisces',
            'content' =>"\F0A84"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-sagittarius',
            'content' =>"\F0A85"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-scorpio',
            'content' =>"\F0A86"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-taurus',
            'content' =>"\F0A87"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'zodiac-virgo',
            'content' =>"\F0A88"
        ]);
        factory(App\MaterialIcon::class)->create([
            'name' => 'blank',
            'content' =>"\F68C"
        ]);
    }
}
