<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\MaterialIcon;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(MaterialIcon::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'content' => $faker->word,
    ];
});
